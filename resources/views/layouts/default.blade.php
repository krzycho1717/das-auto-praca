<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">

        <? $agent = new \Jenssegers\Agent\Agent(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <meta name="token" content="{{ Session::token() }}">

        @if(isset($title))
            <title>{{ $title.' - DasAuto' }}</title>
        @else
            <title>{{ 'DasAuto' }}</title>
        @endif
        <!-- Roboto font -->
        {{--<link href="https://fonts.googleapis.com/css?family=Roboto:300,700,400&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">--}}
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700i" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="/css/app.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="/css/bootstrap.css">
        <!-- Material design icons font -->
        <link href="/css/materialdesignicons.css" media="all" rel="stylesheet" type="text/css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="//js.pusher.com/3.0/pusher.min.js"></script>
        <script>
            var pusher = new Pusher("{{ env('PUSHER_KEY') }}", {
                cluster: 'eu'
            });
        </script>
    </head>
    @if(Auth::user()->getSettings("sidebar_collapsed") == true && $agent->isMobile() == false)
    <body class="row-fluid min-mode">
    @else
    <body class="row-fluid">
    @endif
        <aside id="sidebar">
            @include('layouts.sidebar')
        </aside>
        <section>
            <header>
                @include('layouts.header')
            </header>
            <article>
                @yield('content')
            </article>
        </section>
        <script>
            var channel_app = pusher.subscribe('app-channel');
        </script>
        <script src="/javascript/panel.js"></script>
    </body>
</html>
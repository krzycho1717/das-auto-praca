@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('service.Service'), $errors, session('message'), true, [
            [
                    'name' => Lang::get('ui.edit'),
                    'url' => URL::action('ServiceController@edit', [$service->client, $service->id])
            ],
        ] ) }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('client_id', Lang::get('client.Client')) }}
                    {{ Form::text('client_id', $service->client->fingerprint(), ['class' => 'form-control input-link', 'readonly' => 'readonly', 'data-link' => URL::action('ClientController@show', $service->client->id)]) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                    {{ Form::label('content', Lang::get('table.content')) }}
                    {{ Form::textarea('content', $service->content, ['class' => 'form-control', 'placeholder' => Lang::get('table.content'), 'readonly' => 'readonly']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('charge', Lang::get('table.charge')) }}
                    {{ Form::number('charge', $service->charge, ['class' => 'form-control', 'placeholder' => Lang::get('table.charge'), 'readonly' => 'readonly', 'min' => '0.0', 'step' => '0.1']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('date', Lang::get('table.performed')) }}
                    {{ Form::date('date', $service->date, ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'readonly' => 'readonly']) }}
                </div>
            </div>
        </div>
        {{ Html::panel_close() }}
    </div>

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="content"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            },
            readonly: 1
        });
    </script>

@stop
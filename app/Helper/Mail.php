<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 11.11.2016
 * Time: 21:48:26
 */

namespace App\Helper;

use Lang;

class Mail
{

    public static function send($view, $user_email, $subject){

        $from = new \SendGrid\Email(null, "admin@das-auto.herokuapp.com");
        $to = new \SendGrid\Email(null, $user_email);
        $content = new \SendGrid\Content("text/html", $view);
        $mail = new \SendGrid\Mail($from, $subject, $to, $content);
        $mail->setTemplateId('90e74053-83df-4524-9027-c718379af28c');

        $personalization = new \SendGrid\Personalization();
        $personalization->addTo($to);
        $personalization->addSubstitution('-template.welcome-', Lang::get('ui.email.welcome'));
        $personalization->addSubstitution('-template.no_reply-', Lang::get('ui.email.no_reply'));
        $mail->addPersonalization($personalization);

        $apiKey = getenv('SENDGRID_API_KEY');
        $sg = new \SendGrid($apiKey);

        $response = null;
        try{
            $response = $sg->client->mail()->send()->post($mail);
        }catch (Exception $e){
            return false;
        }

        if($response == null || $response->statusCode() != 202)
            return false;
        return true;
    }
}
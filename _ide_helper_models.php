<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
    /**
     * App\Models\ActionLog
     *
     * @property integer $id
     * @property integer $user_id
     * @property string $type
     * @property string $affected
     * @property string $extra
     * @property string $from_ip
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \App\Models\User $user
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereUserId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereType($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereAffected($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereExtra($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereFromIp($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\ActionLog whereUpdatedAt($value)
     */
    class ActionLog extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Client
     *
     * @property integer $id
     * @property integer $user_id
     * @property string $name
     * @property string $surname
     * @property string $phone
     * @property string $email
     * @property string $image
     * @property string $address
     * @property string $city
     * @property string $country
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \App\Models\User $user
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $services
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereUserId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereName($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereSurname($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client wherePhone($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereEmail($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereImage($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereAddress($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCity($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCountry($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereUpdatedAt($value)
     */
    class Client extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Review
     *
     * @property integer $id
     * @property integer $user_id
     * @property integer $client_id
     * @property string $vin
     * @property string $reg
     * @property integer $mileage
     * @property string $comment
     * @property float $tiresFrontLeft
     * @property float $tiresFrontRight
     * @property float $tiresBackLeft
     * @property float $tiresBackRight
     * @property string $n1
     * @property string $n2
     * @property string $n3
     * @property string $n4
     * @property string $n5
     * @property string $n6
     * @property string $n7
     * @property string $n8
     * @property string $n9
     * @property string $n10
     * @property string $n11
     * @property string $n12
     * @property string $n13
     * @property string $n14
     * @property string $n15
     * @property string $n16
     * @property string $n17
     * @property string $n18
     * @property string $n19
     * @property string $n20
     * @property string $n21
     * @property string $n22
     * @property string $n23
     * @property string $n24
     * @property string $n25
     * @property string $n26
     * @property string $n27
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \App\Models\Client $client
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereUserId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereClientId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereVin($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereReg($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereMileage($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereComment($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereTiresFrontLeft($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereTiresFrontRight($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereTiresBackLeft($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereTiresBackRight($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN1($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN2($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN3($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN4($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN5($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN6($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN7($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN8($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN9($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN10($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN11($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN12($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN13($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN14($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN15($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN16($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN17($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN18($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN19($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN20($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN21($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN22($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN23($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN24($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN25($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN26($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereN27($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereUpdatedAt($value)
     */
    class Review extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Service
     *
     * @property integer $id
     * @property integer $user_id
     * @property integer $client_id
     * @property string $content
     * @property float $charge
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \App\Models\Client $client
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereUserId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereClientId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereContent($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereCharge($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Service whereUpdatedAt($value)
     */
    class Service extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Token
     *
     * @property integer $id
     * @property integer $user_id
     * @property string $name
     * @property string $token
     * @property string $udid
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \App\Models\User $user
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereUserId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereName($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereToken($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereUdid($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\Token whereUpdatedAt($value)
     */
    class Token extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\User
     *
     * @property integer $id
     * @property string $name
     * @property string $surname
     * @property string $email
     * @property string $password
     * @property string $phone
     * @property string $settings
     * @property string $locale
     * @property string $image
     * @property string $visitor
     * @property string $remember_token
     * @property string $seen_at
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Token[] $tokens
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActionLog[] $actionLogs
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $services
     * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereSurname($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePhone($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereSettings($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereLocale($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereImage($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereVisitor($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereSeenAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
     * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
     */
    class User extends \Eloquent {}
}

namespace App{
    /**
     * App\Task
     *
     */
    class Task extends \Eloquent {}
}


<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 27.03.2016
 * Time: 13:28
 */

namespace App\Helper;

class Vin
{
    protected static function transliterate ($c) {
        return strpos('0123456789.ABCDEFGH..JKLMN.P.R..STUVWXYZ', $c) % 10;
    }

    protected static function get_check_digit ($vin) {
        $map = '0123456789X';
        $weights = '8765432X098765432';
        $sum = 0;
        for ($i = 0; $i < 17; ++$i)
            $sum += Vin::transliterate($vin[$i]) * strpos($map, $weights[$i]);
        return $map[$sum % 11];
    }

    public static function validate ($vin) {
        if (strlen($vin) !== 17) return false;
        return Vin::get_check_digit($vin) === $vin[8];
    }

}
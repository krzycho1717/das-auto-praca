<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Service Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'Service' => 'Usługa',
    'service' => 'usługa',
    'Services' => 'Usługi',
    'services' => 'usługi',
    'gen' => 'usług',
    'add' => 'dodaj usługę',
    'latest' => 'Usługi - ostatnio dodane',
    'edit_title' => 'Usługa - edycja',
    'create_title' => 'Nowa usługa',
    'empty' => 'brak usług',
];

<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;

class LoginEventListener
{
    protected $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->user->visitor = $_SERVER['REMOTE_ADDR'];
        $event->user->seen_at = Carbon::now();
        $event->user->save();
    }
}

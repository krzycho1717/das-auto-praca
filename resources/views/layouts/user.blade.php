<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>DasAuto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <meta name="token" content="{{ Session::token() }}">

        <!-- Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,700,400&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="/css/app.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="/css/bootstrap.css">
        <!-- Material design icons font -->
        <link rel="stylesheet" href="/css/materialdesignicons.css" media="all"  type="text/css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <body style="display: table;width: 100%;">
        <div class="container pl0 pr0" style="display: table-cell;vertical-align: middle;">
            @yield('content')
        </div>
    </body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 10.08.2016
 * Time: 17:57
 */

namespace App\Listeners;

use App\Events\ReadModel;
use App\Models\ActionLog;
use App\Models\Review;
use Auth;
use Session;

class ReviewsEventListener
{
    /**
     * ReviewsEventListener constructor.
     */
    public function __construct()
    {
    }

    public function handleOnCreated(Review $review){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "CREATE";
        $log->affected = "reviews";
        $log->extra = json_encode(array(
            'id' => $review->id
        ));
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();

        Session::set('reviews_number', Auth::user()->statistics()->reviews_count);
    }

    public function handleOnRead(ReadModel $readModel){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "READ";
        $log->affected = "reviews";
        $log->extra = json_encode(array(
            'id' => $readModel->model->id
        ));
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
    }

    public function handleOnUpdated(Review $review){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "UPDATE";
        $log->affected = "reviews";
        $log->extra = json_encode(array(
            'id' => $review->id
        ));
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
    }

    public function handleOnDeleted(Review $review){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "DELETE";
        $log->affected = "reviews";
        $log->extra = $review->toJson();
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();

        Session::set('reviews_number', Auth::user()->statistics()->reviews_count);
    }
}

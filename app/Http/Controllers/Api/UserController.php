<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Helper\PusherManager;
use App;
use Auth;
use JsonPackage;
use Validator;
use Input;
use Lang;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function wall($type="json"){
        $clients = Auth::user()->clients()->orderBy('created_at', 'desc')->take(5)->get();
        $services = Auth::user()->services()->orderBy('created_at', 'desc')->take(5)->with('client')->get();
        $reviews = Auth::user()->reviews()->orderBy('created_at', 'desc')->take(5)->with('client')->get();

        $clients_number = Auth::user()->clients()->count();
        $services_number = Auth::user()->services()->count();
        $reviews_number = Auth::user()->reviews()->count();

        $result = array(
            'latest_clients' => $clients,
            'latest_services' => $services,
            'latest_reviews' => $reviews,
            'clients_number' => $clients_number,
            'services_number' => $services_number,
            'reviews_number' => $reviews_number,
        );

        return JsonPackage::prepare($type, $result, null);
    }

    /**
     * Activate device by exchanging login & password with token & device id
     * @param string $type
     * @return \Illuminate\Http\Response
     */
    public function activate($type="json"){
        $rules = array(
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:4',
            'token' => 'required|token',
            'udid' => 'required|unique_device_id'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );
            if (Auth::attempt($userdata)) {
                $token = Auth::user()->tokens()->where('token', Input::get('token'))->first();
                if($token != null){
                    $token->udid = Input::get('udid');
                    $token->save();

                    PusherManager::trigger('token-channel', 'activated-event', ['success' => true, 'token' => $token->toArray()]);

                    return JsonPackage::prepare($type, Auth::user(), null);
                }else{
                    return JsonPackage::prepare($type, null, 'no token found - failed');
                }
            }else{
                return JsonPackage::prepare($type, null, 'auth failed');
            }
        }
    }

    public function api_handshake($type="json"){
        $user = Auth::user();
        if ($user != null){
            PusherManager::trigger('app-channel', 'logged-in-event', ['success' => true]);
        }
        return JsonPackage::prepare($type, $user, null);
    }

    /**
     * Deactivates device
     *
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function deactivate($type="json"){
        $rules = array(
            'udid'  => 'required|unique_device_id|exists:tokens,udid'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {
            $token = Auth::user()->tokens()->where("udid", Input::get("udid"))->first();
            if($token != null){
                $token->udid = null;
                $token->save();
                PusherManager::trigger('app-channel', 'logged-out-event', ['success' => true]);
                return JsonPackage::prepare('json', $token, null);
            }else{
                return JsonPackage::prepare('json', null, 'Not found');
            }
        }
    }

    public function search($raw_type, $keyword, $type="json"){
        $model = ['client' => 0, 'service' => 1, 'review' => 2][$raw_type];
        $nType = array(
            Lang::get('client.clients'),
            Lang::get('service.services'),
            Lang::get('review.reviews')
        );
        switch($model){
            case 0:
                $clients = Auth::user()->clients()->where(function ($query) use ($keyword){
                        $query->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    });
                return JsonPackage::prepare($type, $clients->get()->toArray(), null);
            case 1:
                $services = Auth::user()->services()->whereHas('client', function($q) use (&$keyword) {
                    $q->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orWhere(DB::raw('LOWER(content)'), 'LIKE', '%'. strtolower($keyword) .'%');

                return JsonPackage::prepare($type, $services->with('client')->get()->toArray(), null);
                break;
            case 2:
                $reviews = Auth::user()->reviews()->whereHas('client', function($q) use (&$keyword) {
                    $q->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orWhere(DB::raw('LOWER(vin)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orWhere(DB::raw('LOWER(reg)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orWhere(DB::raw('LOWER(comment)'), 'LIKE', '%'. strtolower($keyword) .'%');

                return JsonPackage::prepare($type, $reviews->with('client')->get()->toArray(), null);
                break;
            default:
                abort(404);
                break;
        }
    }
    

}

<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 10.08.2016
 * Time: 18:35
 */

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Lang;
use Redirect;
use Auth;
use View;

class ActionLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $result = Auth::user()->actionLogs()->orderBy('created_at', 'desc')->get();

        return View::make('page.panel.log.index')->with('logs', $result);
    }
}
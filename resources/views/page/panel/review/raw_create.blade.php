@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('review.create_title'), $errors, session('message')) }}
        {{ Form::open(['action' => ['ReviewController@raw_store'], 'method' => 'POST']) }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('client_id') ? 'has-error' : '' }}">
                    {{ Form::label('client_id', Lang::get('client.Client')) }}
                    <select name="client_id" id="client_id" class="form-control" required>
                        <option value="-1">{{ Lang::get('client.select') }}</option>
                        @foreach($clients as $c)
                            <option value="{{ $c->id }}">{{ $c->fingerprint() }}</option>
                        @endforeach
                    </select>
                    <span class="text-danger">{{ $errors->first('client_id') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('plate') ? 'has-error' : '' }}">
                    {{ Form::label('plate', Lang::get('table.plate')) }}
                    {{ Form::text('plate', Input::old('plate'), ['class' => 'form-control', 'placeholder' => Lang::get('table.plate'), 'required' => 'required']) }}
                    <span class="text-danger">{{ $errors->first('plate') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('vin') ? 'has-error' : '' }}">
                    {{ Form::label('vin', Lang::get('table.vin')) }}
                    {{ Form::text('vin', Input::old('vin'), ['class' => 'form-control', 'placeholder' => Lang::get('table.vin'), 'required' => 'required']) }}
                    <span class="text-danger">{{ $errors->first('vin') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('mileage') ? 'has-error' : '' }}">
                    {{ Form::label('mileage', Lang::get('table.mileage')) }}
                    <div class="input-group">
                        {{ Form::number('mileage', Input::old('mileage'), ['min' => 0, 'step' => 0, 'class' => 'form-control', 'placeholder' => Lang::get('table.mileage'), 'required' => 'required']) }}
                        <span class="input-group-addon">km</span>
                    </div>
                    <span class="text-danger">{{ $errors->first('mileage') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                    {{ Form::label('date', Lang::get('table.performed')) }}
                    {{ Form::date('date', Input::old('date') ? Input::old('date') : Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'required' => 'required']) }}
                    <span class="text-danger">{{ $errors->first('date') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('car') ? 'has-error' : '' }}">
                    {{ Form::label('car', Lang::get('table.auto')) }}
                    {{ Form::text('car', Input::old('car'), ['class' => 'form-control', 'placeholder' => Lang::get('table.auto'), 'required' => 'required']) }}
                    <span class="text-danger">{{ $errors->first('car') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                    <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                    {{ Form::label('comment', Lang::get('table.comment')) }}
                    {{ Form::textarea('comment', Input::old('comment'), ['class' => 'form-control', 'placeholder' => Lang::get('table.comment'), 'novalidate' => 'novalidate']) }}
                    <span class="text-danger">{{ $errors->first('comment') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('og1L') ? 'has-error' : '' }}">
                    {{ Form::label('og1L', Lang::get('table.og1L')) }}
                    <div class="input-group">
                        {{ Form::number('og1L', Input::old('og1L') ? Input::old('og1L') : '0.00', ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og1L'), 'required' => 'required']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                    <span class="text-danger">{{ $errors->first('og1L') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('og1P') ? 'has-error' : '' }}">
                    {{ Form::label('og1P', Lang::get('table.og1P')) }}
                    <div class="input-group">
                        {{ Form::number('og1P', Input::old('og1P') ? Input::old('og1P') : '0.00', ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og1P'), 'required' => 'required']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                    <span class="text-danger">{{ $errors->first('og1P') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('og2L') ? 'has-error' : '' }}">
                    {{ Form::label('og2L', Lang::get('table.og2L')) }}
                    <div class="input-group">
                        {{ Form::number('og2L', Input::old('og2L') ? Input::old('og2L') : '0.00', ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og2L'), 'required' => 'required']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                    <span class="text-danger">{{ $errors->first('og2L') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('og2P') ? 'has-error' : '' }}">
                    {{ Form::label('og2P', Lang::get('table.og2P')) }}
                    <div class="input-group">
                        {{ Form::number('og2P', Input::old('og2P') ? Input::old('og2P') : '0.00', ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og2P'), 'required' => 'required']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                    <span class="text-danger">{{ $errors->first('og2P') }}</span>
                </div>
            </div>
        </div>
        @for($i = 1; $i <= 27; $i++)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('n'.$i) ? 'has-error' : '' }}">
                        {{ Form::label('n'.$i, Lang::get('table.n'.$i)) }}
                        {{ Form::text('n'.$i, Input::old('n'.$i), ['class' => 'form-control', 'placeholder' => Lang::get('table.n'.$i)]) }}
                        <span class="text-danger">{{ $errors->first('n'.$i) }}</span>
                    </div>
                </div>
            </div>
        @endfor
        <div class="row">
            <div class="col-md-12">
                {{ Form::submit(Lang::get('ui.save'), ['class' => 'btn btn-success pull-right']) }}
            </div>
        </div>
        {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>
    <script src="/javascript/jquery.inputmask.bundle.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="comment"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[name="vin"]').inputmask("*{17}");
        });
    </script>
@stop
<h4>{{ Lang::get('ui.email.activation') }}</h4>
<div style="text-align: center">
    <a class="btn btn-success" href="{{ Config::get('app.url').'/verify/'.$user->email.'/'.$user->verification_hash }}">{{ Lang::get('ui.email.verify') }}</a>
</div>
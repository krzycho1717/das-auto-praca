<div class="row">

    <h2>{{ Lang::get('service.Services') }}</h2>

    <ul style="list-style: none">
        @if($services->count() == 0)
            <li><h3>Brak danych z tego okresu</h3></li>
        @else
        @foreach($services as $service)
            <li>{{ $service->date }}<div class="pull-right">{{ $service->charge }} zł.</div>
                <p>
                    {{ $service->content }}
                </p>
            </li>
            @endforeach
        @endif
    </ul>

</div>
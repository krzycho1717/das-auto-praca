<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    protected $table = 'reviews';
    protected $fillable = ['date', 'vin', 'reg', 'mileage', 'comment', 'tiresFrontLeft', 'tiresFrontRight', 'tiresBackLeft', 'tiresBackRight',
                           'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9', 'n10', 'n11', 'n12', 'n13', 'n14', 'n15', 'n16', 'n17', 'n18',
                           'n19', 'n20', 'n21', 'n22', 'n23', 'n24', 'n25', 'n26', 'n27'];

    protected static $allowed_to_order = ['name', 'surname', 'date', 'vin', 'reg', 'mileage'];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public static function canOrderBy($attribute)
    {
        return in_array($attribute, self::$allowed_to_order);
    }

    // default date format for created_at
    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    // default date format for updated_at
    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}

?>
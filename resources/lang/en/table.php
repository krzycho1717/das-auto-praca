<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'address'   => 'Address',
    'auto'      => 'Car',
    'charge'    => 'Charge',
    'city'      => 'City',
    'comment'   => 'Comment',
    'content'   => 'Content',
    'country'   => 'Country',
    'date'      => 'Date',
    'created_at' => 'Created at',
    'performed' => 'Performed at',
    'email'     => 'Email',
    'password'  => 'Password',
    'repeat_password'  => 'Repeat password',
    'mileage'   => 'Mileage',
    'name'      => 'Name',
    'name_and_surname' => 'Name and surname',
    'surname_and_name' => 'Surname and name',
    'og1L'      => 'Tires front left side',
    'og1P'      => 'Tires front right side',
    'og2L'      => 'Tires rear left side',
    'og2P'      => 'Tires rear right side',
    'phone'     => 'Phone',
    'plate'     => 'Number plate',
    'surname'   => 'Surname',
    'vin'       => 'VIN',
    'n1' => 'Replaced oil and oil filter',
    'n2' => 'Replace or clean the air filter',
    'n3' => 'Replace or clean the pollen filter',
    'n4' => 'Replace fuel filter or dehydration.',
    'n5' => 'Check condition of auxiliary belt',
    'n6' => 'Checking the brake system "front" cleaning or replacement',
    'n7' => 'Checking the brake system "back" cleaning or replacement',
    'n8' => 'Check brake pipes and fuel',
    'n9' => 'Check the tightness of the engine and gearbox',
    'n10' => 'Check wiper glasses install the washer nozzle',
    'n11' => 'checking the clutch pedal',
    'n12' => 'Checking locks and lubrication of hinges and door stops, hinges, tailgate',
    'n13' => 'Check the oil level in the system power steering',
    'n14' => 'Check headlight setting and following the lighting of the missing',
    'n15' => 'Checking, adjustment coolant level - replace',
    'n16' => 'Check the water pump.',
    'n17' => 'Check the front suspension(king pins, steering reaction, welded connections and screwed)',
    'n18' => 'Remote control battery replacement',
    'n19' => 'Check rear suspension',
    'n20' => 'Check condition of the tires and air pressure',
    'n21' => 'Replacing the spark plug',
    'n22' => 'Check the power steering',
    'n23' => 'Check the operation of the engine.',
    'n24' => 'check the status of protective rubber on the hinges',
    'n25' => 'Checking tightness of exhaust system',
    'n26' => 'Check the battery charging system',
    'n27' => 'Check oil in gear box or replacing'
];

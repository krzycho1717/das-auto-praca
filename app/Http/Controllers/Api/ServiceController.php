<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use \App\Models\Service;
use App;
use Auth;
use JsonPackage;
use Validator;
use Input;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  string $type
     * @param int $limit
     * @return \Illuminate\Http\Response
     */
    public function index($type="json", $limit=0)
    {
        if($limit > 0)
            return JsonPackage::prepare($type, Auth::user()->services()->orderBy('created_at', 'desc')->take(intval($limit))->with('client')->get()->toArray(), intval($limit));
        else
            return JsonPackage::prepare($type, Auth::user()->services()->with('client')->get()->toArray(), null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  string $type
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function store($type="json")
    {

        $rules = array(
            'client_id'  => 'required|exists:clients,id',
            'content'    => 'required',
            'charge'     => 'required',
            'date'       => 'required|date',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {
            $service = new Service();
            $service->user_id = Auth::user()->id;
            $service->client_id = Input::get('client_id');
            $service->content = Input::get('content');
            $service->charge = Input::get('charge');
            $service->date = Input::get('date');
            $service->save();

            return JsonPackage::prepare($type, null, null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type='json')
    {
        $result = Auth::user()->services()->with('client')->find($id);

        if($result == null){
            return JsonPackage::prepare($type, $result, null, 404);
        }
        return JsonPackage::prepare($type, $result, null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function update($id, $type="json")
    {
        $result = Auth::user()->services()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, $result, null, 404);
        }

        $rules = array(
            'client_id'  => 'required|exists:clients,id',
            'content'    => 'required',
            'charge'     => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {

            $userdata = array(
                'client_id' => Input::get('client_id'),
                'content' => Input::get('content'),
                'charge' => Input::get('charge')
            );

            $result->update($userdata);
            return JsonPackage::prepare($type, null, null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $type="json")
    {
        $result = Auth::user()->services()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, null, null, 404);
        }

        $result->delete();
        return JsonPackage::prepare($type, null, null);
    }
}

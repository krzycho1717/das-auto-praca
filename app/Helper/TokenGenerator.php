<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 02.07.2016
 * Time: 12:02
 */

namespace App\Helper;

use Auth;
/**
 * Class TokenGenerator
 * Generates token for API Authentication
 * @package App\Helper
 */
class TokenGenerator {

    public static function make($length = 16){
        $t = strtoupper(bin2hex(openssl_random_pseudo_bytes($length)));

        if(self::token_exists($t)){
            return self::make($length);
        }
        return $t;
    }

    private static function token_exists($token){
        return Auth::user()->tokens()->where('token', '=', $token)->exists();
    }
} 
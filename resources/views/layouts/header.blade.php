<div class="clearfix search-box">
    <div class="col-md-6 col-xs-12 column">
        <div class="input-group">
            @if(isset($keyword) && $keyword != null)
                <input type="text" id="keyword" class="form-control" required="required" name="keyword" value="{{ $keyword }}">
            @else
                <input type="text" id="keyword" class="form-control" required="required" name="keyword" placeholder="{{ Lang::get('ui.search_placeholder') }}">
            @endif
            <div class="input-group-btn">
                <button id="select-button" style="width: 105px" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ (isset($typeLang) && $typeLang != null) ? $typeLang : Lang::get('client.clients') }}
                    <span class="caret ml5"></span>
                </button>

                <ul id="search_filter" class="dropdown-menu" data-selected="{{ (isset($typeRaw) && $typeRaw != null) ? $typeRaw : 'client' }}">
                    <li class="select-type" data-value="client"><a><i class="mdi mdi-clipboard-account mr5"></i><span>{{ Lang::get('client.clients') }}</span></a></li>
                    <li class="select-type" data-value="service"><a><i class="mdi mdi-clipboard-outline mr5"></i><span>{{ Lang::get('service.services') }}</span></a></li>
                    <li class="select-type" data-value="review"><a><i class="mdi mdi-calendar-check mr5"></i><span>{{ Lang::get('review.reviews') }}</span></a></li>
                </ul>
                <button id="search_button" class="btn btn-primary">{{ Lang::get('ui.search') }}</button>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12 column">
        <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-18px mdi-plus"></i>{{ Lang::get('ui.add') }}</button>
        <ul class="dropdown-menu mt5">
            <li><a href="{{ URL::action('ClientController@create') }}"><i class="mdi mdi-clipboard-account mr5"></i><span>{{ Lang::get('client.client') }}</span></a></li>
            <li><a href="{{ URL::action('ServiceController@raw_create') }}"><i class="mdi mdi-clipboard-outline mr5"></i><span>{{ Lang::get('service.service') }}</span></a></li>
            <li><a href="{{ URL::action('ReviewController@raw_create') }}"><i class="mdi mdi-calendar-check mr5"></i><span>{{ Lang::get('review.review') }}</span></a></li>
        </ul>
        <div class="btn-group pull-right">
            @if(Cookie::get('applocale') == 'pl')
            <a class="btn btn-success active" href="{{ URL::to('/change-locale/pl') }}"><img src="/images/pl.png" width="24"/></a>
            <a class="btn btn-default" href="{{ URL::to('/change-locale/en') }}"><img src="/images/gb.png" width="24"/></a>
            @else
            <a class="btn btn-default" href="{{ URL::to('/change-locale/pl') }}"><img src="/images/pl.png" width="24"/></a>
            <a class="btn btn-success active" href="{{ URL::to('/change-locale/en') }}"><img src="/images/gb.png" width="24"/></a>
            @endif
        </div>
        <div class="pull-right badge p5 mr10 mt5" id="message-login" style="display: none">
            zalogowano do aplikacji - <span><b></b></span>
        </div>
        <div class="pull-right badge p5 mr10 mt5" id="message-logout" style="display: none">
            wylogowano z aplikacji - <span><b></b></span>
        </div>
    </div>
</div>
<link rel="stylesheet" href="/css/transition.css"/>
<script src="/javascript/transition.js"></script>
<link rel="stylesheet" href="/css/popup.css"/>
<script src="/javascript/popup.js"></script>

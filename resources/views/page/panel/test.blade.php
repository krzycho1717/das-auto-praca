@extends('layouts.user')
@section('content')



    <?php
    $to       = 'w50364@student.rzeszow.pl';
    $subject  = 'Testing sendmail.exe';
    $message  = 'Hi, you just received an email using sendmail!';
    $headers  = 'From: krzycho1717@gmail.com' . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=utf-8';
    if(mail($to, $subject, $message, $headers))
        echo "Email sent";
    else
        echo "Email sending failed";
    ?>

    <style>

        body {
            font-family: arial;
        }

        table {
            border: 1px solid #ccc;
            width: 100%;
            margin:0;
            padding:0;
            border-collapse: collapse;
            border-spacing: 0;
        }

        table tr {
            border: 1px solid #ddd;
            padding: 5px;
        }

        table th, table td {
            padding: 10px;
            text-align: center;
        }

        table th {
            text-transform: uppercase;
            font-size: 14px;
            letter-spacing: 1px;
        }

        @media screen and (max-width: 600px) {

            table {
                border: 0;
            }

            table thead {
                display: none;
            }

            table tr {
                margin-bottom: 10px;
                display: block;
                border-bottom: 2px solid #ddd;
            }

            table td {
                display: block;
                text-align: right;
                font-size: 13px;
                border-bottom: 1px dotted #ccc;
            }

            table td:last-child {
                border-bottom: 0;
            }

            table td:before {
                content: attr(data-label);
                float: left;
                text-transform: uppercase;
                font-weight: bold;
            }
        }

        .row:nth-child(even){
            background-color: #0a97b9;
        }
    </style>

    <table class="table">
        <thead>
        <tr>
            <th>Content</th>
            <th>Charge</th>
            <th>Created at</th>
        </tr>
        </thead>
        <tbody>
        @foreach($test_value as $item)
        <tr>
            <td data-label="Content">{{ $item->content }}</td>
            <td data-label="Charge">{{ $item->charge }}</td>
            <td data-label="Created at">{{ $item->created_at }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>


{{--{{ \App\Helper\Log::d('$test_value', $test_value) }}--}}

@stop
@extends('layouts.user')
@section('content')

    <div class="login-panel panel panel-default plain animated bounceIn">
        <!-- Start .panel -->
        <div class="panel-heading">
            <h4 class="panel-title text-center">
                <img id="logo" src="/images/logo2.png" alt="DasAuto logo">
            </h4>
        </div>
        <div class="panel-body text-center">
            <h5 class="text-center">{{ Lang::get('ui.email.verify_success') }}</h5>
            <a class="btn btn-primary" href="{{ URL::to('/') }}">{{ Lang::get('auth.sign_in') }}</a>
        </div>
    </div>

@stop
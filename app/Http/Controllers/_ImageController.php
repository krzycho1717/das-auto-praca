<?php

namespace App\Http\Controllers;

use File;

class ImageController extends Controller
{
    public function user($name){
        $path = public_path() .'/uploads/users/'.$name;
        $type = "image/jpeg";
        header('Content-Type:'.$type);
        if(File::exists($path)){
            readfile($path);
        }else{
            readfile(public_path() .'/uploads/default.png');
        }
    }

    public function client($name){
        $path = public_path() .'/uploads/clients/'.$name;
        $type = "image/jpeg";
        header('Content-Type:'.$type);
        if(File::exists($path)){
            readfile($path);
        }else{
            readfile(public_path() .'/uploads/default.png');
        }
    }

    public function pub($name){
        $path = base_path('public/images/'.$name);
        $type = "image/jpeg";
        header('Content-Type:'.$type);
        readfile($path);
    }
}

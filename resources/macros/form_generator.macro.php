<?php

Form::macro('generate_form', function($configuration, $action){
    $settings = array(
        'form_settings' => array(
            'method' => 'POST',
            'autocomplete' => 'off'
        ),
        'form_class' => 'form-horizontal group-border stripped mt0 col-md-12',
        'input_container_class' => 'col-md-7',
        'input_class' => 'form-control',
        'label_class' => '',
        'buttons' => array(
            0 => new \App\Helper\Button(\Illuminate\Support\Facades\Lang::get('ui.save'), 'submit')
        ),
        'controls' => array()
    );

    $default_control = array(
        'input_type' => 'text',
        'input_attr' => array(),
        'input_name' => '',
        'input_value' => '',
        'input_placeholder' => '',
        'input_addon' => false,
        'input_addon_value' => '',
        'input_addon_dir' => 'right'
    );

    $settings = array_merge($settings, $configuration);
    $settings['form_settings']['class'] = $settings['form_class'];
    $settings['form_settings']['action'] = $action;

    $html = Form::open($settings['form_settings']);

    foreach($settings['controls'] as $control){
        $control = array_merge($default_control, $control);

        $html .= '<div class="form-group">';
        $html .= '<label for="reg" class="'.$settings['label_class'].'">'.$control['label_value'].'</label>';
        switch($control['input_type']){
            case 'textarea':
                $html .= Form::textarea(
                    $control['input_name'],
                    $control['input_value'],
                    array_merge(
                        array(
                            'class' => $settings['input_class'],
                            'rows' => 5,
                            'cols' => 10,
                        ),
                        $control['input_attr']
                    )
                );
                break;
            case 'select':
                $html .= Form::select(
                    $control['input_name'],
                    $control['input_value'],
                    0,
                    array_merge(
                        array(
                            'class' => $settings['input_class'],
                            'rows' => 5,
                            'cols' => 10
                        ),
                        $control['input_attr']
                    )
                );
                break;
            default:
                if($control['input_addon']){
                    $html .= '<div class="input-group">';
                    if($control['input_addon_dir'] == 'left')
                        $html .'<div class="input-group-addon">'.$control['input_addon_value'].'</div>';
                }
                $html .= Form::input(
                    $control['input_type'],
                    $control['input_name'],
                    $control['input_value'],
                    array_merge(
                        array(
                            'class' => $settings['input_class'],
                            'placeholder' => $control['input_placeholder'],
                        ),
                        $control['input_attr']
                    )

                );
                if($control['input_addon']){
                    if($control['input_addon_dir'] == 'right')
                        $html .'<div class="input-group-addon">'.$control['input_addon_value'].'</div>';
                    $html .= '</div>';
                }
                break;
        }
        $html .= '</div>';
    }

    foreach($settings['buttons'] as $button){
        if($button instanceof \App\Helper\Button){
            $html .= '<div class="form-group">';
            //$html .= '<div class="col-lg-12 col-md-12">';

            $html .= Form::button($button->getValue(), array('class' => $button->getClass(), 'type' => $button->getType()));

            //$html .= '</div>';
            $html .= '</div>';
        }else{
            $html .= '<div class="form-group">';
            //$html .= '<div class="col-lg-12 col-md-12">';
            foreach($settings['buttons'] as $button){
                if($button instanceof \App\Helper\Button){
                    $html .= Form::button($button->getValue(), array('class' => $button->getClass(), 'type' => $button->getType()));
                }
            }
            //$html .= '</div>';
            $html .= '</div>';
        }
    }
    $html .= Form::close();

    return new \Illuminate\Support\HtmlString($html);
});

?>

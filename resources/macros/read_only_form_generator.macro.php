<?php

Form::macro('generate_read_form', function($configuration){
    $settings = array(
        'form_class' => 'form-horizontal group-border stripped mt0 col-md-12',
        'input_container_class' => 'col-md-7',
        'input_class' => 'form-control',
        'label_class' => '',
        'buttons' => array(
            0 => new \App\Helper\Button(\Illuminate\Support\Facades\Lang::get('ui.save'), 'submit')
        ),
        'controls' => array()
    );

    $default_control = array(
        'input_type' => 'text',
        'input_attr' => array(),
        'input_name' => '',
        'input_value' => ''
    );

    $settings = array_merge($settings, $configuration);
    $settings['form_settings']['class'] = $settings['form_class'];

    $html = Form::open($settings['form_settings']);

    for($i = 0; $i < count($settings['controls']); $i++){
        $control = array_merge($default_control, $settings['controls'][$i]);
        if(!array_has($control['input_attr'], 'class'))
            $control['input_attr']['class'] = $settings['input_class'];

        $html .= '<div class="form-group">';
        $html .= '<label for="reg" class="' . $settings['label_class'] . '">' . $control['label_value'] . '</label>';
//        $html .= '<div class="' . $settings['input_container_class'] . '">';

        if ($control['input_type'] == 'textarea') {
            $html .= Form::textarea(
                $control['input_name'],
                $control['input_value'],
                array_merge(
                    array(
                        'class' => $settings['input_class'],
                        'rows' => 5,
                        'cols' => 10
                    ),
                    $control['input_attr']
                )
            );
        }elseif($control['input_type'] == 'select') {
            $html .= Form::select(
                $control['input_name'],
                $control['input_value'],
                null,
                array_merge(
                    array(
                        'rows' => 5,
                        'cols' => 10
                    ),
                    $control['input_attr']
                )
            );
        }else{
            $html .= Form::input(
                $control['input_type'],
                $control['input_name'],
                $control['input_value'],
                $control['input_attr']

            );
        }
        $html .= '</div>';
//        $html .= '</div>';
    }
    $html .= Form::close();

    return new \Illuminate\Support\HtmlString($html);
});

?>

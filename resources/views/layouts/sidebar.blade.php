@if($agent->isMobile() == false)
    @if(Auth::user()->getSettings("sidebar_collapsed") == true)
        <div class="toggle-sidebar"><i class="mdi mdi-arrow-right"></i></div>
        <div class="sidebar-seperator">{{ Lang::get('ui.profile.show') }}<span
                    class="pull-right toggle-sidebar hide hidden-sm hidden-xs"><i class="mdi mdi-arrow-left"></i></span>
        </div>
    @else
        <div class="toggle-sidebar hide"><i class="mdi mdi-arrow-right"></i></div>
        <div class="sidebar-seperator">{{ Lang::get('ui.profile.show') }}<span
                    class="pull-right toggle-sidebar hidden-sm hidden-xs"><i class="mdi mdi-arrow-left"></i></span>
        </div>
    @endif
@else
    <div class="sidebar-seperator">{{ Lang::get('ui.profile.show') }}</div>
@endif
<div class="profile-row">
    <a href="{{ URL::action('UserController@show') }}" class="p10">
        <span class="text-center image-column mt10 mb10">
                <img class="picture" src="/images/default.png">
        </span>
        <span class="text-center username-column ml10 mt10 mb10">
            {{ Auth::user()->fullname() }}
        </span>
    </a>
</div>
<div class="sidebar-seperator">{{ Lang::get('ui.navigation') }}</div>
<nav id="main">
    <ul class="m0">
        <li class="menu-item{{ Request::is('panel') ? ' active': '' }}"><a href="{{ URL::to('/') }}"><i
                        class="mdi mdi-home"></i><span>{{ Lang::get('ui.start') }}</span></a></li>
        <li class="menu-item{{ Request::is('panel/client') ? ' active': '' }}"><a href="{{ URL::to('/panel/client') }}">
                <i class="mdi mdi-clipboard-account"></i><span>{{ Lang::get('client.clients') }}</span>
                <span class="badge">{{ Session::get('clients_number') }}</span></a></li>
        <li class="menu-item{{ Request::is('panel/service') ? ' active': '' }}"><a
                    href="{{ URL::to('/panel/service') }}"><i
                        class="mdi mdi-clipboard-outline"></i><span>{{ Lang::get('service.services') }}</span><span
                        class="badge">{{ Session::get('services_number') }}</span></a></li>
        <li class="menu-item{{ Request::is('panel/review') ? ' active': '' }}"><a href="{{ URL::to('/panel/review') }}"><i
                        class="mdi mdi-calendar-check"></i><span>{{ Lang::get('review.reviews') }}</span><span
                        class="badge">{{ Session::get('reviews_number') }}</span></a></li>
        <li class="menu-item"><a href="{{ URL::to('/logout') }}"><i
                        class="mdi mdi-logout"></i><span>{{ Lang::get('ui.logout') }}</span></a></li>
    </ul>
</nav>
<a id="android-download" href="/">
    <div class="android-download-icon pull-left"><i class="mdi mdi-android mdi-36px"></i></div>
    <div class="pull-right text-center m0 p0" style="width: 113px;">
        <h5 class="m0">{{ Lang::get('ui.download_app') }}</h5>
        <h6>{{ Lang::get('ui.version') }}: 1.02</h6>
    </div>
</a>
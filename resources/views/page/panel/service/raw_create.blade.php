@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('service.create_title'), $errors, session('message')) }}
            {{ Form::open(['action' => ['ServiceController@raw_store'], 'method' => 'POST']) }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('client_id') ? 'has-error' : '' }}">
                            {{ Form::label('client_id', Lang::get('client.Client')) }}
                            <select name="client_id" id="client_id" class="form-control" required>
                                <option value="-1">{{ Lang::get('client.select') }}</option>
                                @foreach($clients as $c)
                                    <option value="{{ $c->id }}">{{ $c->fingerprint() }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger">{{ $errors->first('client_id') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                            <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                            {{ Form::label('content', Lang::get('table.content')) }}
                            {{ Form::textarea('content', Input::old('content'), ['class' => 'form-control', 'placeholder' => Lang::get('table.content'), 'novalidate' => 'novalidate']) }}
                            <span class="text-danger">{{ $errors->first('content') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('charge') ? 'has-error' : '' }}">
                            {{ Form::label('charge', Lang::get('table.charge')) }}
                            <div class="input-group">
                                {{ Form::number('charge', Input::old('charge'), ['class' => 'form-control', 'placeholder' => '0.0', 'required' => 'required', 'min' => '0.0', 'step' => '0.1']) }}
                                <span class="input-group-addon">zł</span>
                            </div>
                            <span class="text-danger">{{ $errors->first('charge') }}</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                            {{ Form::label('date', Lang::get('table.performed')) }}
                            {{ Form::date('date', Input::old('date') ? Input::old('date') : Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('date') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(Lang::get('ui.save'), ['class' => 'btn btn-success pull-right']) }}
                    </div>
                </div>
            {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="content"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            }
        });
    </script>

@stop
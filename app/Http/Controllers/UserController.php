<?php

namespace App\Http\Controllers;


use App;
use App\Models\Client;
use App\Models\Review;
use App\Models\Service;
use App\Models\User;
use Auth;
use DB;
use Hash;
use Input;
use JsonPackage;
use Lang;
use Mail;
use Redirect;
use Session;
use Validator;
use View;


class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check() || Auth::viaRemember()){
            return Redirect::to('/panel');
        }else{
            return View::make('page.user.index');
        }
    }

    public function verify_resend($email){
        $user = User::where('email', $email)->first();
        if($user){
            if(Mail::send(View::make('page.user.email_activation')->with('user', $user)->render(), $user->email))
                return View::make('page.user.verification_email_message')->with('email', $user->email);
            return View::make('page.user.verification_email_failure')->with('email', $user->email);
        }
        return Redirect::to('/');
    }

    public function verify($email, $hash){
        if(!empty($hash) && strlen($hash) == 32){
            $user = User::where('email', $email)->first();
            if($user && $user->verification_hash == $hash){
                $user->active = true;
                $user->verification_hash = null;
                $user->save();
                return View::make('page.user.verification_email_success');
            }else{
                return View::make('page.user.verification_email_failure')->with('email', $user->email);
            }
        }
        return Redirect::to('/');
    }

    public function login(){
        if(Auth::check() || Auth::viaRemember())
        {
            return Redirect::to('/panel');
        }else {
            $rules = array(
                'email'    => 'required|email',
                'password' => 'required|alphaNum|min:4',
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::to('/')
                    ->withErrors($validator) // send back all errors to the login form
                    ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
            } else {

                if(strcmp(Input::get('remember'), 'on') === 0){
                    // attempt to do the login
                    if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password'), 'active' => true], true)) {
                        Session::set('clients_number', Auth::user()->statistics()->clients_count);
                        Session::set('services_number', Auth::user()->statistics()->services_count);
                        Session::set('reviews_number', Auth::user()->statistics()->reviews_count);
                        return Redirect::to(Session::get('url.intended'));
                    } else {
                        // validation not successful, send back to form
                        return Redirect::to('/')
                            ->with('message', Lang::get('auth.failed_msg'));
                    }
                }else{
                    // attempt to do the login
                    if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password'), 'active' => true])) {
                        // if (!Cookie::has('applocale')){
                        //     Cookie::queue(Cookie::make('applocale', Auth::user()->locale, 2628000));
                        // }
                        Session::set('clients_number', Auth::user()->statistics()->clients_count);
                        Session::set('services_number', Auth::user()->statistics()->services_count);
                        Session::set('reviews_number', Auth::user()->statistics()->reviews_count);
                        return Redirect::to(Session::get('url.intended'));
                    } else {
                        // validation not successful, send back to form
                        return Redirect::to('/')
                            ->with('message', Lang::get('auth.failed_msg'));
                    }
                }

            }
        }
    }

    public function logout()
    {
        Auth::logout(); // log the user out of our application
        \Session::forget('applocale');
        return Redirect::to('/'); // redirect the user to the login screen
    }

    /*
     * Method for changing settings
     */
    public function setting(){
        $rules = array(
            'key'    => 'required',
            'value'    => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare('json', null, $validator->messages());
        } else {
            $key = Input::get('key');
            $value = Input::get('value');
            Auth::user()->setSettings($key, $value);
            Auth::user()->save();
            return JsonPackage::prepare('json', Auth::user()->getSettings($key), null);
        }
    }

    public function wall(){

        $clients = Auth::user()->clients()->orderBy('created_at', 'desc')->take(5)->get();
        $services = Auth::user()->services()->orderBy('created_at', 'desc')->take(5)->get();
        $reviews = Auth::user()->reviews()->orderBy('created_at', 'desc')->take(5)->get();

        $raw_chart_data = Auth::user()->average_per_moth();
        $chart_data = array();
        for ($i = 0; $i < count($raw_chart_data); $i++){
            $chart_data[$raw_chart_data[$i]->month-1] = $raw_chart_data[$i]->avg_charge;
        }

        return View::make('page.panel.index')
            ->with('chart_data', $chart_data)
            ->with('clients', $clients)
            ->with('services', $services)
            ->with('reviews', $reviews)
            ->with('clients_number', Session::get('clients_number', 0))
            ->with('services_number', Session::get('services_number', 0))
            ->with('reviews_number', Session::get('reviews_number', 0));
    }

    public function search_vs($raw_type, $attribute, $keyword){
        $type = ['client' => 0, 'service' => 1, 'review' => 2][$raw_type];
        $nType = array(
            Lang::get('client.clients'),
            Lang::get('service.services'),
            Lang::get('review.reviews')
        );

        switch($type){
            case 0:
                if(Client::canOrderBy($attribute)){
                    $clients = Auth::user()->clients()->where(DB::raw('LOWER(clients.'.$attribute.')'), 'LIKE', '%'. strtolower($keyword) .'%');

                    return View::make('page.panel.search')
                        ->with('keyword', $keyword)
                        ->with('typeLang', $nType[$type])
                        ->with('typeNum', $type)
                        ->with('typeRaw', $raw_type)
                        ->with('clients', $clients->paginate(25));
                }else{
                    abort(404);
                }
                break;
            case 1:
                if(Service::canOrderBy($attribute)) {
                    $services = Auth::user()->services()->where(DB::raw('LOWER(services.' . $attribute . ')'), 'LIKE', '%' . strtolower($keyword) . '%');

                    return View::make('page.panel.search')
                        ->with('keyword', $keyword)
                        ->with('typeLang', $nType[$type])
                        ->with('typeNum', $type)
                        ->with('typeRaw', $raw_type)
                        ->with('services', $services);
                }else{
                    abort(404);
                }
                break;
            case 2:
                if(Review::canOrderBy($attribute)) {
                    $reviews = Auth::user()->reviews()->where(DB::raw('LOWER(reviews.' . $attribute . ')'), 'LIKE', '%' . strtolower($keyword) . '%');

                    return View::make('page.panel.search')
                        ->with('keyword', $keyword)
                        ->with('typeLang', $nType[$type])
                        ->with('typeNum', $type)
                        ->with('typeRaw', $raw_type)
                        ->with('reviews', $reviews);
                }else{
                    abort(404);
                }
                break;
            default:
                break;
        }
    }

    public function search($raw_type, $keyword){
        $type = ['client' => 0, 'service' => 1, 'review' => 2][$raw_type];
        $nType = array(
            Lang::get('client.clients'),
            Lang::get('service.services'),
            Lang::get('review.reviews')
        );
        switch($type){
            case 0:
                $clients = Auth::user()->clients()->where(function ($query) use ($keyword){
                        $query->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%');
                        $query->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    });
                return View::make('page.panel.search')
                    ->with('keyword', $keyword)
                    ->with('typeLang', $nType[$type])
                    ->with('typeNum', $type)
                    ->with('typeRaw', $raw_type)
                    ->with('clients', $clients->paginate(25));
                break;
            case 1:
                $services = Auth::user()->services()->whereHas('client', function($q) use (&$keyword) {
                    $q->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orWhere(DB::raw('LOWER(content)'), 'LIKE', '%'. strtolower($keyword) .'%')->paginate(25);

                return View::make('page.panel.search')
                    ->with('keyword', $keyword)
                    ->with('typeLang', $nType[$type])
                    ->with('typeNum', $type)
                    ->with('typeRaw', $raw_type)
                    ->with('services', $services);

                break;
            case 2:
                $reviews = Auth::user()->reviews()->whereHas('client', function($q) use (&$keyword) {
                    $q->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orWhere(DB::raw('LOWER(vin)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orWhere(DB::raw('LOWER(reg)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orWhere(DB::raw('LOWER(comment)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->paginate(25);

                return View::make('page.panel.search')
                    ->with('keyword', $keyword)
                    ->with('typeLang', $nType[$type])
                    ->with('typeNum', $type)
                    ->with('typeRaw', $raw_type)
                    ->with('reviews', $reviews);

                break;
            default:
                break;
        }
    }

    public function search_ordered($raw_type, $keyword, $attribute, $order){
        $type = ['client' => 0, 'service' => 1, 'review' => 2][$raw_type];
        $nType = array(
            Lang::get('client.clients'),
            Lang::get('service.services'),
            Lang::get('review.reviews')
        );
        switch($type){
            case 0:
                if(!Client::canOrderBy($attribute)) return Redirect::action('UserController@search', ['type' => $type, 'keyword' => $keyword]);
                $clients = Auth::user()->clients()->where(function ($query) use ($keyword){
                    $query->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    $query->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    $query->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    $query->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    $query->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    $query->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%');
                    $query->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orderBy($attribute, $order)->paginate(25);
                return View::make('page.panel.search')
                    ->with('keyword', $keyword)
                    ->with('typeLang', $nType[$type])
                    ->with('typeNum', $type)
                    ->with('typeRaw', $raw_type)
                    ->with('clients', $clients);
                break;
            case 1:
                if(!Service::canOrderBy($attribute)) return Redirect::action('UserController@search', ['type' => $type, 'keyword' => $keyword]);
                $services = Auth::user()->services()->whereHas('client', function($q) use (&$keyword) {
                    $q->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orWhere(DB::raw('LOWER(content)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orderBy($attribute, $order)
                    ->paginate(25);

                return View::make('page.panel.search')
                    ->with('keyword', $keyword)
                    ->with('typeLang', $nType[$type])
                    ->with('typeNum', $type)
                    ->with('typeRaw', $raw_type)
                    ->with('services', $services);

                break;
            case 2:
                if(!Review::canOrderBy($attribute)) return Redirect::action('UserController@search', ['type' => $type, 'keyword' => $keyword]);
                $reviews = Auth::user()->reviews()->whereHas('client', function($q) use (&$keyword) {
                    $q->where(DB::raw('LOWER(clients.name)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.surname)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.email)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.phone)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.city)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.address)'), 'LIKE', '%'. strtolower($keyword) .'%')
                        ->orWhere(DB::raw('LOWER(clients.country)'), 'LIKE', '%'. strtolower($keyword) .'%');
                })->orWhere(DB::raw('LOWER(vin)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orWhere(DB::raw('LOWER(reg)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orWhere(DB::raw('LOWER(comment)'), 'LIKE', '%'. strtolower($keyword) .'%')
                    ->orderBy($attribute, $order)
                    ->paginate(25);

                return View::make('page.panel.search')
                    ->with('keyword', $keyword)
                    ->with('typeLang', $nType[$type])
                    ->with('typeNum', $type)
                    ->with('typeRaw', $raw_type)
                    ->with('reviews', $reviews);

                break;
            default:
                break;
        }
    }

    public function show_services(){
        $result = Auth::user()->services()
            ->orderBy('created_at', 'desc')
            ->paginate(25);
        return View::make('page.panel.service.index')
            ->with('services', $result);
    }

    public function show_reviews(){
        $result = Auth::user()->reviews()
            ->orderBy('created_at', 'desc')
            ->paginate(25);
        return View::make('page.panel.review.index')
            ->with('reviews', $result);
    }

    public function show(){
        $tokens = Auth::user()->tokens()->orderBy('created_at', 'asc')->get();
        return View::make('page.panel.settings.index')
            ->with('tokens', $tokens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('page.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $rules = array(
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|alphaNum|min:3|confirmed',
            'password_confirmation' => 'required|alphaNum|min:3'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/create')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            // create our user data for the authentication

            $userdata = array(
                'name' => Input::get('name'),
                'surname' => Input::get('surname'),
                'phone' => Input::get('phone'),
                'email'     => Input::get('email'),
                'password'  => Hash::make(Input::get('password')),
                'settings' => '{}',
                'seen_at' => \Carbon\Carbon::now(),
                'active' => false,
                'verification_hash' => strtoupper(bin2hex(openssl_random_pseudo_bytes(16)))
            );

            $newUser = User::create($userdata);

            if($newUser){
                //$message = 'Click <a href="http://das-auto.herokuapp.com/verify/'.$newUser->email.'/'.$newUser->verification_hash.'">here</a> to verify account';
                if(Mail::send(View::make('page.user.email_activation')->with('user', $newUser)->render(), $newUser->email, Lang::get('ui.email.subject.activation')))
                    return View::make('page.user.verification_email_message')->with('email', $newUser->email);
                return View::make('page.user.verification_email_failure')->with('email', $newUser->email);
            }else{
                return Redirect::to('/');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return View::make('page.panel.settings.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $userdata = array(
            'name' => Input::get('name'),
            'surname' => Input::get('surname'),
            'phone' => Input::get('phone'),
            'email'     => Input::get('email')
        );

        if(Input::has('password')){
            $userdata['password'] = Hash::make(Input::get('password'));
        }

        Auth::user()->update($userdata);

        return Redirect::to('/panel/settings');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Auth::user()->delete();
        return Redirect::to('/');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 10.08.2016
 * Time: 15:50
 */

namespace App\Listeners;

use App\Events\ReadModel;
use App\Models\ActionLog;
use App\Models\Service;
use Auth;
use Session;


class ServiceEventListener
{

    /**
     * ServiceEventListener constructor.
     */
    public function __construct()
    {
    }

    public function handleOnCreated(Service $service){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "CREATE";
        $log->affected = "services";
        $log->extra = $service->toJson();
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();

        Session::set('services_number', Auth::user()->statistics()->services_count);
    }

    public function handleOnRead(ReadModel $readModel){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "READ";
        $log->affected = "services";
        $log->extra = $readModel->model->toJson();
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
    }

    public function handleOnUpdated(Service $service){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "UPDATE";
        $log->affected = "services";
        $log->extra = $service->toJson();
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
    }

    public function handleOnDeleted(Service $service){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "DELETE";
        $log->affected = "services";
        $log->extra = $service->toJson();
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();

        Session::set('services_number', Auth::user()->statistics()->services_count);
    }
}
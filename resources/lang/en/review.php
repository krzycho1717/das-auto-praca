 <?php

return [

    /*
    |--------------------------------------------------------------------------
    | Review Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'Review' => 'Review',
    'review' => 'review',
    'Reviews' => 'Reviews',
    'reviews' => 'reviews',
    'gen' => 'reviews',
    'add' => 'add review',
    'latest' => 'Reviews - recently added',
    'edit_title' => 'Review - edit',
    'create_title' => 'New review',
    'empty' => 'no reviews',
    'next_expected' => 'Next expected review'

];

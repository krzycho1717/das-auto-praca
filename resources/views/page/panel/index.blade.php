@extends('layouts.default')
@section('content')

    <div class="modal fade modal-danger" id="delete-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <!-- Start .panel -->
                <div class="modal-header">
                    <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
                </div>
                <div class="modal-body">
                    {{ Lang::get('ui.confirm_delete.message') }}
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default delete-cancel inline" data-dismiss="modal" data-bb-handler="cancel">{{ Lang::get('ui.cancel') }}</a>
                    <form action="" method="post" id="delete-form" class="inline">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-primary" data-bb-handler="confirm" type="submit">{{ Lang::get('ui.ok') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 column">
        <!-- aktualnosci -->
        <div class="clearfix">
            <div class="panel panel-success plain">
                <div class="panel-body">
                    <div class="header-stats no-selection text-center">
                        <div class="spark clearfix no-selection">
                            <div class="spark-info"><span class="number">{{ $clients_number or '0' }}</span>{{ Lang::get('client.gen') }}</div>
                            <div id="spark-templateviews" class="sparkline"><img src="/images/chart.png"></div>
                        </div>
                        <div class="spark clearfix no-selection">
                            <div class="spark-info"><span class="number">{{ $services_number or '0' }}</span>{{ Lang::get('service.gen') }}</div>
                            <div id="spark-templateviews" class="sparkline"><img src="/images/chart.png"></div>
                        </div>
                        <div class="spark clearfix no-selection">
                            <div class="spark-info"><span class="number">{{ $reviews_number or '0' }}</span>{{ Lang::get('review.gen') }}</div>
                            <div id="spark-templateviews" class="sparkline"><img src="/images/chart.png"></div>
                        </div>
                    </div>
                    @if($services_number != null && $services_number != 0)
                    {{ Html::chart('earning-chart', array(
                        'type' => 'horizontalBar',
                        'data' => $chart_data
                    )) }}
                    @endif
                </div>
            </div>
        </div>
        <!-- clients -->
        <div class="clearfix">
            <div class="panel panel-default">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title"><i class="icon-user-tie"></i>{{ Lang::get('client.latest') }}</h4>
                </div>
                @if(!CheckVar::c_array($clients))
                    <div class="panel-body pl0 pr0">
                        <div class="bs-callout bs-callout-info">
                            <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('client.empty') }}</h4>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <a href="{{ URL::action('ClientController@create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                    </div>
                @else
                    <div class="panel-body pl0 pr0">
                        @include('page.panel.client.table')
                    </div>
                    <div class="panel-footer clearfix">
                        <a href="{{ URL::action('ClientController@create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6 column">
        <!-- services -->
        <div class="clearfix">
            <div class="panel panel-default">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title"><i class="icon-file-text2" style="font-size: 17px;padding-right: 5px;"></i>{{ Lang::get('service.latest') }}</h4>
                    <div class="panel-controls panel-controls-right">
                    </div>
                </div>
                @if(!CheckVar::c_array($services))
                    <div class="panel-body pl0 pr0">
                        <div class="bs-callout bs-callout-info">
                            <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('service.empty') }}</h4>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        @if(!CheckVar::c_array($clients))
                            <div class="btn btn-primary pull-right" disabled>{{ Lang::get('ui.add') }}</div>
                        @else
                            <a href="{{ URL::action('ServiceController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                        @endif
                    </div>
                @else
                    <div class="panel-body pl0 pr0">
                        @include('page.panel.service.table')
                    </div>
                    <div class="panel-footer clearfix">
                        <a href="{{ URL::action('ServiceController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                    </div>
                @endif
            </div>
        </div>
        <!-- reviews -->
        <div class="clearfix">
            <div class="panel panel-default">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title"><i class="icon-address-book" style="font-size: 17px;padding-right: 5px;"></i>{{ Lang::get('review.latest') }}</h4>
                    <div class="panel-controls panel-controls-right">
                    </div>
                </div>
                @if(!CheckVar::c_array($reviews))
                    <div class="panel-body pl0 pr0">
                        <div class="bs-callout bs-callout-info">
                            <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('service.empty') }}</h4>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        @if(!CheckVar::c_array($clients))
                            <div class="btn btn-primary pull-right" disabled>{{ Lang::get('ui.add') }}</div>
                        @else
                            <a href="{{ URL::action('ReviewController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                        @endif
                    </div>
                @else
                    <div class="panel-body pl0 pr0">
                        @include('page.panel.review.table')
                    </div>
                    <div class="panel-footer clearfix">
                        <a href="{{ URL::action('ReviewController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>


@stop
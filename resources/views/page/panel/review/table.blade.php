<table class="table table-striped table-hover table-condensed">
    <thead>
    <tr>
        @if(!Request::is('panel'))
            @if(Route::is('search') || Route::is('search.order'))
                <td></td>
                <td>
                    {{ Lang::get('table.name') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'name', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'name', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
                <td>
                    {{ Lang::get('table.surname') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'surname', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'surname', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
                <td>
                    {{ Lang::get('table.mileage') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'mileage', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'mileage', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
                <td>
                    {{ Lang::get('table.performed') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'date', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'date', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}"
                           class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
            @else
                <td></td>
                <td>
                    {{ Lang::get('table.name') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'name', 'order' => 'asc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'name', 'order' => 'desc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
                <td>
                    {{ Lang::get('table.surname') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'surname', 'order' => 'asc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'surname', 'order' => 'desc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
                <td>
                    {{ Lang::get('table.mileage') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'mileage', 'order' => 'asc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'mileage', 'order' => 'desc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
                <td>
                    {{ Lang::get('table.performed') }}
                    <div class="btn-group ml5">
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'date', 'order' => 'asc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-up m0"></i></a>
                        <a href="{{ URL::action('ReviewController@show_ordered', ['attribute' => 'date', 'order' => 'desc']) }}" class="btn-link p0"><i
                                    class="mdi mdi-chevron-down m0"></i></a>
                    </div>
                </td>
            @endif
        @else
            <td></td>
            <td>{{ Lang::get('table.name') }}</td>
            <td>{{ Lang::get('table.surname') }}</td>
            <td>{{ Lang::get('table.mileage') }}</td>
            <td>{{ Lang::get('table.performed') }}</td>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($reviews as $key => $value)
        <tr data-link="{{ URL::action('ReviewController@show', [$value->client, $value->id]) }}">
            <td class="tool-menu">
                <div class="tool-menu-container">
                    <div class="floating-options"><i class="mdi mdi-dots-vertical"></i></div>
                    <div class="toolButtons">
                        <div>
                            <a href="{{ URL::action('ReviewController@edit', [$value->client, $value->id]) }}"
                               class="mdi mdi-pencil btn btn-success" data-tite="{{ Lang::get('ui.edit') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.edit') }}</div>
                        </div>
                        <div>
                            <a href="{{ URL::action('ReviewController@destroy', [$value->client, $value->id]) }}"
                               class="mdi mdi-delete btn btn-danger" data-tite="{{ Lang::get('ui.delete') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.delete') }}</div>
                        </div>
                        <div>
                            <a target="_blank" href="{{ URL::action('ReportController@print_review', [$value->client, $value->id]) }}" class="mdi mdi-printer btn btn-primary"
                               data-tite="{{ Lang::get('ui.print') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.print') }}</div>
                        </div>
                    </div>
                </div>
            </td>
            <td data-label="{{ Lang::get('table.name') }}">{{ $value->client->name }}</td>
            <td data-label="{{ Lang::get('table.surname') }}">{{ $value->client->surname }}</td>
            <td data-label="{{ Lang::get('table.mileage') }}">{{ number_format($value->mileage) }} km</td>
            <td data-label="{{ Lang::get('table.performed') }}">{{ $value->date }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@extends('layouts.default')
@section('content')

<div class="col-md-8 column">
    {{ Html::panel_open('default', Lang::get('ui.profile.edit'), $errors, session('message')) }}
        <form class="form mt0" action="{{ URL::action('UserController@update', Auth::user()->id) }}" method="POST" enctype="multipart/form-data" id="register-form" role="form" autocomplete="off">
            {!! csrf_field() !!}
            <input type="hidden" name="_method" value="PUT" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">{{ Lang::get('table.name') }}</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="{{ Lang::get('auth.placeholder_name') }}" value="{{ Auth::user()->name }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="surname">{{ Lang::get('table.surname') }}</label>
                        <input type="text" name="surname" id="surname" class="form-control" placeholder="{{ Lang::get('auth.placeholder_surname') }}" value="{{ Auth::user()->surname }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="email">{{ Lang::get('table.email') }}</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="{{ Lang::get('auth.placeholder_email') }}" value="{{ Auth::user()->email }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="phone">{{ Lang::get('table.phone') }}</label>
                        <input type="text" name="phone" id="phone" class="form-control" placeholder="{{ Lang::get('auth.placeholder_phone') }}" value="{{ Auth::user()->phone }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="password">{{ Lang::get('table.password') }}</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="{{ Lang::get('auth.placeholder_password') }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="password1">{{ Lang::get('table.repeat_password') }}</label>
                        <input type="password" name="password1" id="password1" class="form-control" placeholder="{{ Lang::get('auth.placeholder_repeat_password') }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-success pull-right" type="submit">{{ Lang::get('ui.save') }}</button>
                        <a href="{{ URL::action('UserController@show') }}" class="btn btn-primary pull-right mr5" type="submit">{{ Lang::get('ui.return') }}</a>
                    </div>
                </div>
            </div>

        </form>
    {{ Html::panel_close() }}
</div>
@stop
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            @if(!Request::is('panel'))
                @if(Route::is('search') || Route::is('search.order'))
                    <td></td>
                    <td>
                        {{ Lang::get('table.name') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'name', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'name', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.surname') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'surname', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'surname', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>

                    <td>
                        {{ Lang::get('table.city') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'city', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'city', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.address') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'address', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'address', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>

                    <td>
                        {{ Lang::get('table.date') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'created_at', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'created_at', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                @else
                    <td></td>
                    <td>
                        {{ Lang::get('table.name') }}
                        <div class="btn-group ml5">
                            <a href="/panel/client/orderby/name/asc" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="/panel/client/orderby/name/desc" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.surname') }}
                        <div class="btn-group ml5">
                            <a href="/panel/client/orderby/surname/asc" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="/panel/client/orderby/surname/desc" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>

                    <td>
                        {{ Lang::get('table.city') }}
                        <div class="btn-group ml5">
                            <a href="/panel/client/orderby/city/asc" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="/panel/client/orderby/city/desc" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.address') }}
                        <div class="btn-group ml5">
                            <a href="/panel/client/orderby/address/asc" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="/panel/client/orderby/address/desc" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>

                    <td>
                        {{ Lang::get('table.date') }}
                        <div class="btn-group ml5">
                            <a href="/panel/client/orderby/created_at/asc" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="/panel/client/orderby/created_at/desc" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                @endif
            @else
                <td></td>
                <td>{{ Lang::get('table.name') }}</td>
                <td>{{ Lang::get('table.surname') }}</td>
                <td>{{ Lang::get('table.date') }}</td>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach($clients as $key => $value)
        <tr data-link="{{ URL::action('ClientController@show', $value->id) }}">
            <td class="tool-menu">
                <div class="tool-menu-container">
                    <div class="floating-options"><i class="mdi mdi-dots-vertical"></i></div>
                    <div class="toolButtons">
                        <div>
                            <a href="{{ URL::action('ClientController@edit', $value->id) }}" class="mdi mdi-pencil btn btn-success" data-tite="{{ Lang::get('ui.edit') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.edit') }}</div>
                        </div>
                        <div>
                            <a href="{{ URL::action('ClientController@destroy', $value->id) }}" class="mdi mdi-delete btn btn-danger" data-tite="{{ Lang::get('ui.delete') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.delete') }}</div>
                        </div>
                        <div class="print-options mt-15 hidden">
                            <a target="_blank" href="{{ URL::action('ReportController@last_month', [ 'id' => $value->id ]) }}">{{ Lang::get('ui.generate_report.with_all') }}</a>
                            <a target="_blank" href="{{ URL::action('ReportController@last_week', [ 'id' => $value->id ]) }}">{{ Lang::get('ui.generate_report.with_all') }}</a>
                        </div>
                    </div>
                </div>
            </td>
            <td data-label="{{ Lang::get('table.name') }}">{{ $value->name }}</td>
            <td data-label="{{ Lang::get('table.surname') }}">{{ $value->surname }}</td>
            @if($_SERVER['REQUEST_URI'] != '/panel')
                <td data-label="{{ Lang::get('table.city') }}">{{ $value->city }}</td>
                <td data-label="{{ Lang::get('table.address') }}">{{ $value->address }}</td>
            @endif
            <td data-label="{{ Lang::get('table.date') }}">{{ $value->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
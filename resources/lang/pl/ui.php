<?php

return [

    /*
    |--------------------------------------------------------------------------
    | UI Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'add'                       => 'dodaj',
    'cancel'                    => 'anuluj',
    'confirm_delete'            => [
        'message'   => 'Czy napewno chcesz usunąć ?',
        'title'     => 'Potwierdzenie usunięcia',
    ],
    'copy_to_clipboard'         => 'skopiuj do schowka',
    'delete'                    => 'usuń',
    'earning_chart_collapse'    => 'pokaż mniej',
    'earning_chart_expand'      => 'pokaż więcej',
    'earning_chart_title'       => 'Średnie zarobki w miesiącu',
    'edit'                      => 'edytuj',
    'call'                      => 'zadzwoń',
    'show_empty_fields'         => 'pokaż puste pola',
    'hide_empty_fields'         => 'schowaj puste pola',
    'mechanic'                  => 'mechanik',
    'signature'                 => 'podpis',
    'hint'                      => 'Podpowiedź',
    'from'                      => 'Od',
    'to'                        => 'Do',
    'select_range'              => 'Wybierz przedział',
    'device_hint'               => 'Tutaj mozesz dać lub odebrać dostęp urządzeniowi do danych twojego konta.<br>Wystarczy kliknąć <kbd>dodaj</kbd> wpisać nazwę urządzenia, następnie <span class="text-info">zeskanować</span> kod QR <i class="mdi mdi-qrcode"></i> przy logowaniu w aplikacji.<br>Usunięcie urządzenia z listy poniżej spowoduje odebranie uprawnień dostępu.',
    'device_name'               => 'Nazwa urządzenia',
    'from_day'                  => 'Z dnia: ',
    'generate_report'           => [
        'label'         => 'generuj raport',
        'ready'         => 'Gotowe',
        'last_month'    => 'ostatni miesiąc',
        'last_week'     => 'ostatni tydzień',
        'select_date'   => 'wybierz datę',
        'custom'        => 'generuj własny',
        'with_services' => 'z usługami',
        'with_reviews'  => 'z przeglądami',
        'with_all'      => 'z wszystkim',
    ],
    'export'                    => [
        'label'         => 'eksport do pliku',
        'format_xls'     => 'Microsoft Excel (XLS)',
        'format_xlsx'     => 'Microsoft Excel (XLSX)',
        'format_csv'     => 'Plik tekstowy (CSV)',
    ],
    'info_basic'                => 'Informacje podstawowe',
    'info_additional'           => 'Informacje dodatkowe',
    'map'                       => [
        'dir'       => 'Mapa dojazdu',
        'warning'   => [
            'header'     => 'Nie znaleziono adresu',
            'message'    => 'Czy podany adres jest na pewno poprawny?'
        ]
    ],
    'lang'                      => 'Język',
    'lang_en'                   => 'Angielski',
    'lang_pl'                   => 'Polski',
    'logout'                    => 'wyloguj',
    'last_seen'                 => 'OSTATNIO WIDZIANY',
    'month_1'                   => 'Styczeń',
    'month_10'                  => 'Październik',
    'month_11'                  => 'Listopad',
    'month_12'                  => 'Grudzień',
    'month_2'                   => 'Luty',
    'month_3'                   => 'Marzec',
    'month_4'                   => 'Kwiecień',
    'month_5'                   => 'Maj',
    'month_6'                   => 'Czerwiec',
    'month_7'                   => 'Lipiec',
    'month_8'                   => 'Sierpień',
    'month_9'                   => 'Wrzesień',
    'navigation'                => 'Nawigacja',
    'ok'                        => 'ok',
    'profile'                   => [
        'show'  => 'Profil',
        'edit'  => 'Profil - edycja',
    ],
    'print'                     => 'drukuj',
    'qrcode'                    => 'show qr code',
    'qrcode_scann_title'        => 'zeskanuj kod qr aplikacją aby aktywować urządzenie',
    'reset'                     => 'zresetuj',
    'return'                    => 'wróć',
    'save'                      => 'zapisz',
    'download_app'              => 'Pobierz aplikację',
    'version'                   => 'wersja',
    'search'                    => 'szukaj',
    'adv_search'                => 'zaawansowane wyszukiwanie',
    'search_failed'             => 'nie znaleziono',
    'search_placeholder'        => 'szukaj na stronie',
    'search_title'              => 'Wynik wyszukiwania dla <span class="label label-info">:keyword</span> w <span class="label label-success">:type</span>',
    'settings'                  => 'ustawienia',
    'start'                     => 'start',
    'toast_clipboard_message'   => 'Skopiowano do schowka',
    'email'                     => [
        'send'             => 'wyślij',
        'verify_sended'    => 'Email potwierdzający został wysłany na :email. Kliknij na link w wiadomości aby aktywować swoje konto.',
        'verify_success'   => 'Twoje konto zostało pomyślnie zweryfikowane',
        'verify_failure'   => 'Ups. Coś poszło nie tak podczas weryfikacji emaila',
        'subject'          => [
            'password_reset'    => 'DasAuto - reset hasła',
            'activation'        => 'DasAuto - weryfikacja'
        ],
        'resend'           => 'Wyślij ponownie',
        'no_reply'         => 'Ta wiadomość została wygenerowana automatycznie. Proszę na nią nie odpowiadać.',
        'welcome'          => 'Witamy w DasAuto',
        'activation'       => 'Kliknij poniżej aby zweryfikować swój email',
        'password_reset'   => 'Kliknij poniżej aby zmienić swoje hasło',
        'change_password'  => 'zmień hasło',
        'verify'           => 'zweryfikuj'
    ]
];

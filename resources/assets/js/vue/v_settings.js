/**
 * Created by Krzysztof Stec on 24.11.2016.
 */

/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('story-item', require('./components/StoryItem.vue'));
//Vue.component('movie-item', require('./components/MovieItem.vue'));
//Vue.component('pagination', require('./components/Pagination.vue'));

//Vue.component('device-item', require('./components/device.vue'));

const app = new Vue({
    el: '#v_settings',
    data: {
        is_busy: false,
        devices: [ ]
    },
    components: {
        device_item: require('./components/device.vue')
    },
    methods: {
        getTokens: function () {
            var self = this;
            self.is_busy = true;
            self.$http.get('/panel/settings/token/get').then(function(response){
                console.dir(response);
                if (response.status == 200) {
                    self.devices = response.data.data;
                }
                self.is_busy = false;
            });
        }
    },
    mounted: function () {
        console.info('Vue v_settings - running');
        this.getTokens();
    }
});

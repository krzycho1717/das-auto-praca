@extends('layouts.default')
@section('content')

    <div class="panel panel-default toggle panelMove panelClose panelRefresh">
        <!-- Start .panel -->
        <div class="panel-heading">
            <h4 class="panel-title inline mr10">{!! Lang::get('ui.search_title', ['keyword' => $keyword, 'type' => $typeLang]) !!}</h4>
            @if (Request::route()->getName() == 'search.order')
                <div class="btn-group inline">
                    <a href="{{ URL::route('search.order', ['keyword' => $keyword, 'type' => $typeRaw, 'order' => Route::Input('order') == 'asc' ? 'desc':'asc', 'attribute' => Route::Input('attribute')]) }}" class="btn btn-xs btn-success">
                        <span>{{ Lang::get('table.'.Route::Input('attribute')) }}</span>
                        @if (Route::Input('order') == 'desc')
                            <i class="mdi mdi-chevron-down m0"></i>
                        @else
                            <i class="mdi mdi-chevron-up m0"></i>
                        @endif
                    </a>
                    <a href="{{ URL::action('UserController@search', ['keyword' => $keyword, 'type' => $typeRaw]) }}" class="btn btn-xs btn-danger">
                        <i class="mdi mdi-close p0 m0"></i>
                    </a>
                </div>
            @endif
        </div>
        <div class="panel-body pl0 pr0">
            @if($typeNum == 0)
                @if(!CheckVar::c_array($clients))
                    <div class="bs-callout bs-callout-info">
                       <div class="row">
                           <div class="col-md-9 column">
                               <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('ui.search_failed') }}</h4>
                           </div>
                           <div class="clearfix"></div>
                       </div>
                    </div>
                @else
                    @include('page.panel.client.table', ['keyword' => $keyword, 'type' => $typeRaw])
                    {!! $clients->render() !!}
                @endif
            @elseif($typeNum == 1)
                @if(!CheckVar::c_array($services))
                    <div class="bs-callout bs-callout-info">
                       <div class="row">
                           <div class="col-md-9 column">
                               <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('ui.search_failed') }}</h4>
                           </div>
                           <div class="clearfix"></div>
                       </div>
                    </div>
                @else
                    @include('page.panel.service.table', ['keyword' => $keyword, 'type' => $typeRaw])
                    {!! $services->render() !!}
                @endif
            @elseif($typeNum == 2)
                @if(!CheckVar::c_array($reviews))
                    <div class="bs-callout bs-callout-info">
                        <div class="row">
                            <div class="col-md-9 column">
                                <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('ui.search_failed') }}</h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @else
                    @include('page.panel.review.table', ['keyword' => $keyword, 'type' => $typeRaw])
                    {!! $reviews->render() !!}
                @endif
            @endif
        </div>
    </div>

@stop
<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use \App\Models\Client;
use App;
use Auth;
use JsonPackage;
use Validator;
use Input;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  string $type
     * @param int $limit
     * @return \Illuminate\Http\Response
     */
    public function index($type="json", $limit=0)
    {
        if($limit > 0)
            return JsonPackage::prepare($type, Auth::user()->clients()->orderBy('created_at', 'desc')->take(intval($limit))->get()->toArray(), null);
        else
            return JsonPackage::prepare($type, Auth::user()->clients()->get()->toArray(), null);
    }

    public function show_services($id, $type="json"){
        $client = Auth::user()->clients()->find($id);
        if ($client == null) {
            return JsonPackage::prepare($type, null, null, 404);
        }
        return JsonPackage::prepare($type, $client->services()->with('client')->get()->toArray(), null);
    }

    public function show_reviews($id, $type="json"){
        $client = Auth::user()->clients()->find($id);
        if ($client == null) {
            return JsonPackage::prepare($type, null, null, 404);
        }
        return JsonPackage::prepare($type, $client->reviews()->with('client')->get()->toArray(), null);
    }

    /**
     * Allow to search clients of user
     *
     * @param $keyword
     * @param  string $type
     * @param int $limit
     * @return \Illuminate\Http\Response
     */
    public function search($keyword, $type="json", $limit=0)
    {
        $clients = Auth::user()->clients()
            ->where('clients.name', 'LIKE', '%'. $keyword .'%')
            ->orWhere('clients.surname', 'LIKE', '%'. $keyword .'%')
            ->orWhere('clients.email', 'LIKE', '%'. $keyword .'%')
            ->orWhere('clients.phone', 'LIKE', '%'. $keyword .'%')
            ->orWhere('clients.city', 'LIKE', '%'. $keyword .'%')
            ->orWhere('clients.address', 'LIKE', '%'. $keyword .'%')
            ->orWhere('clients.country', 'LIKE', '%'. $keyword .'%');
        return JsonPackage::prepare($type, $clients->get()->toArray(), null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function store($type="json")
    {
        $rules = array(
            'name'    => 'required',
            'surname'    => 'required',
            'phone'    => 'required',
            'email'    => 'required|email',
            'country'    => 'required',
            'city'    => 'required',
            'address'    => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {
            $client = new Client();
            $client->user_id = Auth::user()->id;
            $client->name = Input::get('name');
            $client->surname = Input::get('surname');
            $client->phone = Input::get('phone');
            $client->email = Input::get('email');
            $client->country = Input::get('country');
            $client->city = Input::get('city');
            $client->address = Input::get('address');

            $client->save();

            return JsonPackage::prepare($type, null, null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type='json')
    {
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, $result, null, 404);
        }
        return JsonPackage::prepare($type, $result, null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function update($id, $type="json")
    {
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, $result, null, 404);
        }

        $rules = array(
            'name'    => 'required',
            'surname'    => 'required',
            'phone'    => 'required',
            'email'    => 'required|email',
            'country'    => 'required',
            'city'    => 'required',
            'address'    => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {

            $userdata = array(
                'name' => Input::get('name'),
                'surname' => Input::get('surname'),
                'phone' => Input::get('phone'),
                'email' => Input::get('email'),
                'country' => Input::get('country'),
                'city' => Input::get('city'),
                'address' => Input::get('address'),
            );

            $result->update($userdata);
            return JsonPackage::prepare($type, null, null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $type="json")
    {
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, null, 404);
        }

        $result->delete();
        return JsonPackage::prepare($type, null, null);
    }
}

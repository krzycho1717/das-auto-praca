@extends('layouts.user')
@section('content')

<div class="login-panel panel panel-default plain animated bounceIn" style="margin-top: 100px">
    <!-- Start .panel -->
    <div class="panel-heading">
        <h4 class="panel-title text-center">
            <img id="logo" src="/images/logo2.png" alt="DasAuto logo">
        </h4>
    </div>
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="bs-callout bs-callout-danger">
                <i class="mdi mdi-alert-outline"></i>
                <strong>Oh snap!</strong>
                <ul style="list-style: none">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('message'))
            <div class="bs-callout bs-callout-danger">
                <i class="mdi mdi-alert-outline"></i>

                <strong>Oh snap!</strong>
                {{ session('message') }}
            </div>
        @endif
        <form class="form-horizontal mt0" action="{{ URL::to('/register') }}" method="POST" enctype="multipart/form-data" id="register-form" role="form" autocomplete="off">
            {!! csrf_field() !!}
            <div class="form-group ml0 mr0">
                <input value="{{ Input::old('name') }}" type="text" name="name" id="name" class="form-control" placeholder="{{ Lang::get('auth.placeholder_name') }}">
            </div>
            <div class="form-group ml0 mr0">
                <input value="{{ Input::old('surname') }}" type="text" name="surname" id="surname" class="form-control" placeholder="{{ Lang::get('auth.placeholder_surname') }}">
            </div>
            <div class="form-group ml0 mr0">
                <input value="{{ Input::old('email') }}" type="text" name="email" id="email" class="form-control" placeholder="{{ Lang::get('auth.placeholder_email') }}">
            </div>
            <div class="form-group ml0 mr0">
                <input value="{{ Input::old('phone') }}" type="text" name="phone" id="phone" class="form-control" placeholder="{{ Lang::get('auth.placeholder_phone') }}">
            </div>
            <div class="form-group ml0 mr0">
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ Lang::get('auth.placeholder_password') }}">
            </div>
            <div class="form-group ml0 mr0">
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ Lang::get('auth.placeholder_repeat_password') }}">
            </div>
            {{--<div class="form-group ml0 mr0">--}}
                {{--<input name="locale" id="locale"  value="{{ Input::old('locale') ? Input::old('locale') :'pl' }}" hidden>--}}
                {{--<div id="change-language" class="btn-group dropdown mr10">--}}
                    {{--<button  type="button" class="btn btn-primary btn-alt dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<span class="language-polish"><i class="mdi mdi-language-polish"></i><span class="ml10">{{ Lang::get('ui.lang_pl') }}</span></span>--}}
                        {{--<span class="language-english" style="display: none"><i class="mdi mdi-language-english"></i><span class="ml10">{{ Lang::get('ui.lang_en') }}</span></span>--}}
                        {{--<span class="caret"></span>--}}
                    {{--</button>--}}
                    {{--<ul class="dropdown-menu right" role="menu" style="margin-left: -41px;">--}}
                        {{--<li data-locale="pl"><a href="#"><i class="mdi mdi-language-polish"></i>{{ Lang::get('ui.lang_pl') }}</a></li>--}}
                        {{--<li data-locale="en"><a href="#"><i class="mdi mdi-language-english"></i>{{ Lang::get('ui.lang_en') }}</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group ml0 mr0">
                <div class="col-lg-12 col-md-9">
                    <button class="btn btn-success pull-right" type="submit">{{ Lang::get('auth.create_account') }}</button>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-footer gray-lighter-bg bt">
        <h4 class="text-center"><strong>{{ Lang::get('auth.already_have_account') }}</strong>
        </h4>
        <p class="text-center"><a href="/" class="btn btn-primary">{{ Lang::get('auth.sign_in') }}</a>
        </p>
    </div>
</div>
<!-- End .panel -->
<div class="container pt10">
    <div class="footer">
        <p class="text-center">©2015 Copyright Krzysztof Stec. All right reserved</p>
    </div>
</div>
@stop
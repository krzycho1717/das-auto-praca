<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Models\Client;
use DB;
use DNS2D;
use Validator;
use View;

class ReportController extends Controller
{
    public function print_review($client_id, $review_id)
    {
        $client = Auth::user()->clients()->find($client_id);
        if ($client == null)
            abort(404);
        $review = $client->reviews()->find($review_id);
        if ($review == null)
            abort(404);
        $qr_code = DNS2D::getBarcodePNG(base64_encode('USER_'.Auth::user()->id.'_REVIEW_'.$review->id), "QRCODE", 5, 5);
        return View::make('page.panel.review.print')->with('review', $review)->with('qr_code', $qr_code);
    }

    public function last_month($id, $extra=null)
    {
        $client = Auth::user()->clients()->find($id);
        if($client == null){
            abort(404);
        }
        $qr_code = DNS2D::getBarcodePNG(base64_encode('USER_'.Auth::user()->id.'_CLIENT_'.$client->id), "QRCODE", 5, 5);
        #   with/service,review     0
        #   with/service            1
        #   with/review             2
        $extra_option = 0;
        if ($extra != null){
            $pe = explode(',', str_replace('with/', '', $extra));
            if (count($pe) == 1){
                $extra_option = $pe[0] == 'service' ? 1 : 2;
            }
        }
        switch ($extra_option){
            case 0:
                $services = $client->services()
                    ->where(DB::raw('TO_CHAR(date, \'MM\')'), Carbon::now()->subMonth(1)->month)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);
                $reviews = $client->reviews()
                    ->where(DB::raw('TO_CHAR(date, \'MM\')'), Carbon::now()->subMonth(1)->month)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);
                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('services', $services)
                    ->with('reviews', $reviews);
            case 1:
                $services = $client->services()
                    ->where(DB::raw('TO_CHAR(date, \'MM\')'), Carbon::now()->subMonth(1)->month)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);
                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('services', $services);
            case 2:
                $reviews = $client->reviews()
                    ->where(DB::raw('TO_CHAR(date, \'MM\')'), Carbon::now()->subMonth(1)->month)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);
                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('reviews', $reviews);
            default:
                $services = $client->services()
                    ->where(DB::raw('TO_CHAR(date, \'MM\')'), Carbon::now()->subMonth(1)->month)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);
                $reviews = $client->reviews()
                    ->where(DB::raw('TO_CHAR(date, \'MM\')'), Carbon::now()->subMonth(1)->month)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);
                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('services', $services)
                    ->with('reviews', $reviews);
        }
    }

    public function last_week($id, $extra=null)
    {
        $client = Auth::user()->clients()->find($id);
        if($client == null){
            abort(404);
        }
        $qr_code = DNS2D::getBarcodePNG(base64_encode('USER_'.Auth::user()->id.'_CLIENT_'.$client->id), "QRCODE", 5, 5);
        #   with/service,review     0
        #   with/service            1
        #   with/review             2
        $extra_option = 0;
        if ($extra != null){
            $pe = explode(',', str_replace('with/', '', $extra));
            if (count($pe) == 1){
                $extra_option = $pe[0] == 'service' ? 1 : 2;
            }
        }
        switch ($extra_option) {
            case 0:
                $services = $client->services()
                    ->where(DB::raw('TO_CHAR(date, \'WW\')'), Carbon::now()->subWeek(1)->weekOfYear)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);

                $reviews = $client->reviews()
                    ->where(DB::raw('TO_CHAR(date, \'WW\')'), Carbon::now()->subWeek(1)->weekOfYear)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);

                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('services', $services)
                    ->with('reviews', $reviews);
            case 1:
                $services = $client->services()
                    ->where(DB::raw('TO_CHAR(date, \'WW\')'), Carbon::now()->subWeek(1)->weekOfYear)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);

                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('services', $services);
            case 2:
                $reviews = $client->reviews()
                    ->where(DB::raw('TO_CHAR(date, \'WW\')'), Carbon::now()->subWeek(1)->weekOfYear)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);

                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('reviews', $reviews);
            default:
                $services = $client->services()
                    ->where(DB::raw('TO_CHAR(date, \'WW\')'), Carbon::now()->subWeek(1)->weekOfYear)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);

                $reviews = $client->reviews()
                    ->where(DB::raw('TO_CHAR(date, \'WW\')'), Carbon::now()->subWeek(1)->weekOfYear)
                    ->where(DB::raw('TO_CHAR(date, \'YYYY\')'), Carbon::now()->year);

                return View::make('page.panel.client.print')
                    ->with('qr_code', $qr_code)
                    ->with('extra', $extra_option)
                    ->with('client', $client)
                    ->with('services', $services)
                    ->with('reviews', $reviews);
        }
    }

    public function range($id, $from, $to, $extra=null)
    {
        $client = Auth::user()->clients()->find($id);
        if ($client == null) {
            echo 'abort(404)';
        }

        $rules = array(
            'from' => 'required|date_format:Y-m-d',
            'to' => 'required|date_format:Y-m-d',
        );

        $validator = Validator::make([
            'from' => $from,
            'to' => $to
        ], $rules);

        if ($validator->fails()) {
            //abort(404);
            echo '404<br>';
            var_dump($validator->getMessageBag()->getMessages());
        } else {
            $qr_code = DNS2D::getBarcodePNG(base64_encode('USER_' . Auth::user()->id . '_CLIENT_' . $client->id), "QRCODE", 5, 5);
            #   with/service,review     0
            #   with/service            1
            #   with/review             2
            $extra_option = 0;
            if ($extra != null){
                $pe = explode(',', str_replace('with/', '', $extra));
                if (count($pe) == 1){
                    $extra_option = $pe[0] == 'service' ? 1 : 2;
                }
            }
            switch ($extra_option) {
                case 0;
                    $services = $client->services()
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '>=', $from)
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '<', $to);

                    $reviews = $client->reviews()
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '>=', $from)
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '<', $to);

                    return View::make('page.panel.client.print')
                        ->with('qr_code', $qr_code)
                        ->with('extra', $extra_option)
                        ->with('client', $client)
                        ->with('services', $services)
                        ->with('reviews', $reviews);
                case 1;
                    $services = $client->services()
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '>=', $from)
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '<', $to);

                    return View::make('page.panel.client.print')
                        ->with('qr_code', $qr_code)
                        ->with('extra', $extra_option)
                        ->with('client', $client)
                        ->with('services', $services);
                case 2;
                    $reviews = $client->reviews()
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '>=', $from)
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '<', $to);

                    return View::make('page.panel.client.print')
                        ->with('qr_code', $qr_code)
                        ->with('extra', $extra_option)
                        ->with('client', $client)
                        ->with('reviews', $reviews);
                default:
                    $services = $client->services()
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '>=', $from)
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '<', $to);

                    $reviews = $client->reviews()
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '>=', $from)
                        ->where(DB::raw('TO_DATE(date::text, \'YYYY-MM-DD\')'), '<', $to);

                    return View::make('page.panel.client.print')
                        ->with('qr_code', $qr_code)
                        ->with('extra', $extra_option)
                        ->with('client', $client)
                        ->with('services', $services)
                        ->with('reviews', $reviews);
            }
        }

    }
}

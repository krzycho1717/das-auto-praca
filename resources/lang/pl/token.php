<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Token Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'device'    => [
        'id'    => [
            'empty' => [
                'tag'   => 'NIE AKTYWOWANO',
            ],
            'tag'   => 'ID URZĄDZENIA',
        ],
        'token' => [
            'tag'   => 'TOKEN',
        ],
    ],
    'devices'   => [
        'title' => 'Urządzenia',
    ],
];

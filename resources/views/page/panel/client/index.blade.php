@extends('layouts.default')
@section('content')

<div class="modal fade modal-danger" id="delete-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
                <!-- Start .panel -->
                <div class="modal-header">
                    <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
                </div>
                <div class="modal-body">
                    {{ Lang::get('ui.confirm_delete.message') }}
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default delete-cancel inline" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.cancel') }}</a>
                    <form action="" method="post" id="delete-form" class="inline">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-primary" data-bb-handler="confirm" type="submit">{{ Lang::get('ui.ok') }}</button>
                    </form>
                </div>
        </div>
    </div>
</div>

<div class="panel panel-default toggle panelMove panelClose panelRefresh">
    <!-- Start .panel -->
    <div class="panel-heading">
        <h4 class="panel-title inline mr10"><i class="icon-user-tie"></i>{{ Lang::get('client.Clients') }}</h4>
        @if (Request::route()->getName() == 'client.order')
            <div class="btn-group inline">
                <a href="{{ URL::route('client.order', ['order' => Route::Input('order') == 'asc' ? 'desc':'asc', 'attribute' => Route::Input('attribute')]) }}" class="btn btn-xs btn-success">
                    <span>{{ Lang::get('table.'.Route::Input('attribute')) }}</span>
                    @if (Route::Input('order') == 'desc')
                        <i class="mdi mdi-chevron-down m0"></i>
                    @else
                        <i class="mdi mdi-chevron-up m0"></i>
                    @endif
                </a>
                <a href="{{ URL::action('ClientController@index') }}" class="btn btn-xs btn-danger">
                    <i class="mdi mdi-close p0 m0"></i>
                </a>
            </div>
        @endif
    </div>
    <div class="panel-body pl0 pr0">
        @if(!CheckVar::c_array($clients))
            <div class="bs-callout bs-callout-info">
                <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('client.empty') }}</h4>
            </div>
        @else
            @include('page.panel.client.table')
            {!! $clients->render() !!}
        @endif
    </div>
    <div class="panel-footer clearfix">
        <a href="{{ URL::action('ClientController@create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
    </div>
</div>

@stop
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    '404'       => [
        'message'   => 'Strona której szukasz nie istnieje.',
        'title'     => 'Nie mogliśmy znaleźć ...',
    ],
    '500'       => [
        'message'   => 'Wewnętrzny błąd serwera.',
        'title'     => 'Coś poszło nie tak',
    ],
    'choice'    => 'Wróć',
    'no_data'   => 'brak',
];

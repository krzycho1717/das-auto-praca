@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('client.create_title'), $errors, session('message')) }}
            {{ Form::open(['action' => 'ClientController@store', 'method' => 'POST']) }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {{ Form::label('name', Lang::get('table.name')) }}
                            {{ Form::text('name', Input::old('name'), ['class' => 'form-control', 'placeholder' => Lang::get('table.name'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('surname') ? 'has-error' : '' }}">
                            {{ Form::label('surname', Lang::get('table.surname')) }}
                            {{ Form::text('surname', Input::old('surname'), ['class' => 'form-control', 'placeholder' => Lang::get('table.surname'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('surname') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                            {{ Form::label('phone', Lang::get('table.phone')) }}
                            {{ Form::tel('phone', Input::old('phone'), ['class' => 'form-control', 'placeholder' => Lang::get('table.phone'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            {{ Form::label('email', Lang::get('table.email')) }}
                            {{ Form::text('email', Input::old('email'), ['class' => 'form-control', 'placeholder' => Lang::get('table.email'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                            {{ Form::label('country', Lang::get('table.country')) }}
                            {{ Form::text('country', Input::old('country'), ['class' => 'form-control', 'placeholder' => Lang::get('table.country'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('country') }}</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                            {{ Form::label('city', Lang::get('table.city')) }}
                            {{ Form::text('city', Input::old('city'), ['class' => 'form-control', 'placeholder' => Lang::get('table.city'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('city') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                            {{ Form::label('address', Lang::get('table.address')) }}
                            {{ Form::text('address', Input::old('address'), ['class' => 'form-control', 'placeholder' => Lang::get('table.address'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(Lang::get('ui.add'), ['class' => 'btn btn-success pull-right']) }}
                    </div>
                </div>
            {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>

@stop
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    '404'       => [
        'message'   => 'The page you`re looking for doesn`t exist.',
        'title'     => 'We couldn`t find it ...',
    ],
    '500'       => [
        'message'   => 'Internal server error.',
        'title'     => 'Something went wrong',
    ],
    'choice'    => 'Go back',
    'no_data'   => 'no data',
];

<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
            @if(!Request::is('panel'))
                @if(Route::is('search') || Route::is('search.order'))
                    <td></td>
                    <td>
                        {{ Lang::get('table.name') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'name', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'name', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.surname') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'surname', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'surname', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.charge') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'charge', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'charge', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.performed') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'date', 'order' => 'asc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('UserController@search_ordered', ['attribute' => 'date', 'order' => 'desc', 'keyword' => $keyword, 'type' => $type]) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                @else
                    <td></td>
                    <td>
                        {{ Lang::get('table.name') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'name', 'order' => 'asc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'name', 'order' => 'desc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.surname') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'surname', 'order' => 'asc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'surname', 'order' => 'desc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.charge') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'charge', 'order' => 'asc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'charge', 'order' => 'desc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                    <td>
                        {{ Lang::get('table.performed') }}
                        <div class="btn-group ml5">
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'date', 'order' => 'asc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-up m0"></i></a>
                            <a href="{{ URL::action('ServiceController@show_ordered', ['attribute' => 'date', 'order' => 'desc']) }}" class="btn-link p0"><i class="mdi mdi-chevron-down m0"></i></a>
                        </div>
                    </td>
                @endif
            @else
                <td></td>
                <td>{{ Lang::get('table.name') }}</td>
                <td>{{ Lang::get('table.surname') }}</td>
                <td>{{ Lang::get('table.charge') }}</td>
                <td>{{ Lang::get('table.performed') }}</td>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach($services as $key => $value)
        <tr data-link="{{ URL::action('ServiceController@show', [$value->client, $value->id]) }}">
            <td class="tool-menu">
                <div class="tool-menu-container">
                    <div class="floating-options"><i class="mdi mdi-dots-vertical"></i></div>
                    <div class="toolButtons">
                        <div>
                            <a href="{{ URL::action('ServiceController@edit', [$value->client, $value->id]) }}" class="mdi mdi-pencil btn btn-success" data-tite="{{ Lang::get('ui.edit') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.edit') }}</div>
                        </div>
                        <div>
                            <a href="{{ URL::action('ServiceController@destroy', [$value->client, $value->id]) }}" class="mdi mdi-delete btn btn-danger" data-tite="{{ Lang::get('ui.delete') }}"></a>
                            <div class="no-selection">{{ Lang::get('ui.delete') }}</div>
                        </div>
                    </div>
                </div>
            </td>
            <td data-label="{{ Lang::get('table.name') }}">{{ $value->client->name }}</td>
            <td data-label="{{ Lang::get('table.surname') }}">{{ $value->client->surname }}</td>
            <td data-label="{{ Lang::get('table.charge') }}">{{ $value->charge }} zł</td>
            <td data-label="{{ Lang::get('table.performed') }}">{{ $value->date }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
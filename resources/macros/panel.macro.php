<?php
Html::macro('panel_open', function($panel_type, $title, $errors, $message, $with_link=false, Array $links=null){

    $html = '<div class="panel panel-'.$panel_type.'">';

    $html .= '<div class="panel-heading">';
    $html .= '<h4 class="panel-title">'.$title;
    if($with_link && $links != null)
    {
        $link_defaults = ['style' => '', 'url' => '', 'class' => '', 'name' => '', 'sep_style' => ''];
        foreach ($links as $key => $link)
        {
            $link = array_merge($link_defaults, $link);
            if($link['style'] == '')
                $html .= '<a href="'.$link['url'].'" class="header-link pull-right '.$link['class'].'">'.$link['name'].'</a>';
            else
                $html .= '<a href="'.$link['url'].'" style="'.$link['style'].'" class="header-link pull-right '.$link['class'].'">'.$link['name'].'</a>';
            $html .= '<span class="ml5 mr5 pull-right" style="color: rgba(11, 172, 211, 0.55);'.$link['sep_style'].'">|</span>';
        }
    }
    $html .= '</h4>';
    $html .= '</div><div class="panel-body">';

    if (count($errors) > 0){
        $html .= <<<HTML
        <div class="bs-callout bs-callout-danger">
            <i class="mdi mdi-alert-outline"></i>
                <strong>Oh snap!</strong>
                <ul style="list-style: none">
HTML;
        foreach($errors->all() as $error){
            $html .= '<li>'.$error.'</li>';
        }
        $html .= '</ul></div>';
    }
    if ($message){
        $html .= <<<HTML
        <div class="bs-callout bs-callout-danger">
            <i class="mdi mdi-alert-outline"></i>
                <strong>Oh snap!</strong>
HTML;
        $html .= '$message</div>';
    }

    return new \Illuminate\Support\HtmlString($html);

});

Html::macro('panel_close', function(){
    $html = <<<HTML
                </div>
        </div>
HTML;
});

?>

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ActionLog extends Model {

    protected $table = 'action_logs';
    protected $fillable = ['type', 'affected', 'from_ip', 'extra'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getExtraAsArray(){
        return json_decode($this->extra);
    }

    /**
     * Deletes oldest log when limit is achieved
     */
    public static function pop(){
        $a = Auth::user()->actionLogs()->orderBy('created_at', 'asc');
        if($a == null) return;
        if($a->count() == 6)
            $a->first()->delete();
    }

    // default date format for created_at
    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    // default date format for updated_at
    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}

?>
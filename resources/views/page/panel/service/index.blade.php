@extends('layouts.default')
@section('content')

<div class="modal fade modal-danger" id="delete-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- Start .panel -->
            <div class="modal-header">
                <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
            </div>
            <div class="modal-body">
                {{ Lang::get('ui.confirm_delete.message') }}
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default delete-cancel inline" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.cancel') }}</a>
                <form action="" method="post" id="delete-form" class="inline">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-primary" data-bb-handler="confirm" type="submit">{{ Lang::get('ui.ok') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@if(Route::is('panel.client.service.index'))
    <div class="panel panel-default toggle panelMove panelClose panelRefresh">
        <!-- Start .panel -->
        <div class="panel-heading">
            <h4 class="panel-title inline mr10"><i class="icon-user-tie"></i>{{ Lang::get('service.Services') }}</h4>
                <a href="{{ URL::action('ClientController@show', [ 'id' => $client->id ]) }}"><span class="label label-info">{{ $client->fullname() }}</span></a>
        </div>
    </div>
    @if(!CheckVar::c_array($services))
        <div class="panel-body pl0 pr0">
            <div class="bs-callout bs-callout-info">
                <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('service.empty') }}</h4>
            </div>
        </div>
    @else
    @foreach($services as $service)
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default toggle panelMove panelClose panelRefresh">
                <div class="panel-heading clearfix">
                    <div class="tool-menu inline">
                        <div class="tool-menu-container">
                            <div class="floating-options"><i class="mdi mdi-dots-vertical"></i></div>
                            <div class="toolButtons">
                                <div>
                                    <a href="{{ URL::action('ServiceController@edit', [$service->client, $service->id]) }}" class="mdi mdi-pencil btn btn-success" data-tite="{{ Lang::get('ui.edit') }}"></a>
                                    <div class="no-selection">{{ Lang::get('ui.edit') }}</div>
                                </div>
                                <div>
                                    <a href="{{ URL::action('ServiceController@destroy', [$service->client, $service->id]) }}" class="mdi mdi-delete btn btn-danger" data-tite="{{ Lang::get('ui.delete') }}"></a>
                                    <div class="no-selection">{{ Lang::get('ui.delete') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inline">{{ Lang::get('table.performed') }}</div>
                    <div class="badge inline">{{ $service->date }}</div>
                </div>
                <div class="panel-body">
                    {!! $service->content !!}
                </div>
            </div>
        </div>
    @endforeach
    @endif
@else
    <div class="panel panel-default toggle panelMove panelClose panelRefresh">
        <!-- Start .panel -->
        <div class="panel-heading">
            <h4 class="panel-title inline mr10"><i class="icon-user-tie"></i>{{ Lang::get('service.Services') }}</h4>
            @if(Request::route()->getName() == 'panel.client.service.index')
                <a href="{{ URL::action('ClientController@show', [ 'id' => $client->id ]) }}"><span class="label label-info">{{ $client->fullname() }}</span></a>
            @elseif(Request::route()->getName() == 'service.order')
                <div class="btn-group inline">
                    <a href="{{ URL::route('service.order', ['order' => Route::Input('order') == 'asc' ? 'desc':'asc', 'attribute' => Route::Input('attribute')]) }}" class="btn btn-xs btn-success">
                        <span>{{ Lang::get('table.'.Route::Input('attribute')) }}</span>
                        @if (Route::Input('order') == 'desc')
                            <i class="mdi mdi-chevron-down m0"></i>
                        @else
                            <i class="mdi mdi-chevron-up m0"></i>
                        @endif
                    </a>
                    <a href="{{ URL::route('service.index') }}" class="btn btn-xs btn-danger">
                        <i class="mdi mdi-close p0 m0"></i>
                    </a>
                </div>
            @endif
        </div>
        @if(!CheckVar::c_array($services))
            <div class="panel-body pl0 pr0">
                <div class="bs-callout bs-callout-info">
                    <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('service.empty') }}</h4>
                </div>
            </div>
            <div class="panel-footer clearfix">
                @if(Session::get('clients_number') == 0)
                    @if(isset($client))
                        <div class="btn btn-primary pull-right" disabled>{{ Lang::get('ui.add') }}</div>
                    @else
                        <div class="btn btn-primary pull-right" disabled>{{ Lang::get('ui.add') }}</div>
                    @endif
                @else
                    @if(isset($client))
                        <a href="{{ URL::action('ServiceController@create', $client) }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                    @else
                        <a href="{{ URL::action('ServiceController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                    @endif
                @endif
            </div>
        @else
            <div class="panel-body pl0 pr0">
                @include('page.panel.service.table')
                {!! $services->render() !!}
            </div>
            <div class="panel-footer clearfix">
                @if(isset($client))
                    <a href="{{ URL::action('ServiceController@create', $client) }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                @else
                    <a href="{{ URL::action('ServiceController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                @endif
            </div>
        @endif
    </div>
@endif
@stop
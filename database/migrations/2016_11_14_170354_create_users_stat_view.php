<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersStatView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE VIEW users_stat AS
                SELECT 
                    "users".email,
                    (SELECT COUNT(*) AS clients_count FROM "clients" WHERE "clients".user_id = users.id),
                    (SELECT COUNT(*) AS services_count FROM "services" WHERE "services".user_id = users.id),
                    (SELECT COUNT(*) AS reviews_count FROM "reviews" WHERE "reviews".user_id = users.id)
                FROM "users";
        ');
//        DB::statement('
//            CREATE VIEW users_stat AS
//                SELECT "users".email,
//                    count("clients".id) AS clients_count,
//                    count("services".id) AS services_count,
//                    count("reviews".id) AS reviews_count
//                FROM "users"
//                    LEFT JOIN "clients" ON "users".id = "clients".user_id
//                    LEFT JOIN "services" ON "users".id = "services".user_id
//                    LEFT JOIN "reviews" ON "users".id = "reviews".user_id
//                GROUP BY "users".email;
//        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS users_stat');
    }
}

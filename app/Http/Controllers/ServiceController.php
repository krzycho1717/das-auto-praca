<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Service;
use Illuminate\Support\Facades\Lang;
use Auth;
use Input;
use View;
use Redirect;
use Validator;
use Session;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Client $client
     * @return \Illuminate\Http\Response
     */

    public function index(Client $client)
    {
        $result = $client->services()
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return View::make('page.panel.service.index')
            ->with('services', $result)
            ->with('client', $client)
            ->with('title', Lang::get('title.service.start'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Client $client
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        return View::make('page.panel.service.create')
            ->with('client', $client)
            ->with('title', Lang::get('title.service.new'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function raw_create()
    {
        $clients = Auth::user()->clients()->get();
        return View::make('page.panel.service.raw_create')
            ->with('clients', $clients)
            ->with('title', Lang::get('title.service.new'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Client $client
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store(Client $client)
    {
        $rules = array(
            'content'    => 'required',
            'charge'    => 'required',
            'date'    => 'required|date',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput(Input::all())
                ->withErrors($validator);
        } else {
            $service = new Service();
            $service->user_id = Auth::user()->id;
            $service->client_id = $client->id;
            $service->content = Input::get('content');
            $service->charge = Input::get('charge');
            $service->date = Input::get('date');

            $service->save();

            return Redirect::action('UserController@wall');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function raw_store()
    {
        $rules = array(
            'client_id' => 'required|numeric|exists:clients,id',
            'content'   => 'required',
            'charge'    => 'required|numeric|min:0',
            'date'    => 'required|date',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput(Input::all())
                ->withErrors($validator);
        } else {
            $client = Client::find(Input::get('client_id'));
            if($client != null) {
                $service = new Service();
                $service->user_id = Auth::user()->id;
                $service->client_id = $client->id;
                $service->content = Input::get('content');
                $service->charge = Input::get('charge');
                $service->date = Input::get('date');

                $service->save();

                return Redirect::action('UserController@wall');
            }else{
                return Redirect::back()
                    ->withInput(Input::all());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, $id)
    {

        $service = Auth::user()->services()->find($id);
        if($service == null){
            abort(404);
        }

        return View::make('page.panel.service.show')
            ->with('service', $service)
            ->with('title', Lang::get('title.service.show'));
    }

    public function show_ordered($attribute, $order){
        if(!Service::canOrderBy($attribute)) return Redirect::to('/panel/service');
        $result = Auth::user()->services()->join('clients', 'services.client_id', '=', 'clients.id')->orderBy($attribute, $order)->paginate(25);
        return View::make('page.panel.service.index')
            ->with('services', $result)
            ->with('attribute', $attribute)
            ->with('order', $order)
            ->with('title', Lang::get('title.service.start'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, $id)
    {
        $result = $client->services()->find($id);

        if($result == null){
            abort(404);
        }

        return View::make('page.panel.service.edit')
            ->with('service', $result)
            ->with('client', $client)
            ->with('title', Lang::get('title.service.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function update(Client $client, $id)
    {
        $result = $client->services()->find($id);
        if($result == null){
            abort(404);
        }

        $rules = array(
            'content'    => 'required',
            'charge'    => 'required',
            'date'    => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {

            $userdata = array(
                'content' => Input::get('content'),
                'charge' => Input::get('charge'),
                'date' => Input::get('date')
            );

            $result->update($userdata);

            return Redirect::action('ServiceController@show', [$client, $id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client, $id)
    {
        $result = $client->services()->find($id);

        if($result == null){
            abort(404);
        }
        $result->delete();
        Session::set('services_number', Auth::user()->statistics()->services_count);
        return Redirect::action('UserController@wall');
    }
}

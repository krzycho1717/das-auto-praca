<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 18.01.2017
 * Time: 20:24:08
 */

namespace App\Helper;

use Pusher;


class PusherManager
{
    public static function trigger($channel, $event, array $data)
    {
        $pusher = new Pusher(env('PUSHER_KEY'), env('PUSHER_SECRET'), env('PUSHER_APP_ID'), [
            'cluster'   => 'eu'
        ]);
        $pusher->trigger($channel, $event, $data);
    }
}
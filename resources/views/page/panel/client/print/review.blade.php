<div class="row">

    <h2>{{ Lang::get('review.Reviews') }}</h2>
    <ul style="list-style: none">
        @if($reviews->count() == 0)
            <li><h3>Brak danych z tego okresu</h3></li>
        @else
        @foreach($reviews as $review)
            <li>{{ $review->created_at }}, {{ Lang::get('table.mileage') }} {{ $review->mileage }} km
                <p>
                    {{ $review->content }}
                </p>
            </li>
        @endforeach
        @endif
    </ul>
</div>
<div class="chart-container pt10 pb10 text-center">
    <button class="btn btn-link expand-chart">{{ Lang::get('ui.earning_chart_expand') }}</button>
    <button class="btn btn-link collapse-chart" style="display: none;">{{ Lang::get('ui.earning_chart_collapse') }}</button>
    <div class="chart-body" style="display: none">
        <h5>{{ Lang::get('ui.earning_chart_title').'('.\Carbon\Carbon::now()->subYear(1)->year.')' }}</h5>
        <script type="text/javascript" src="/javascript/Chart.min.js"></script>
        <canvas id="{{ $canvas_id }}" width="400" height="400"></canvas>
        <script>
            var data = {
                type: "{{ $type }}",
                data: {
                    labels: [{!! $labels !!}],
                    datasets: [{
                        label: "{{ $label }}",
                        scaleShowLabels : false,
                        data: {{ $data }},
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(49, 162, 96, 0.2)',
                            'rgba(64, 38, 13, 0.2)',
                            'rgba(65, 58, 127, 0.2)',
                            'rgba(85, 162, 139, 0.2)',
                            'rgba(156, 39, 176, 0.2)',
                            'rgba(255, 193, 7, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(49, 162, 96, 1)',
                            'rgba(64, 38, 13, 1)',
                            'rgba(65, 58, 127, 1)',
                            'rgba(85, 162, 139, 1)',
                            'rgba(156, 39, 176, 1)',
                            'rgba(255, 193, 7, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    tooltipTemplate: "[[if (label){]][[= label ]]: [[}]][[= value ]]zł",
                    responsive: true,
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            };
            var ctx = document.getElementById("{{ $canvas_id }}");
            var myChart = new Chart(ctx, data);
        </script>
    </div>
</div>
@extends('layouts.print')
@section('content')

<div class="row">
    <h1 class="text-center">{{ Lang::get('client.report') }}</h1>
    <div class="text-right">{{ Lang::get('ui.from_day').date('Y-m-d') }}</div>
    <div class="col-xs-6">
        <img src="data:image/png;base64,{{ $qr_code }}"/>
        <dl class="mt20">
            <dt class="text-muted">{{ Lang::get('table.name').' '.Lang::get('table.surname') }}</dt>
            <dd>{{ $client->fullname() }}</dd>
            <dt class="text-muted">{{ Lang::get('table.email') }}</dt>
            <dd>{{ $client->email }}</dd>
        </dl>
    </div>
    <div class="col-xs-6">
        <dl class="mt20">
            <dt class="text-muted">{{ Lang::get('table.address') }}</dt>
            <dd>{{ $client->address }}</dd>
            <dt class="text-muted">{{ Lang::get('table.city') }}</dt>
            <dd>{{ $client->city }}</dd>
            <dt class="text-muted">{{ Lang::get('table.country') }}</dt>
            <dd>{{ $client->country }}</dd>
            <dt class="text-muted">{{ Lang::get('table.phone') }}</dt>
            <dd>{{ $client->phone }}</dd>
        </dl>
    </div>
</div>
@if($extra == 0)
    @include('page.panel.client.print.service')
    @include('page.panel.client.print.review')
@elseif($extra == 1)
    @include('page.panel.client.print.service')
@elseif($extra == 2)
    @include('page.panel.client.print.review')
@endif
<div class="row" style="margin-top: 20px">
    <div class="col-xs-offset-6 col-xs-6">
        <div style="text-transform: capitalize">{{ Lang::get('ui.mechanic').': '.Auth::user()->fullname() }}</div>
        <div style="text-transform: capitalize">{{ Lang::get('ui.signature') }}:</div>
        <div style="border-bottom: 1px dotted black"></div>
    </div>
</div>
@stop
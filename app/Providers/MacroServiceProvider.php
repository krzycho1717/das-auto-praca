<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 30.03.2016
 * Time: 14:51
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


/**
 * Class MacroServiceProvider
 * Loads all macros from
 * @package App\Providers
 */
class MacroServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //require base_path() . '/resources/macros/macro1.php';
        //require base_path() . '/resources/macros/macro2.php';
        // etc...
        foreach(glob(base_path('resources/macros/*.macro.php')) as $filename){
            require_once($filename);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
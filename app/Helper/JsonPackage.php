<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 05.05.2016
 * Time: 17:34
 */

namespace App\Helper;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;

class JsonPackage
{
    public static function prepare($type, $data, $error, $code = null){
//        if($data != null && is_array($data) == fals){
//            $data = $data->toArray();
//        }
        if($code == null){
            return Response::$type([
                'version' => Config::get('app.api_version'),
                'data' => $data,
                'error' => $error
            ], 200, [], JSON_NUMERIC_CHECK);
        }
        return Response::$type([
            'version' => Config::get('app.api_version'),
            'data' => $data,
            'error' => $error
        ], $code, [], JSON_NUMERIC_CHECK);
    }
}
<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Cookie;

class Language
{
    public function handle($request, Closure $next)
    {
        if (Cookie::get('applocale') !== null) {
            App::setLocale(Cookie::get('applocale', 'pl'));
        }
        return $next($request);
    }
}

?>
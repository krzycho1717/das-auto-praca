<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {
    
    protected $table = 'services';
    protected $fillable = ['content', 'charge', 'date'];

    protected static $allowed_to_order = ['name', 'surname', 'charge', 'date'];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public static function canOrderBy($attribute){
        return in_array($attribute, self::$allowed_to_order);
    }

    // default date format for created_at
    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    // default date format for updated_at
    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}

?>
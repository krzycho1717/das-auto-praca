<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 13.07.2016
 * Time: 14:41
 */

Html::macro('chart', function($canvas_id, $configuration){
    $default_control = array(
        'canvas_id' => 'chart-canvas',
        'type' => 'bar',
        'data' => 0
    );
    $configuration['canvas_id'] = $canvas_id;

    $configuration = array_merge($default_control, $configuration);

    $data = array();

    for($i = 0; $i < 12; $i++){
        if(array_has($configuration['data'], $i)){
            $data[$i] = floatval($configuration['data'][$i]);
        }else{
            $data[$i] = floatval(0);
        }
    }

    $months = "";
    for ($i = 1; $i <= 12; $i++){
        if($i < 12)
            $months .= "'".Lang::get('ui.month_'.$i)."',";
        else
            $months .= "'".Lang::get('ui.month_'.$i)."'";
    }

    $html = View::make('layouts.chart')
        ->with('canvas_id', $configuration['canvas_id'])
        ->with('type', $configuration['type'])
        ->with('data', json_encode($data))
        ->with('labels', $months)
        ->with('label', Lang::get('ui.earning_chart_title'))
        ->render();

    return new \Illuminate\Support\HtmlString($html);
});
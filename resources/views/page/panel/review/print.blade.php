@extends('layouts.print')
@section('content')

<div class="row">
    <h1 class="text-center">{{ Lang::get('review.Review') }}</h1>
    <div class="text-right">{{ Lang::get('table.performed').': '.$review->date }}</div>
    <div class="col-xs-6">
        <img src="data:image/png;base64,{{ $qr_code }}"/>
        <dl class="mt20">
            <dt class="text-muted">{{ Lang::get('table.name').' '.Lang::get('table.surname') }}</dt>
            <dd>{{ $review->client->fullname() }}</dd>
            <dt class="text-muted">{{ Lang::get('table.email') }}</dt>
            <dd>{{ $review->client->email }}</dd>
        </dl>
    </div>
    <div class="col-xs-6">
        <dl class="mt20">
            <dt class="text-muted">{{ Lang::get('table.address') }}</dt>
            <dd>{{ $review->client->address }}</dd>
            <dt class="text-muted">{{ Lang::get('table.city') }}</dt>
            <dd>{{ $review->client->city }}</dd>
            <dt class="text-muted">{{ Lang::get('table.country') }}</dt>
            <dd>{{ $review->client->country }}</dd>
            <dt class="text-muted">{{ Lang::get('table.phone') }}</dt>
            <dd>{{ $review->client->phone }}</dd>
        </dl>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <dl class="mt20">
            <dt class="text-muted">{{ Lang::get('table.auto') }}</dt>
            <dd>{{ $review->car }}</dd>
            <dt class="text-muted">{{ Lang::get('table.plate') }}</dt>
            <dd>{{ $review->reg }}</dd>
            <dt class="text-muted">{{ Lang::get('table.vin') }}</dt>
            <dd>{{ $review->vin }}</dd>
            <dt class="text-muted">{{ Lang::get('table.mileage') }} km</dt>
            <dd>{{ $review->mileage }}</dd>
        </dl>
    </div>
    <div class="col-xs-6">
        <dl class="mt20">
            <dt class="text-muted">{{ Lang::get('table.og1L') }}</dt>
            <dd>{{ $review->tiresFrontLeft }}</dd>
            <dt class="text-muted">{{ Lang::get('table.og1P') }}</dt>
            <dd>{{ $review->tiresFrontRight }}</dd>
            <dt class="text-muted">{{ Lang::get('table.og2L') }}</dt>
            <dd>{{ $review->tiresBackLeft }}</dd>
            <dt class="text-muted">{{ Lang::get('table.og2P') }}</dt>
            <dd>{{ $review->tiresBackRight }}</dd>
        </dl>
    </div>
</div>

<dl class="mt20">
    @for($i = 1; $i <= 27; $i++)
        @php($in = 'n'.$i)
        @if(strlen(trim($review->$in)) > 0)
            <dt class="text-muted">{{ Lang::get('table.'.$in) }}</dt>
            <dd>{{ $review->$in }}</dd>
        @endif
    @endfor
</dl>

<div class="row" style="margin-top: 20px">
    <div class="col-xs-offset-6 col-xs-6">
        <div style="text-transform: capitalize">{{ Lang::get('ui.mechanic').': '.Auth::user()->fullname() }}</div>
        <div style="text-transform: capitalize">{{ Lang::get('ui.signature') }}:</div>
        <div style="border-bottom: 1px dotted black"></div>
    </div>
</div>

@stop
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('client_id')->unsigned()->index();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->string('vin', 17);
            $table->string('car');
            $table->string('reg');
            $table->integer('mileage')->unsigned();
            $table->mediumText('comment');
            $table->decimal('tiresFrontLeft', 2, 2);
            $table->decimal('tiresFrontRight', 2, 2);
            $table->decimal('tiresBackLeft', 2, 2);
            $table->decimal('tiresBackRight', 2, 2);
            $table->mediumText('n1');
            $table->mediumText('n2');
            $table->mediumText('n3');
            $table->mediumText('n4');
            $table->mediumText('n5');
            $table->mediumText('n6');
            $table->mediumText('n7');
            $table->mediumText('n8');
            $table->mediumText('n9');
            $table->mediumText('n10');            
            $table->mediumText('n11');
            $table->mediumText('n12');
            $table->mediumText('n13');
            $table->mediumText('n14');
            $table->mediumText('n15');
            $table->mediumText('n16');
            $table->mediumText('n17');
            $table->mediumText('n18');
            $table->mediumText('n19');
            $table->mediumText('n20');            
            $table->mediumText('n21');
            $table->mediumText('n22');
            $table->mediumText('n23');
            $table->mediumText('n24');
            $table->mediumText('n25');
            $table->mediumText('n26');
            $table->mediumText('n27');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}

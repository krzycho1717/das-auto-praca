process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.stylesIn("public/css");

    mix.less('app.less');
    mix.less('print.less');

    mix.sass('materialdesignicons/materialdesignicons.scss');
    mix.sass('bootstrap/bootstrap.scss');

    //mix.webpack('vue/v_settings.js', 'public/javascript');

    //mix.styles('bootstrap.css');
    //mix.styles('bootstrap-print.css');

    mix.copy('node_modules/clipboard/dist/clipboard.min.js', 'public/javascript/');
    mix.copy('node_modules/chart.js/dist/Chart.min.js', 'public/javascript/');
    mix.copy('node_modules/semantic-ui-popup/popup.js', 'public/javascript/');
    mix.copy('node_modules/semantic-ui-popup/popup.css', 'public/css/');
    mix.copy('node_modules/semantic-ui-transition/transition.js', 'public/javascript/');
    mix.copy('node_modules/semantic-ui-transition/transition.css', 'public/css/');
    mix.copy('resources/assets/js/panel.js', 'public/javascript/');
    mix.copy('resources/assets/js/user.js', 'public/javascript/');
    mix.copy('resources/assets/js/toast.js', 'public/javascript/');
    mix.copy('resources/assets/js/jquery.qrcode.min.js', 'public/javascript/');
    mix.copy('resources/assets/js/jquery.inputmask.bundle.js', 'public/javascript/');
});

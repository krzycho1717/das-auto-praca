<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'client.start' => 'Clients',
    'client.new'   => 'New client',
    'client.show'  => 'Client',
    'client.edit'  => 'Client - edition',

    'service.start' => 'Services',
    'service.new'   => 'New service',
    'service.show'  => 'Service',
    'service.edit'  => 'Service - edition',

    'review.start' => 'Reviews',
    'review.new'   => 'New review',
    'review.show'  => 'Review',
    'review.edit'  => 'Review - edition',
];
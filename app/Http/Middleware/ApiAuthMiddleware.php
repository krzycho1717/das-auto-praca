<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 03.07.2016
 * Time: 17:16
 */

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;
use Illuminate\Support\Facades\Auth;
use JsonPackage;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $udid = $request->server('PHP_AUTH_USER');
        $token = $request->server('PHP_AUTH_PW');

        if($token == '' || $udid == '')
            return JsonPackage::prepare('json', null, 'unauthorised', 401);
        else{
            $token = Token::where('token', $token)->where('udid', $udid)->first();
            if($token == null)
                return JsonPackage::prepare('json', null, 'unauthorised', 401);
            else{
                if (Auth::loginUsingId($token->user()->first()->id)) {
                    return $next($request);
                } else {
                    return JsonPackage::prepare('json', null, 'unauthorised', 401);
                }
            }
        }
    }

    // public function handle($request, Closure $next)
    // {
    //     $udid = $request->server('PHP_AUTH_USER');
    //     $token = $request->server('PHP_AUTH_PW');

    //     if($token == '' || $udid == '')
    //         return response(['message' => 'unauthorised'], 401, ['WWW-Authenticate' => 'Basic']);
    //     else{
    //         $token = Token::where('token', $token)->where('udid', $udid)->first();
    //         if($token == null)
    //             return response(['message' => 'unauthorised'], 401, ['WWW-Authenticate' => 'Basic']);
    //         else{
    //             if (Auth::loginUsingId($token->user()->first()->id)) {
    //                 return $next($request);
    //             } else {
    //                 return response(['message' => 'unauthorised'], 401, ['WWW-Authenticate' => 'Basic']);
    //             }
    //         }
    //     }
    // }
}
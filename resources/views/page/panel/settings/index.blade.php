@extends('layouts.default')
@section('content')

<script type="text/javascript" src="/javascript/jquery.qrcode.min.js"></script>

<div class="modal fade modal-primary" id="qr-modal" role="dialog">
    <div class="modal-dialog" style="width: 390px">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body text-center">

            </div>
            <div class="modal-footer text-center">
                {{ Lang::get('ui.qrcode_scann_title') }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-danger" id="delete-device-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
            </div>
            <div class="modal-body">
                {{ Lang::get('ui.confirm_delete.message') }}
            </div>
            <div class="modal-footer">
                <button class="btn btn-default delete-cancel" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.cancel') }}</button>
                <button class="btn btn-primary delete-confirm" id="delete-device-confirm" data-bb-handler="confirm">{{ Lang::get('ui.ok') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-danger" id="delete-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ Lang::get('ui.confirm_delete.message') }}</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default delete-cancel inline" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.cancel') }}</a>
                {{ Form::open(['method' => 'DELETE', 'action' => ['UserController@destroy', Auth::user()->id], 'class' => 'inline' ]) }}
                {{ Form::button(Lang::get('ui.ok'), ['class' => 'btn btn-primary', 'type' => 'submit']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default plain">
    <!-- Start .panel -->
    <div class="panel-heading">
        <h4 class="panel-title">{{ Lang::get('ui.profile.show') }}</h4>
    </div>
    <div class="panel-body">
        <div class="row profile">
            <!-- Start .row -->
            <div class="col-md-2">
                <div class="picture128 mb10">
                    <img src="/images/default.png" alt="Avatar" class="picture128">
                </div>
            </div>
            <div class="col-md-4">
                <div class="profile-name">
                    <h3>{{ Auth::user()->fullname() }}</h3>
                    <a id="edit-action" href="{{ URL::action('UserController@edit') }}" class="btn btn-primary btn-large mr10"><i class="mdi mdi-pen"></i>{{ Lang::get('ui.edit') }}</a>
                    <a id="open-delete-modal" href="#" class="btn btn-danger btn-large mr10"><i class="mdi mdi-delete"></i>{{ Lang::get('ui.delete') }}</a>
                </div>
            </div>
            <div class="col-md-6">
                <dl class="mt20">
                    <dt class="text-muted">{{ Lang::get('table.phone') }}</dt>
                    <dd>{{ Auth::user()->phone }}</dd>
                    <dt class="text-muted">{{ Lang::get('table.email') }}</dt>
                    <dd>{{ Auth::user()->email }}</dd>
                </dl>
            </div>
        </div>
        <script src="/javascript/clipboard.min.js"></script>
    </div>
    <div class="panel-body devices-body pl0 pr0">
        <div class="row">
            @include('page.panel.settings.devices')
        </div>
    </div>
    <div class="panel-footer clearfix">
        <div class="row token-form mt20" style="display: none">
            <div class="form-horizontal group-border stripped mr15 ml15">
                <div class="form-group">
                    <label class="col-lg-2 col-md-3 control-label">{{ Lang::get('ui.device_name') }}</label>
                    <div class="col-lg-10 col-md-9">
                        <input class="form-control" id="token"/>
                    </div>
                </div>
            </div>
        </div>
        <button class="generate-token btn btn-success pull-right ml10 mr15">{{ Lang::get('ui.add') }}</button>
        <button class="cancel-generate-token btn btn-danger pull-right hide">{{ Lang::get('ui.cancel') }}</button>
    </div>
</div>
<script>
    var channel_token = pusher.subscribe('token-channel');
</script>
@stop
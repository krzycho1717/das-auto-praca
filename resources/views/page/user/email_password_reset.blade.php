<h4>{{ Lang::get('ui.email.password_reset') }}</h4>
<div style="text-align: center">
    <a class="btn btn-primary" href="{{ Config::get('app.url').'/password/reset/'.$user->email.'/'.$user->password_reset_hash.' }}">{{ Lang::get('ui.email.change_password') }}</a>
</div>
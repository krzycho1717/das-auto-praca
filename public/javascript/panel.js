jQuery.fn.extend({
    slideRightShow: function(t, callback) {
        return this.each(function(t, callback) {
            $(this).show('slide', {direction: 'right'}, t, callback);
        });
    },
    slideLeftHide: function(t, callback) {
        return this.each(function() {
            $(this).hide('slide', {direction: 'left'}, t, callback);
        });
    },
    slideRightHide: function(t, callback) {
        return this.each(function() {
            $(this).hide('slide', {direction: 'right'}, t, callback);
        });
    },
    slideLeftShow: function(t, callback) {
        return this.each(function() {
            $(this).show('slide', {direction: 'left'}, t, callback);
        });
    }
});

$(document).ready(function() {

    var body = $('body');

    $('.input-link').click(function () {
        if ( $(this).attr('data-link') ){
            if ($(this).data('link-target')){
                window.open($(this).data('link'), $(this).data('link-target'));
            }else {
                window.location = $(this).data('link');
            }
        }
    });

    //if($('.table').length) {

        $(document).click(function(event) {
            if(!$(event.target).is('.floating-options > i.mdi')) {
                if($('.tool-menu-container > .toolButtons').is(":visible")) {
                    $('.floating-options i.mdi').removeClass('mdi-close-circle-outline');
                    $('.floating-options i.mdi').addClass('mdi-dots-vertical');
                    $('.tool-menu-container > .toolButtons').slideLeftHide(250);
                }
            }
        });

        $('.table .floating-options, .panel-heading .floating-options').click(function () {
            if ($(this).next('.toolButtons').is(':visible')){
                $('i.mdi', this).removeClass('mdi-close-circle-outline');
                $('i.mdi', this).addClass('mdi-dots-vertical');
                $(this).next('.toolButtons').slideLeftHide(250);
            }else{
                var self = this;
                var toolButtons = $(this).next('.toolButtons');
                toolButtons.css('top', (-22.66666666666667*toolButtons.children().length));
                $('.table .toolButtons, .panel-heading .toolButtons').slideLeftHide(250).promise().done(function () {
                    $('.table .floating-options > i.mdi, .panel-heading .floating-options > i.mdi').removeClass('mdi-close-circle-outline').addClass('mdi-dots-vertical');
                    $('i.mdi', self).removeClass('mdi-dots-vertical');
                    $('i.mdi', self).addClass('mdi-close-circle-outline');
                    $(self).next('.toolButtons').slideLeftShow(250);
                });
            }
        });

        $('.table td:not(.tool-menu)').click(function () {
            var tr = $(this).parent();
            if ( !$('.toolButtons').is(':visible') && $(tr).data('link')) {
                if ($(tr).data('link-target')){
                    window.open($(tr).data('link'), $(tr).data('link-target'));
                }else {
                    window.location = $(tr).data('link');
                }
            }
        });

    //}

    body.on('click', '.toggle-sidebar', function () {
        body.toggleClass('min-mode');
        $('div.toggle-sidebar').toggleClass('hide');
        $('span.toggle-sidebar').toggleClass('hide');

        var v = false;
        if (body.hasClass('min-mode')){
            v  = true;
        }else{
            v = false;
        }

        $.ajax({
            url: '/panel/settings/update',
            type: 'POST',
            data: {
                key: 'sidebar_collapsed',
                value: v
            },
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
            },
            success: function (response) {
                if (response.error != null) {
                    console.error(response.error);
                } else {
                    if (response.data != null){}
                }
            }
        });
    });

    // token methods
    if(window.location.pathname == '/panel/settings'){
        console.info('settings page');

        $('.clipboard-button').popup({
            trigger: 'manual'
        });

        var clipboard = new Clipboard('.clipboard-button');
        clipboard.on('success', function(e) {
            $(e.trigger).popup('show', {
               content: $(e.trigger).data('clipboard-message')
            });
            e.clearSelection();
        });

        clipboard.on('error', function(e) {
            console.error('Action:', e.action);
            console.error('Trigger:', e.trigger);
        });

        function reload_tokens() {
            $.ajax({
                url: window.location.pathname + '/token',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
                },
                success: function (response) {
                    if (response.error != null) {
                        console.error(response.error);
                    } else {
                        if (response.data != null)
                            $('.devices-body').html(response.data.html);
                    }
                }
            });
        }

        function token_activated_callback(data) {
            console.info('Pusher::callback(data) %o', data);
            if (data != null && 'success' in data && data.success) {
                reload_tokens();
                $('#qr-modal').modal('hide');
            }
        }

        $('#qr-modal').on('hidden.bs.modal', function () {
            channel_token.unbind('activated-event', token_activated_callback);
        });

        body.on('click', '.item-device > .item-up .mdi-delete', function () {
            console.info('delete udid');
            var token_container = $(this).closest('.token-container');
            var id = token_container.data('token-id');

            $.ajax({
                url: window.location.pathname + '/token',
                type: 'PUT',
                data: {
                    type: 'RESET',
                    id: id
                },
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
                },
                success: function (response) {
                    if (response.error != null) {
                        console.error(response.error);
                    } else {
                        if (response.data != null) {
                            $('.devices-body').html(response.data.html);
                        }
                    }
                }
            });
        });

        body.on('click', '.item-token > .item-up .mdi-reload', function () {
            console.info('reload token');
            var token_container = $(this).closest('.token-container');
            var id = token_container.data('token-id');

            $.ajax({
                url: window.location.pathname + '/token',
                type: 'PUT',
                data: {
                    type: 'REGEN',
                    id: id
                },
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
                },
                success: function (response) {
                    if (response.error != null) {
                        console.error(response.error);
                    } else {
                        if (response.data != null)
                            $('.devices-body').html(response.data.html);
                    }
                }
            });
        });

        body.on('click', '.item-token > .item-up .mdi-qrcode', function () {
            var token = $(this).parent().data('qr-token');
            console.info(token);
            $('#qr-modal .modal-body').html('').qrcode(token);
            $('#qr-modal').modal();
            channel_token.bind('activated-event', token_activated_callback);
        });

        $('.generate-token').click(function () {
            if ($('.token-form').is(':visible')) {

                $('.token-form, .cancel-generate-token').slideUp('fast');

                var name = $('input#token').val();

                $('input#token').val('');

                console.info('generate-token');
                $.ajax({
                    url: window.location.pathname + '/token',
                    type: 'POST',
                    data: {
                        name: name
                    },
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
                    },
                    success: function (response) {
                        if (response.error != null) {
                            console.error(response.error);
                        } else {
                            if (response.data != null)
                                $('.devices-body').html(response.data.html);
                        }
                    }
                });
            }else{
                $('.token-form, .cancel-generate-token').slideDown('fast', function () {
                    $('input#token').focus();
                });
            }
        });

        $('input#token').keypress(function (e) {
            if (e.which == 13) {
                $('.token-form, .cancel-generate-token').slideUp('fast');

                var name = $('input#token').val();

                $('input#token').val('');

                console.info('generate-token');
                $.ajax({
                    url: window.location.pathname + '/token',
                    type: 'POST',
                    data: {
                        name: name
                    },
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
                    },
                    success: function (response) {
                        if (response.error != null) {
                            console.error(response.error);
                        } else {
                            if (response.data != null)
                                $('.devices-body').html(response.data.html);
                        }
                    }
                });
                return false;
            }
        });

        $('.cancel-generate-token').click(function () {
            $('.token-form, .cancel-generate-token').slideUp('fast');
        });

        body.on('click', '.delete-button', function () {
            var id = $(this).closest('.token-container').data('token-id');
            var dialog = $('#delete-device-modal');
            dialog.data('token-id', id);
            dialog.modal();
        });

        $('#delete-device-confirm').click(function () {
            var id = $('#delete-device-modal').data('token-id');

            $.ajax({
                url: window.location.pathname + '/token',
                type: 'DELETE',
                data: {
                    id: id
                },
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
                },
                success: function (response) {
                    if (response.error != null) {
                        console.error(response.error);
                    } else {
                        if (response.data != null) {
                            $('.devices-body').html(response.data.html);
                            console.info('sending remove event');
                            channel_app.trigger('device-remove-event', { token_id: id });
                        }
                    }
                    $('#delete-device-modal').modal('hide');
                }
            });
        });
    }

    if (window.location.pathname.indexOf('/panel') != -1){

        channel_app.bind('logged-in-event', application_logged_in_callback);
        channel_app.bind('logged-out-event', application_logged_out_callback);

        function application_logged_in_callback(data) {
            console.info('Pusher::callback(data) %o', data);
            if (!$('#message-login').is(':visible')){
                $('#message-login > span > b').text(new Date(new Date().getTime()).toLocaleTimeString());
                $('#message-login').slideDown(200, function () {
                    console.info('logged-in-event');
                    $('#message-login').delay(5000).fadeOut('slow');
                });
            }
        }

        function application_logged_out_callback(data) {
            console.info('Pusher::callback(data) %o', data);
            if (!$('#message-logout').is(':visible')){
                $('#message-logout > span > b').text(new Date(new Date().getTime()).toLocaleTimeString());
                $('#message-logout').slideDown(200, function () {
                    $('#message-logout').delay(5000).fadeOut('slow');
                });
            }
        }

        var chart_body = $('.chart-container > .chart-body');
        $('.expand-chart').click(function () {
            $(this).hide();
            $('.collapse-chart').show();
            chart_body.css('height', 'auto').slideDown(450);
        });

        $('.collapse-chart').click(function () {
            chart_body.css('height', 'auto').slideUp(450);
            $(this).hide();
            $('.expand-chart').show();
        });
    }

    // user delete method
    $('#open-delete-modal').click(function (event) {
        event.preventDefault();
        $('#delete-modal').modal();
    });

    $('.delete-cancel').click(function(){
        $('.modal').modal('hide');
    });

    $('.toolButtons a.btn').click(function () {
        $('.table .toolButtons').slideLeftHide(250);
        $('.table .floating-options > i.mdi').removeClass('mdi-close-circle-outline').addClass('mdi-dots-vertical');
    });

    $('.toolButtons a.mdi-delete').click(function(event){
        event.preventDefault();
        console.info('test');
        var delete_url = $(this).attr('href');
        $('.delete-confirm').attr('href', delete_url);
        $('#delete-form').attr('action', delete_url);
        $('#delete-modal').modal();
    });

    $('.toolButtons > #delete-action').click(function(event){
        event.preventDefault();
        var delete_url = $(this).closest('a').attr('href');
        $('.delete-confirm').attr('href', delete_url);
        $('#delete-modal').modal();
    });

    // search
    $('header').on('click', '#advanced-search', function () {
        $('.advanced-search-box').toggleClass("hidden");
    });
    $('#keyword').popup({
       'on': 'manual'
    });
    $('.select-type').click(function () {
        $('#select-button').html($(this).text()+'<span class="caret ml5"></span>');
        console.info($(this).data('value'));
        $('#search_filter').attr('data-selected', $(this).data('value'));
    });
    $('#search_button').click(function () {
        if ($('#keyword').val().length >= 4) {
            var keyword = $('#keyword').val();
            var type = $('#search_filter').data('selected');

            console.log(keyword);
            console.log(type);

            window.location = '/panel/search/' + type + '/' + keyword;
        }else{
            console.info('not enough characters');
            $('#keyword').popup('show',{
                'popup' : $('.popup-search')
            });
        }
    });
    $('#keyword').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $('#search_button').click();//Trigger search button click event
        }
    });

});
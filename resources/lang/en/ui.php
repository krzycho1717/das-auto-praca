<?php

return [

    /*
    |--------------------------------------------------------------------------
    | UI Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'add'                       => 'add',
    'cancel'                    => 'anuluj',
    'confirm_delete'            => [
        'message'   => 'Are you sure You want to delete ?',
        'title'     => 'Delete confirmation',
    ],
    'copy_to_clipboard'         => 'copy to clipboard',
    'delete'                    => 'delete',
    'earning_chart_collapse'    => 'show less',
    'earning_chart_expand'      => 'show more',
    'earning_chart_title'       => 'Average earnings in a month',
    'edit'                      => 'edit',
    'call'                      => 'call',
    'show_empty_fields'         => 'show empty fields',
    'hide_empty_fields'         => 'hide empty fields',
    'mechanic'                  => 'mechanic',
    'signature'                 => 'signature',
    'hint'                      => 'Hint',
    'from'                      => 'From',
    'to'                        => 'To',
    'select_range'              => 'Select range',
    'device_hint'               => 'Here you can grant or revoke device access to Your account data.<br>Just click <kbd>add</kbd> type in name of device, and <span class="text-info">scan</span> QR code <i class="mdi mdi-qrcode"></i> when logging into app.<br>Removing device from the list below will revoke access permission.',
    'device_name'               => 'Device name',
    'from_day'                  => 'From day: ',
    'generate_report'           => [
        'label'         => 'generate report',
        'ready'         => 'Ready',
        'last_month'    => 'last month',
        'last_week'     => 'last week',
        'select_date'   => 'select date',
        'custom'        => 'custom report',
        'with_services' => 'with services',
        'with_reviews'  => 'with reviews',
        'with_all'      => 'with all',
    ],
    'export'                    => [
        'label'         => 'export to file',
        'format_xls'     => 'Microsoft Excel (XLS)',
        'format_xlsx'     => 'Microsoft Excel (XLSX)',
        'format_csv'     => 'Text file (CSV)',
    ],
    'info_basic'                => 'Basic information',
    'info_additional'           => 'Additional information',
    'map'                       => [
        'dir'       => 'Map directions',
        'warning'   => [
            'header'    => 'Coudn\'t find address',
            'message'   => 'Are you sure that given address is correct?'
        ]
    ],
    'lang'                      => 'Language',
    'lang_en'                   => 'English',
    'lang_pl'                   => 'Polish',
    'logout'                    => 'logout',
    'last_seen'                 => 'LAST SEEN',
    'month_1'                   => 'January',
    'month_10'                  => 'October',
    'month_11'                  => 'November',
    'month_12'                  => 'December',
    'month_2'                   => 'February',
    'month_3'                   => 'March',
    'month_4'                   => 'April',
    'month_5'                   => 'May',
    'month_6'                   => 'June',
    'month_7'                   => 'July',
    'month_8'                   => 'August',
    'month_9'                   => 'September',
    'navigation'                => 'Navigation',
    'ok'                        => 'ok',
    'profile'                   => [
        'show'  => 'Profile',
        'edit'  => 'Profile - edit',
    ],
    'print'                     => 'print',
    'qrcode'                    => 'show qr code',
    'qrcode_scann_title'        => 'scann qr code with application to activate device',
    'reset'                     => 'reset',
    'return'                    => 'return',
    'save'                      => 'save',
    'download_app'              => 'Download application',
    'version'                   => 'version',
    'search'                    => 'search',
    'adv_search'                => 'advanced search',
    'search_failed'             => 'not found',
    'search_placeholder'        => 'search on website',
    'search_title'              => 'Search result for <span class="label label-info">:keyword</span> in <span class="label label-success">:type</span>',
    'settings'                  => 'settings',
    'start'                     => 'start',
    'toast_clipboard_message'   => 'Text has been copied to clipboard',
    'email'                     => [
        'send'             => 'send',
        'verify_sended'    => 'A confirmation email has been sent to :email. Click on the confirmation link in the email to activate your account.',
        'verify_success'   => 'Your account was verified successfully.',
        'verify_failure'   => 'Ops. Something went wrong with verification.',
        'subject'          => [
            'password_reset'    => 'DasAuto - password reset',
            'activation'        => 'DasAuto - account verification'
        ],
        'resend'           => 'Resend',
        'no_reply'         => 'This is an automatically generated email, please do not reply',
        'welcome'          => 'Welcome to DasAuto',
        'activation'       => 'Click below to verify Your email',
        'password_reset'   => 'Click below to change your password',
        'change_password'  => 'change password',
        'verify'           => 'verify'
    ]
];

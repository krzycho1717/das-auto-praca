<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model {
    
    protected $table = 'tokens';
    protected $fillable = ['name', 'token', 'udid'];
    protected $dates = ['created_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    // default date format for created_at
    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    // default date format for updated_at
    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}

?>
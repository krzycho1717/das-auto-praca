<?php

namespace App\Http\Controllers;


use App;
use App\Models\Client;
use Auth;
use Carbon\Carbon;
use Excel;
use Input;
use Lang;
use Redirect;
use App\Helper\JsonPackage;
use Session;
use Validator;
use View;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Client::where('user_id', Auth::user()->id)
        ->orderBy('created_at', 'desc')
            ->paginate(10);
        
        return View::make('page.panel.client.index')
            ->with('clients', $result)
            ->with('title', Lang::get('title.client.start'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return View::make('page.panel.client.create')
            ->with('title', Lang::get('title.client.new'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $rules = array(
            'name'    => 'required',
            'surname'    => 'required',
            'phone'    => 'required',
            'email'    => 'required|email',
            'country'    => 'required',
            'city'    => 'required',
            'address'    => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput(Input::all())
                ->withErrors($validator);
        } else {
            $client = new Client();
            $client->user_id = Auth::user()->id;
            $client->name = Input::get('name');
            $client->surname = Input::get('surname');
            $client->phone = Input::get('phone');
            $client->email = Input::get('email');
            $client->country = Input::get('country');
            $client->city = Input::get('city');
            $client->address = Input::get('address');

            $client->save();

            return Redirect::action('UserController@wall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            abort(404);
        }
        $gmp = null;
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://maps-api-ssl.google.com/maps/api/geocode/json']);
        $response_gmp = $client->request('GET', '?address='.$result->address.','.$result->city.','.$result->country);
        if($response_gmp->getStatusCode() == 200){
            $gmp = \GuzzleHttp\json_decode($response_gmp->getBody());
        }
        $s_count = $result->services()->count();
        $r_count = $result->reviews()->count();
        return View::make('page.panel.client.show')
            ->with('client', $result)
            ->with('gmp', $gmp)
            ->with('s_count', $s_count)
            ->with('r_count', $r_count)
            ->with('title', Lang::get('title.client.show'));
    }

    public function show_ordered($attribute, $order){
        if(!Client::canOrderBy($attribute)) return Redirect::to('/panel/client');
        $result = Auth::user()->clients()
            ->orderBy($attribute, $order)
            ->paginate(25);
        return View::make('page.panel.client.index')
            ->with('clients', $result)
            ->with('title', Lang::get('title.client.start'));
    }

    /**
     * Display print ready client page
     * @param $id
     * @param string $type
     * @return \Illuminate\Http\Response
     * @internal param id $int
     */
    public function export($id, $type='xls'){
        $client = Auth::user()->clients()->find($id);
        if($client == null){
            abort(404);
        }
        ob_end_clean();
        ob_start();
        Excel::create('client_0_' . $client->id . Carbon::now(), function($excel) use($client) {

            $services = $client->services()->get();
            $reviews = $client->reviews()->get();

            // Set the title
            $excel->setTitle(Lang::get('client.Client').' - '.$client->id);

            // Chain the setters
            $excel->setCreator(Auth::user()->fullname())->setCompany('DasAuto');

             $excel->sheet(Lang::get('client.client'), function($sheet) use($client) {
                 $sheet->setColumnFormat(array(
                     'J' => 'yyyy-mm-dd',
                 ));
                 $sheet->cell('A1:J1', function($cell) {
                     $cell->setBorder('solid', 'solid', 'solid', 'solid');
                     $cell->setBackground('#4F81BD');
                     $cell->setFontColor('#ffffff');
                 });
                 $sheet->row(1, [ 'id', 'user_id', Lang::get('table.name'), Lang::get('table.surname'), Lang::get('table.phone'), Lang::get('table.email'), Lang::get('table.address'), Lang::get('table.city'), Lang::get('table.country'), Lang::get('table.created_at') ]);
                 $sheet->row(2, [$client->id, $client->user_id, $client->name, $client->surname, $client->phone, $client->email, $client->address, $client->city, $client->country, $client->created_at]);
             });

            $excel->sheet(Lang::get('service.services'), function($sheet) use($services) {
                $sheet->setColumnFormat(array(
                    'F'   => '#,###.000"zł"',
                    'H' => 'yyyy-mm-dd',
                ));
                $sheet->cell('A1:H1', function($cell) {
                    $cell->setBorder('solid', 'solid', 'solid', 'solid');
                    $cell->setBackground('#4F81BD');
                    $cell->setFontColor('#ffffff');
                });
                $sheet->row(1, ['id', 'user_id', Lang::get('table.name'), Lang::get('table.surname'), Lang::get('table.email'), Lang::get('table.charge'), Lang::get('table.content'), Lang::get('table.performed')]);
                if ($services->count() > 0) {
                    $ix = 2;
                    foreach ($services as $service){
                        $sheet->row($ix, [
                            $service->id,
                            $service->user_id,
                            $service->client->name,
                            $service->client->surname,
                            $service->client->email,
                            $service->charge,
                            strip_tags($service->content),
                            $service->date
                        ]);
                    }
                }
            });

             $excel->sheet(Lang::get('review.reviews'), function($sheet) use($reviews) {
                 $sheet->setColumnFormat(array(
                     'D'   => '#,###.000"Km"',
                     'F:I' => '0.00',
                     'J' => 'yyyy-mm-dd',
                 ));
                 $sheet->cell('A1:AK1', function($cell) {
                     $cell->setBorder('solid', 'solid', 'solid', 'solid');
                     $cell->setBackground('#4F81BD');
                     $cell->setFontColor('#ffffff');
                 });
                 $sheet->row(1, [
                     Lang::get('table.plate'),
                     Lang::get('table.vin'),
                     Lang::get('table.auto'),
                     Lang::get('table.mileage'),
                     Lang::get('table.comment'),
                     Lang::get('table.og1L'),
                     Lang::get('table.og1P'),
                     Lang::get('table.og2L'),
                     Lang::get('table.og2P'),
                     Lang::get('table.performed'),
                     Lang::get('table.n1'),
                     Lang::get('table.n2'),
                     Lang::get('table.n3'),
                     Lang::get('table.n4'),
                     Lang::get('table.n5'),
                     Lang::get('table.n6'),
                     Lang::get('table.n7'),
                     Lang::get('table.n8'),
                     Lang::get('table.n9'),
                     Lang::get('table.n10'),
                     Lang::get('table.n11'),
                     Lang::get('table.n12'),
                     Lang::get('table.n13'),
                     Lang::get('table.n14'),
                     Lang::get('table.n15'),
                     Lang::get('table.n16'),
                     Lang::get('table.n17'),
                     Lang::get('table.n18'),
                     Lang::get('table.n19'),
                     Lang::get('table.n20'),
                     Lang::get('table.n21'),
                     Lang::get('table.n22'),
                     Lang::get('table.n23'),
                     Lang::get('table.n24'),
                     Lang::get('table.n25'),
                     Lang::get('table.n26'),
                     Lang::get('table.n27')]);
                 if ($reviews->count() > 0) {
                     $ix = 2;
                     foreach ($reviews as $review) {
                         $sheet->row($ix, [$review->reg, $review->vin, $review->car, $review->mileage, strip_tags($review->comment), $review->tiresFrontLeft, $review->tiresFrontRight, $review->tiresBackLeft, $review->tiresBackRight, $review->date, $review->n1, $review->n2, $review->n3, $review->n4, $review->n5, $review->n6, $review->n7, $review->n8, $review->n9, $review->n10, $review->n11, $review->n12, $review->n13, $review->n14, $review->n15, $review->n16, $review->n17, $review->n18, $review->n19, $review->n20, $review->n21, $review->n22, $review->n23, $review->n24, $review->n25, $review->n26, $review->n27]);
                     }
                 }
             });

        })->download($type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            abort(404);
        }

        return View::make('page.panel.client.edit')
            ->with('client', $result)
            ->with('title', Lang::get('title.client.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            abort(404);
        }

        $rules = array(
            'name'    => 'required',
            'surname'    => 'required',
            'phone'    => 'required',
            'email'    => 'required|email',
            'country'    => 'required',
            'city'    => 'required',
            'address'    => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {

            $userdata = array(
                'name' => Input::get('name'),
                'surname' => Input::get('surname'),
                'phone' => Input::get('phone'),
                'email' => Input::get('email'),
                'country' => Input::get('country'),
                'city' => Input::get('city'),
                'address' => Input::get('address'),
            );

            $result->update($userdata);

            return Redirect::action('ClientController@show', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Auth::user()->clients()->find($id);

        if($result == null){
            abort(404);
        }
        $result->delete();
        Session::set('clients_number', Auth::user()->statistics()->clients_count);
        return Redirect::to('/panel');
    }
}

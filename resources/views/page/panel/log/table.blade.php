@foreach($logs as $key => $value)

    <div class="row">
        <div class="col s12 m6">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                    <i class="mdi mdi-close pull-right"></i>
                        @if($value->type == 'create')
                            <div class="chip color-green no-selection" style="width: 80px">
                                {{ $value->type }}
                                <div>
                                    ACTION
                                </div>
                            </div>
                        @elseif($value->type == 'read')
                            <div class="chip color-light-blue no-selection" style="width: 80px">
                                {{ $value->type }}
                                <div>
                                    ACTION
                                </div>
                            </div>
                        @elseif($value->type == 'update')
                            <div class="chip color-amber no-selection" style="width: 80px">
                                {{ $value->type }}
                                <div>
                                    ACTION
                                </div>
                            </div>
                        @elseif($value->type == 'delete')
                            <div class="chip color-red no-selection" style="width: 80px">
                                {{ $value->type }}
                                <div>
                                    ACTION
                                </div>
                            </div>
                        @endif
                    @if($value->affected == 'services')
                        <a href="{{ URL::action('ServiceController@show', [$value->getExtraAsArray()->id, $value->getExtraAsArray()->id]) }}" class="chip color-blue-grey no-selection" style="width: 100px">{{ Lang::get('service.service') }}</a>
                    @elseif($value->affected == 'reviews')
                        <a href="{{ URL::action('ReviewController@show', [$value->getExtraAsArray()->id, $value->getExtraAsArray()->id]) }}" class="chip color-blue-grey no-selection" style="width: 100px">{{ Lang::get('review.review') }}</a>
                    @elseif($value->affected == 'clients')
                        <a href="{{ URL::action('ClientController@show', $value->getExtraAsArray()->id) }}" class="chip color-blue-grey no-selection" style="width: 100px">{{ Lang::get('client.client') }}</a>
                    @endif
                    <div class="chip color-red no-selection" style="width: 100px">{{ $value->created_at }}</div>
                    FROM
                    <div class="chip color-red no-selection" style="width: 100px">{{ $value->from_ip }}</div>
                </div>
                {{--<div class="card-action">--}}
                    {{--<a href="#">This is a link</a>--}}
                    {{--<a href="#">This is a link</a>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

@endforeach

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">

        <title>DasAuto</title>

        <!-- Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,700,400&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" media="print,screen" href="/css/print.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" media="print,screen" href="/css/bootstrap-print.css">

        <script type="text/javascript">
            window.print();
        </script>
    </head>
    <body>
        <div class="col-md-12">
            @yield('content')
        </div>
    </body>
</html>
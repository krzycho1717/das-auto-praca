<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    public function average_per_moth(){
        $query = \DB::raw("SELECT (TO_CHAR(date, 'MM')) as month, AVG(charge) AS avg_charge FROM services WHERE user_id = ".$this->id." AND (TO_CHAR(date, 'YYYY')) = (TO_CHAR((current_date - INTERVAL '12 months'), 'YYYY')) GROUP BY (TO_CHAR(date, 'MM'))");
        return \DB::select($query->getValue());
    }

    public function statistics(){
        return \DB::table('users_stat')->where('email', $this->email)->first();
    }
    
    public function tokens(){
        return $this->hasMany('App\Models\Token', 'user_id');
    }

    public function actionLogs(){
        return $this->hasMany('App\Models\ActionLog', 'user_id');
    }
    
    public function clients(){
        return $this->hasMany('App\Models\Client', 'user_id');
    }

    public function services(){
        return $this->hasMany('App\Models\Service', 'user_id');
    }

    public function reviews(){
        return $this->hasMany('App\Models\Review', 'user_id');
    }

    public function fullname(){
        return $this->surname . " " . $this->name;
    }

    public function isActive(){
        return $this->active;
    }

    public function setSettings($key, $value){
        $s = json_decode($this->settings, true);
        if($value == 'true' || $value == 'false')
            $s[$key] = ($value == 'true')? true:false;
        else
            $s[$key] = $value;
        $this->settings = json_encode($s);
    }

    public function getSettings($key){
        $s = json_decode($this->settings, true);
        if(array_has($s, $key)){
            return $s[$key];
        }else{
            return null;
        }
    }

    public function isLogLimitAchieved(){

    }

    // default date format for created_at
    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    // default date format for updated_at
    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'locale', 'phone', 'email', 'password', 'settings', 'seen_at', 'visitor', 'verification_hash', 'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

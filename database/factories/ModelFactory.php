<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//  User
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'locale' => "pl",
        'email' => "krzycho1717@gmail.com",
        'password' => Hash::make("uzjel1919"),
        'remember_token' => str_random(10),
    ];
});
//  Service
$factory->define(App\Models\Service::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->sentences(3, true),
        'charge' => $faker->biasedNumberBetween(10, 1000)
    ];
});

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::post('api/v1/activate.{type?}', 'Api\UserController@activate')->where('type','xml|json');

Route::group(['prefix' => 'api/v1', 'middleware'=>'apiauth'], function () {
    Route::get('/', 'Api\UserController@api_handshake');
    // main
    Route::get('/search/{model}/{keyword}.{type?}', 'Api\UserController@search')->where('type','xml|json')->where('model', 'client|service|review')->name('search');
    Route::get('/wall.{type?}', 'Api\UserController@wall')->where('type','xml|json');
    Route::post('/deactivate.{type?}', 'Api\UserController@deactivate')->where('type','xml|json');
    // client
    Route::get('/client.{type}:{limit?}', 'Api\ClientController@index')->where('type','xml|json');
    Route::get('/client/{id}.{type?}', 'Api\ClientController@show')->where('type','xml|json');
    Route::post('/client.{type?}', 'Api\ClientController@store')->where('type','xml|json');
    Route::put('/client/{id}.{type?}', 'Api\ClientController@update')->where('type','xml|json');
    Route::delete('/client/{id}.{type?}', 'Api\ClientController@destroy')->where('type','xml|json');
    Route::get('/client/{id}/service.{type?}', 'Api\ClientController@show_services')->where('type','xml|json');
    Route::get('/client/{id}/review.{type?}', 'Api\ClientController@show_reviews')->where('type','xml|json');
    // service
    Route::get('/service.{type}:{limit?}', 'Api\ServiceController@index')->where('type','xml|json');
    Route::get('/service/{id}.{type?}', 'Api\ServiceController@show')->where('type','xml|json');
    Route::post('/service.{type?}', 'Api\ServiceController@store')->where('type','xml|json');
    Route::put('/service/{id}.{type?}', 'Api\ServiceController@update')->where('type','xml|json');
    Route::delete('/service/{id}.{type?}', 'Api\ServiceController@destroy')->where('type','xml|json');
    // review
    Route::get('/review.{type?}', 'Api\ReviewController@index')->where('type','xml|json');
    Route::get('/review/{id}.{type?}', 'Api\ReviewController@show')->where('type','xml|json');
    Route::post('/review.{type?}', 'Api\ReviewController@store')->where('type','xml|json');
    Route::put('/review/{id}.{type?}', 'Api\ReviewController@update')->where('type','xml|json');
    Route::delete('/review/{id}.{type?}', 'Api\ReviewController@destroy')->where('type','xml|json');
});

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'UserController@index');

    Route::get('/create', 'UserController@create');
    Route::post('/register', 'UserController@store');
    Route::post('/login', 'UserController@login');
    Route::get('/logout', 'UserController@logout');

    Route::get('/verify/{email}/{hash}', 'UserController@verify');
    Route::get('/verify/{email}/resend', 'UserController@verify_resend');

    Route::get('/password/reset/{email}/{hash}', 'UserController@password_reset');
    Route::post('/password/reset/{email}/{hash}', 'UserController@password_reset_action');

    Route::get('/change-locale/{locale}', function($locale){
        if(in_array($locale, \Config::get('app.available_locales'))){
            \Cookie::queue(Cookie::make('applocale', $locale, 2628000));
        }
        return Redirect::to('/');
    });

    Route::group(['prefix' => 'panel', 'middleware' => 'auth'], function () {

        Route::get('/search/{type}/{keyword}/', 'UserController@search')->where('type', 'client|service|review')->name('search');
        Route::get('/search/{type}/where/{attribute}/{keyword}', 'UserController@search_vs')->where('type', 'client|service|review')->name('search_v2');
        Route::get('/search/{type}/{keyword}/orderby/{attribute}/{order}', 'UserController@search_ordered')->where('type', 'client|service|review')->where('order', 'asc|desc')->name('search.order');
        Route::get('/', 'UserController@wall');

        ### client resources
        Route::resource('/client', 'ClientController');
        Route::get('/client/{id}/export/{type?}', 'ClientController@export')->where('type', 'xls|xlsx|csv');
        Route::get('/client/{id}/print/review/{review_id}', 'ReportController@print_review')->name('report.review');
        Route::get('/client/{id}/print/report/last_month/{extra?}', 'ReportController@last_month')->where('extra', 'with\/(service,review|service|review)')->name('report.last_month');
        Route::get('/client/{id}/print/report/last_week/{extra?}', 'ReportController@last_week')->where('extra', 'with\/(service,review|service|review)')->name('report.last_week');
        Route::get('/client/{id}/print/report/range/{from}/{to}/{extra?}', 'ReportController@range')->where('extra', 'with\/(service,review|service|review)')->name('report.range');

        Route::get('/client/orderby/{attribute}/{order}', 'ClientController@show_ordered')->where('order', 'asc|desc')->name('client.order');
        Route::resource('/client.service', 'ServiceController');
        Route::resource('/client.review', 'ReviewController');

        ### client resources
        Route::get('/service', 'UserController@show_services')->name('service.index');
        Route::get('/service/orderby/{attribute}/{order}', 'ServiceController@show_ordered')->where('order', 'asc|desc')->name('service.order');
        Route::post('/service', 'ServiceController@raw_create');
        Route::get('/service/raw_create', 'ServiceController@raw_create');
        Route::post('/service/raw_store', 'ServiceController@raw_store');

        ### client resources
        Route::get('/review', 'UserController@show_reviews')->name('review.index');
        Route::get('/review/orderby/{attribute}/{order}', 'ReviewController@show_ordered')->where('order', 'asc|desc')->name('review.order');
        Route::get('/review/raw_create', 'ReviewController@raw_create');
        Route::post('/review/raw_store', 'ReviewController@raw_store');

        ### settings
        Route::put('/settings', 'UserController@update');
        Route::delete('/settings', 'UserController@destroy');
        Route::get('/settings', 'UserController@show');
        Route::get('/settings/edit', 'UserController@edit');
        Route::get('/settings/change-locale/{locale}', 'UserController@change_locale');
        Route::post('/settings/update', 'UserController@setting');
        ### Action log
        Route::get('/log', 'ActionLogController@index');
        ### tokens
        Route::get('/settings/token', 'TokenController@index');
        Route::post('/settings/token', 'TokenController@store');
        Route::put('/settings/token', 'TokenController@update');
        Route::delete('/settings/token', 'TokenController@destroy');
    });

});

<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 24.03.2016
 * Time: 22:12
 */

namespace App\Helper;


class Log
{
    public static function d($var_name, $_var){
        echo '<pre class="xdebug-var-dump"> Variable <b>'.$var_name.'</b>: ';
        var_dump($_var);
        echo '</pre>';
    }
}
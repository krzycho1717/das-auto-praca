<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'address'   => 'Adres',
    'auto'      => 'Samochód',
    'charge'    => 'Opłata',
    'city'      => 'Miasto',
    'comment'   => 'Komentarz',
    'content'   => 'Treść',
    'country'   => 'Kraj',
    'date'      => 'Data',
    'created_at' => 'Utworzono',
    'performed' => 'Wykonano',
    'email'     => 'Email',
    'password'  => 'Hasło',
    'repeat_password'  => 'Powtórz hasło',
    'mileage'   => 'Przebieg',
    'name'      => 'Imię',
    'name_and_surname' => 'Imię i nazwisko',
    'surname_and_name' => 'Nazwisko i imię',
    'og1L'      => 'Ogumienie przód str. lewa',
    'og1P'      => 'Ogumienie przód str. prawa',
    'og2L'      => 'Ogumienie tył str. lewa',
    'og2P'      => 'Ogumienie tył str. prawa',
    'phone'     => 'Telefon',
    'plate'     => 'Tablica rejestracyjna',
    'surname'   => 'Nazwisko',
    'vin'       => 'VIN',
    'n1' => 'Wymiana oleju i filtra oleju',
    'n2' => 'Wymiana lub oczyszczanie filtra powietrza',
    'n3' => 'Wymiana lub oczyszczanie filtra przeciwpyłkowego',
    'n4' => 'Wymiana filtra paliwa lub odwodnienie',
    'n5' => 'Sprawdzenie stanu pasków wielorowkowych',
    'n6' => 'Sprawdzenie układu hamulcowego "przód" czyszczenie lub wymiana',
    'n7' => 'Sprawdzenie układu hamulcowego "tył" czyszczenie lub wymiana',
    'n8' => 'Sprawdzenie przewodów hamulcowych oraz paliwowych',
    'n9' => 'Sprawdzenie szczelności silnika oraz skrzyni biegów',
    'n10' => 'Sprawdzenie wycieraczek szyb, ustawienie dyszy spryskiwaczy',
    'n11' => 'Sprawdzenie skoku pedału sprzęgła',
    'n12' => 'Sprawdzenie zamków i smarowanie zawiasów drzwi ograniczników zawiasów klapy bagażnika',
    'n13' => 'Sprawdzenie poziomu oleju w układzie wspomagania kierownicy',
    'n14' => 'Sprawdzenie ustawienia świateł oraz uzupełnienie oświetlenia brakującego',
    'n15' => 'Sprawdzenie korygowanie poziomu cieczy chłodzącej - wymiana',
    'n16' => 'Sprawdzenie pompy wody',
    'n17' => 'Sprawdzenie zawieszenia przód(sworznie, drążki reakcyjne, połączenia spawane i śrubowe)',
    'n18' => 'Wymiana baterii pilota zdalnego sterowania',
    'n19' => 'Sprawdzenie zawieszenia tyłu',
    'n20' => 'Sprawdzenie stanu ogumienia oraz ciśnienia powietrza',
    'n21' => 'Wymiana świec zapłonowych',
    'n22' => 'Sprawdzenie układu kierowniczego',
    'n23' => 'Sprawdzenie pracy silnika',
    'n24' => 'Sprawdzenie stanu osłon gumowych na przegubach',
    'n25' => 'Sprawdzenie szczelności układu wydechowego',
    'n26' => 'Sprawdzenie układu ładowania akumulatora',
    'n27' => 'Sprawdzenie oleju w skrzyni biegów lub wymiana'
];

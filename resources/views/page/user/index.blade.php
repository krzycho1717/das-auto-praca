@extends('layouts.user')
@section('content')


<div class="login-panel panel panel-default plain animated bounceIn">
    <!-- Start .panel -->
    <div class="panel-heading">
        <h4 class="panel-title text-center">
            <img id="logo" src="/images/logo2.png" alt="DasAuto logo">
        </h4>
        <div id="lang-panel-shadow"></div>
        <div id="lang-panel">
            <div><a href="{{ URL::to('/change-locale/pl') }}"><img src="/images/pl.png" width="24"/></a></div>
            <div><a href="{{ URL::to('/change-locale/en') }}"><img src="/images/gb.png" width="24"/></a></div>
        </div>
    </div>
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="bs-callout bs-callout-danger">
                <i class="mdi mdi-alert-outline"></i>
                <strong>Oh snap!</strong>
                <ul style="list-style: none">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('message'))
            <div class="bs-callout bs-callout-danger">
                <i class="mdi mdi-alert-outline"></i>
                <strong>Oh snap!</strong>
                {{ session('message') }}
            </div>
        @endif
        <form class="form-horizontal mt0" action="{{ URL::to('/login') }}" method="POST" id="login-form" role="form" novalidate="novalidate" autocomplete="off">
            {!! csrf_field() !!}
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="input-group input-icon">
                        <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                        <input type="text" name="email" id="email" class="form-control" placeholder="{{ Lang::get('auth.placeholder_email') }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="input-group input-icon">
                        <span class="input-group-addon"><i class="mdi mdi-lock"></i></span>
                        <input type="password" name="password" id="password" class="form-control" placeholder="{{ Lang::get('auth.placeholder_password') }}">
                    </div>
                    {{--<span class="help-block text-right"><a class="btn btn-link" data-toggle="modal" data-target="#reset-password-modal">{{ Lang::get('auth.forgot_password') }}</a></span>--}}
                </div>
            </div>
            <div class="form-group mb0 mt20">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                    <div>
                        <input class="mb5" type="checkbox" name="remember" id="remember" style="vertical-align: middle">
                        <label for="remember">{{ Lang::get('auth.remember_me') }}</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                    <button class="btn btn-default pull-right" type="submit">{{ Lang::get('auth.sign_in') }}</button>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-footer gray-lighter-bg bt">
        <h4 class="text-center"><strong>{{ Lang::get('auth.dont_have_account') }}</strong>
        </h4>
        <p class="text-center">
            <a href="/create" class="btn btn-primary">
                <i class="mdi mdi-apple-finder"></i>
                {{ Lang::get('auth.create_account') }}
            </a>
        </p>
    </div>
</div>
<!-- End .panel -->
<div class="container pt10">
    <div class="footer">
        <p class="text-center">©{{ \Carbon\Carbon::now()->year }} Copyright Krzysztof Stec. All right reserved</p>
    </div>
</div>

@stop
<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 30.03.2016
 * Time: 17:32
 */

namespace App\Helper;

/**
 * Class Button
 * Helper method for creating buttons with macro 'form_generator.macro.php'
 * @package App\Helper
 */
class Button{
    public function __construct($value, $type='submit', $id=null, $class='btn btn-success mr5 mb10 pull-right')
    {
        $this->value = $value;
        $this->type = $type;
        $this->id = $id;
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }
    protected $value;
    protected $type;
    protected $class;
    protected $id;
}
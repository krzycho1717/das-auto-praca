@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('review.Review'), $errors, session('message'), true, [
            [
                'name' => Lang::get('ui.edit'),
                'url' => URL::action('ReviewController@edit', [$review->client, $review->id])
            ],
            [
                'name'  => Lang::get('ui.show_empty_fields'),
                'url'   => '#',
                'class' => 'show-empty-fields',
                'style' => 'display: none',
                'sep_style' => 'display: none'
            ],
            [
                'name'  => Lang::get('ui.hide_empty_fields'),
                'url'   => '#',
                'class' => 'hide-empty-fields'
            ],
            [
                'name' => Lang::get('ui.print'),
                'url' => URL::action('ReportController@print_review', [$review->client->id, $review->id])
            ]
         ] ) }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('client_id', Lang::get('client.Client')) }}
                    {{ Form::text('client_id', $review->client->fingerprint(), ['class' => 'form-control input-link', 'readonly' => 'readonly', 'data-link' => URL::action('ClientController@show', $review->client->id) ]) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('plate', Lang::get('table.plate')) }}
                    {{ Form::text('plate', $review->reg, ['class' => 'form-control', 'placeholder' => Lang::get('table.plate'), 'readonly' => 'readonly']) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('vin', Lang::get('table.vin')) }}
                    {{ Form::text('vin', $review->vin, ['class' => 'form-control', 'placeholder' => Lang::get('table.vin'), 'readonly' => 'readonly', 'data-link' => 'https://www.autodna.com/vin/' . $review->vin ]) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('mileage', Lang::get('table.mileage')) }}
                    <div class="input-group">
                        {{ Form::number('mileage', $review->mileage, ['min' => 0, 'step' => 0, 'class' => 'form-control', 'placeholder' => Lang::get('table.mileage'), 'readonly' => 'readonly']) }}
                        <span class="input-group-addon">km</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('date', Lang::get('table.performed')) }}
                    {{ Form::date('date', $review->date ? $review->date : Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'readonly' => 'readonly']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('car', Lang::get('table.auto')) }}
                    {{ Form::text('car', $review->car, ['class' => 'form-control', 'placeholder' => Lang::get('table.auto'), 'readonly' => 'readonly']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                    {{ Form::label('comment', Lang::get('table.comment')) }}
                    {{ Form::textarea('comment', $review->comment, ['class' => 'form-control', 'placeholder' => Lang::get('table.comment'), 'readonly' => 'readonly']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('og1L', Lang::get('table.og1L')) }}
                    <div class="input-group">
                        {{ Form::number('og1L', $review->tiresFrontLeft, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og1L'), 'readonly' => 'readonly']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('og1P', Lang::get('table.og1P')) }}
                    <div class="input-group">
                        {{ Form::number('og1P', $review->tiresFrontRight, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og1P'), 'readonly' => 'readonly']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('og2L', Lang::get('table.og2L')) }}
                    <div class="input-group">
                        {{ Form::number('og2L', $review->tiresBackLeft, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og2L'), 'readonly' => 'readonly']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('og2P', Lang::get('table.og2P')) }}
                    <div class="input-group">
                        {{ Form::number('og2P', $review->tiresBackRight, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og2P'), 'readonly' => 'readonly']) }}
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
        </div>
        @for($i = 1; $i <= 27; $i++)
            @php($in = 'n'.$i)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label($in, Lang::get('table.'.$in)) }}
                        {{ Form::text($in, $review->$in, ['class' => 'form-control', 'placeholder' => Lang::get('table.'.$in), 'readonly' => 'readonly']) }}
                    </div>
                </div>
            </div>
        @endfor
        {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>
    <script src="/javascript/jquery.inputmask.bundle.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="comment"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            },
            readonly : 1
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[name="vin"]').inputmask("*{17}");
            $('.hide-empty-fields').click(function () {
                $(this).hide();
                $(this).next().hide();
                $('.show-empty-fields').show();
                $('.show-empty-fields').next().show();

                var inputs = $('.panel').find('.form-control');
                $.each($(inputs), function (index, el) {
                    if ($.trim($(el).val()) == '') {
                        $(el).parent('.form-group').hide();
                    }
                });
            });
            $('.show-empty-fields').click(function () {
                $(this).hide();
                $(this).next().hide();
                $('.hide-empty-fields').show();
                $('.hide-empty-fields').next().show();
                var inputs = $('.panel').find('.form-control:hidden');
                $.each($(inputs), function (index, el) {
                    $(el).parent('.form-group').show();
                });
            });
        });
    </script>

@stop
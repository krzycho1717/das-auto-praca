<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\LoginEventListener@handle',
        ],

        // 'App\Events\ReadModel' => [
        //     'App\Listeners\ServiceEventListener@handleOnRead',
        // ],

        // 'eloquent.saved: App\Models\Service' => [
        //     'App\Listeners\ServiceEventListener@handleOnCreated',
        // ],
        // 'eloquent.updated: App\Models\Service' => [
        //     'App\Listeners\ServiceEventListener@handleOnUpdated',
        // ],
        // 'eloquent.deleted: App\Models\Service' => [
        //     'App\Listeners\ServiceEventListener@handleOnDeleted',
        // ],

        // 'eloquent.saved: App\Models\Client' => [
        //     'App\Listeners\ClientEventListener@handleOnCreated',
        // ],
        // 'eloquent.updated: App\Models\Client' => [
        //     'App\Listeners\ClientEventListener@handleOnUpdated',
        // ],
        // 'eloquent.deleted: App\Models\Client' => [
        //     'App\Listeners\ClientEventListener@handleOnDeleted',
        // ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}

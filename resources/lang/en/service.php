<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Service Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'Service' => 'Service',
    'service' => 'service',
    'Services' => 'Services',
    'services' => 'services',
    'gen' => 'services',
    'add' => 'add service',
    'latest' => 'Services - recently added',
    'edit_title' => 'Service - edit',
    'create_title' => 'New service',
    'empty' => 'no services'

];

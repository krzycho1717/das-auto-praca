@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('review.edit_title'), $errors, session('message')) }}
        {{ Form::open(['action' => ['ReviewController@update', $client, $review->id], 'method' => 'PUT']) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('reg', Lang::get('table.plate')) }}
                        {{ Form::text('reg', $review->reg, ['class' => 'form-control', 'placeholder' => Lang::get('table.plate'), 'required' => 'required']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('vin', Lang::get('table.vin')) }}
                        {{ Form::text('vin', $review->vin, ['class' => 'form-control', 'placeholder' => Lang::get('table.vin'), 'required' => 'required']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('mileage', Lang::get('table.mileage')) }}
                        <div class="input-group">
                            {{ Form::number('mileage', $review->mileage, ['min' => 0, 'step' => 0, 'class' => 'form-control', 'placeholder' => Lang::get('table.mileage'), 'required' => 'required']) }}
                            <span class="input-group-addon">km</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('date', Lang::get('table.performed')) }}
                        {{ Form::date('date', $review->date ? $review->date : Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'required' => 'required']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('car', Lang::get('table.auto')) }}
                        {{ Form::text('car', $review->car, ['class' => 'form-control', 'placeholder' => Lang::get('table.auto'), 'required' => 'required']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                        {{ Form::label('comment', Lang::get('table.comment')) }}
                        {{ Form::textarea('comment', $review->comment, ['class' => 'form-control', 'placeholder' => Lang::get('table.comment'), 'required' => 'required', 'novalidate' => 'novalidate']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('og1L', Lang::get('table.og1L')) }}
                        <div class="input-group">
                            {{ Form::number('og1L', $review->tiresFrontLeft, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og1L'), 'required' => 'required']) }}
                            <span class="input-group-addon">cm</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('og1P', Lang::get('table.og1P')) }}
                        <div class="input-group">
                            {{ Form::number('og1P', $review->tiresFrontRight, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og1P'), 'required' => 'required']) }}
                            <span class="input-group-addon">cm</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('og2L', Lang::get('table.og2L')) }}
                        <div class="input-group">
                            {{ Form::number('og2L', $review->tiresBackLeft, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og2L'), 'required' => 'required']) }}
                            <span class="input-group-addon">cm</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('og2P', Lang::get('table.og2P')) }}
                        <div class="input-group">
                            {{ Form::number('og2P', $review->tiresBackRight, ['step' => '0.01','min' => '0.00', 'class' => 'form-control', 'placeholder' => Lang::get('table.og2P'), 'required' => 'required']) }}
                            <span class="input-group-addon">cm</span>
                        </div>
                    </div>
                </div>
            </div>
            @for($i = 1; $i <= 27; $i++)
                @php($in = 'n'.$i)
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label($in, Lang::get('table.'.$in)) }}
                            {{ Form::text($in, $review->$in, ['class' => 'form-control', 'placeholder' => Lang::get('table.'.$in)]) }}
                        </div>
                    </div>
                </div>
            @endfor
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(Lang::get('ui.save'), ['class' => 'btn btn-success pull-right']) }}
                </div>
            </div>
        {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>
    <script src="/javascript/jquery.inputmask.bundle.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="comment"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[name="vin"]').inputmask("*{17}");
        });
    </script>

@stop
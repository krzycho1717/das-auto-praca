<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'Client' => 'Client',
    'client' => 'client',
    'Clients' => 'Clients',
    'clients' => 'clients',
    'gen' => 'clients',
    'add' => 'add client',
    'latest' => 'Clients - recently added',
    'edit_title' => 'Client - edit',
    'create_title' => 'New client',
    'empty' => 'no clients',
    'select' => 'select client',
    'report' => 'Client report'

];

<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User();

        $user->name = "Krzysztof";
        $user->surname = "Stec";
        $user->email = "krzycho1717@gmail.com";
        $user->phone = "668977465";
        $user->password = Hash::make("uzjel1919");

        $user->save();
    }
}

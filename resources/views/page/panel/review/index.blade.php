@extends('layouts.default')
@section('content')

<div class="modal fade modal-danger" id="delete-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- Start .panel -->
            <div class="modal-header">
                <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
            </div>
            <div class="modal-body">
                {{ Lang::get('ui.confirm_delete.message') }}
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default delete-cancel inline" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.cancel') }}</a>
                <form action="" method="post" id="delete-form" class="inline">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-primary" data-bb-handler="confirm" type="submit">{{ Lang::get('ui.ok') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default toggle panelMove panelClose panelRefresh">
    <!-- Start .panel -->
    <div class="panel-heading">
        <h4 class="panel-title inline mr10"><i class="icon-user-tie"></i>{{ Lang::get('review.Reviews') }}</h4>
        @if(Request::route()->getName() == 'panel.client.review.index')
            <a href="{{ URL::action('ClientController@show', [ 'id' => $client->id ]) }}"><span class="label label-info">{{ $client->fullname() }}</span></a>
        @elseif(Request::route()->getName() == 'review.order')
            <div class="btn-group inline">
                <a href="{{ URL::route('review.order', ['order' => Route::Input('order') == 'asc' ? 'desc':'asc', 'attribute' => Route::Input('attribute')]) }}" class="btn btn-xs btn-success">
                    <span>{{ Lang::get('table.'.Route::Input('attribute')) }}</span>
                    @if (Route::Input('order') == 'desc')
                        <i class="mdi mdi-chevron-down m0"></i>
                    @else
                        <i class="mdi mdi-chevron-up m0"></i>
                    @endif
                </a>
                <a href="{{ URL::route('review.index') }}" class="btn btn-xs btn-danger">
                    <i class="mdi mdi-close p0 m0"></i>
                </a>
            </div>
        @endif
    </div>
    @if(!CheckVar::c_array($reviews))
        <div class="panel-body pl0 pr0">
            <div class="bs-callout bs-callout-info">
                <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('review.empty') }}</h4>
            </div>
        </div>
        <div class="panel-footer clearfix">
            @if(Session::get('clients_number') == 0)
                <div class="btn btn-primary pull-right" disabled>{{ Lang::get('ui.add') }}</div>
            @else
                @if(isset($client))
                    <a href="{{ URL::action('ReviewController@create', $client) }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                @else
                    <a href="{{ URL::action('ReviewController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
                @endif
            @endif
        </div>
    @else
        <div class="panel-body pl0 pr0">
        @include('page.panel.review.table')
        {!! $reviews->render() !!}
        </div>
        <div class="panel-footer clearfix">
            @if(isset($client))
                <a href="{{ URL::action('ReviewController@create', $client) }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
            @else
                <a href="{{ URL::action('ReviewController@raw_create') }}" class="btn btn-primary pull-right">{{ Lang::get('ui.add') }}</a>
            @endif
        </div>
    @endif
</div>

@stop
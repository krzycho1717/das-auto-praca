<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Review Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'Review' => 'Przegląd',
    'review' => 'przegląd',
    'Reviews' => 'Przeglądy',
    'reviews' => 'przeglądy',
    'gen' => 'przeglądów',
    'add' => 'dodaj przegląd',
    'latest' => 'Przeglądy - ostatnio dodane',
    'edit_title' => 'Przegląd - edycja',
    'create_title' => 'Nowy przegląd',
    'empty' => 'brak przeglądów',
    'next_expected' => 'Następny przewidywany przegląd'

];

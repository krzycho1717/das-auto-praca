$(document).ready(function() {

    console.info('user script start');

    $('#change-language > ul > li > a').click(function(event){
        event.preventDefault();
        console.info('click test');
        var locale = $(this).parent().data('locale');

        $('#locale').val(locale);

        if(locale == 'pl'){
            $('span.language-english').hide();
            $('span.language-polish').show();
        }else{
            $('span.language-polish').hide();
            $('span.language-english').show();
        }
    });

});
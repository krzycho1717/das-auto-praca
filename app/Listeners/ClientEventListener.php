<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 10.08.2016
 * Time: 17:57
 */

namespace App\Listeners;

use App\Events\ReadModel;
use App\Models\ActionLog;
use App\Models\Client;
use Auth;
use Session;

class ClientEventListener
{
    /**
     * ClientEventListener constructor.
     */
    public function __construct()
    {
    }

    public function handleOnCreated(Client $client){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "CREATE";
        $log->affected = "clients";
        $log->extra = json_encode(array(
            'id' => $client->id
        ));
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();

        Session::set('clients_number', Auth::user()->statistics()->clients_count);
    }

    public function handleOnRead(ReadModel $readModel){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "READ";
        $log->affected = "clients";
        $log->extra = json_encode(array(
            'id' => $readModel->model->id
        ));
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
    }

    public function handleOnUpdated(Client $client){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "UPDATE";
        $log->affected = "clients";
        $log->extra = json_encode(array(
            'id' => $client->id
        ));
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();
    }

    public function handleOnDeleted(Client $client){
        ActionLog::pop();

        $log = new ActionLog();
        $log->user_id = Auth::user()->id;
        $log->type = "DELETE";
        $log->affected = "clients";
        $log->extra = $client->toJson();
        $log->from_ip = $_SERVER['REMOTE_ADDR'];
        $log->save();

        Session::set('clients_number', Auth::user()->statistics()->clients_count);
    }
}

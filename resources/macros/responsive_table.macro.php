<?php

Html::macro('responsive_table', function($headers, $data, $attrs){
    $def_attrs = array(
        'column_class' => '',
        'table_class' => ''
    );
    $attrs = array_merge($def_attrs, $attrs);
    $col_num = 12 / count($headers);

    $html = '';

    foreach ($data as $item){
        $html .= '<div class="row">';
        foreach ($headers as $h){
            $html .= '<div class="col-md-'.$col_num.'">';
            $html .= $item->$h;
            $html .= '</div>';
        }
        $html .= '</div>';
    }

    $html .= '</ul>';

    return new \Illuminate\Support\HtmlString($html);

});

?>

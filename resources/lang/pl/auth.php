<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'already_have_account'          => 'Już masz konto ?',
    'create_account'                => 'Utwórz',
    'dont_have_account'             => 'Nie masz konta ?',
    'failed_msg'                    => 'E-mail lub hasło są nieprawidłowe. Upewnij się, że wpisujesz poprawne dane.',
    'forgot_password'               => 'Nie pamiętasz hasła ?',
    'placeholder_email'             => 'Twój email',
    'placeholder_name'              => 'Twoje imię',
    'placeholder_open_file'         => 'Otwórz',
    'placeholder_password'          => 'Twoje hasło',
    'placeholder_phone'             => 'Twój numer telefonu',
    'placeholder_photo'             => 'Twoje zdjęcie (opcjonalne)',
    'placeholder_repeat_password'   => 'Powtórz hasło',
    'placeholder_surname'           => 'Twoje nazwisko',
    'remember_me'                   => 'zapamiętaj ?',
    'sign_in'                       => 'Zaloguj',
    'sign_out'                      => 'Wyloguj',
];

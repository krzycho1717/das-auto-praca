<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 30.03.2016
 * Time: 13:38
 */

namespace App\Helper;


class CheckVar
{
    public static function c_array(&$v){
        if(isset($v) && $v != null && !$v->isEmpty() ){
            return true;
        }
        return false;
    }
}
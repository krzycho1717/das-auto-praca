<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use \App\Models\Review;
use App;
use Auth;
use JsonPackage;
use Validator;
use Input;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function index($type="json")
    {
        return JsonPackage::prepare($type, Auth::user()->reviews()->with('client')->get()->toArray(), 'error');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  string $type
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function store($type="json")
    {
        $rules = array(
            'client_id' => 'required|exists:clients,id',
            'vin'       => 'required|alpha_num|size:17',
            'plate'     => 'required',
            'mileage'   => 'required|numeric|min:0',
            'comment'   => 'required',
            'car'       => 'required',
            'date'      => 'required|date',
            'og1L'      => 'required|numeric',
            'og1P'      => 'required|numeric',
            'og2L'      => 'required|numeric',
            'og2P'      => 'required|numeric',
            'n1'        => 'string',
            'n2'        => 'string',
            'n3'        => 'string',
            'n4'        => 'string',
            'n5'        => 'string',
            'n6'        => 'string',
            'n7'        => 'string',
            'n8'        => 'string',
            'n9'        => 'string',
            'n10'       => 'string',
            'n11'       => 'string',
            'n12'       => 'string',
            'n13'       => 'string',
            'n14'       => 'string',
            'n15'       => 'string',
            'n16'       => 'string',
            'n17'       => 'string',
            'n18'       => 'string',
            'n19'       => 'string',
            'n20'       => 'string',
            'n21'       => 'string',
            'n22'       => 'string',
            'n23'       => 'string',
            'n24'       => 'string',
            'n25'       => 'string',
            'n26'       => 'string',
            'n27'       => 'string'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {
            $review = new Review();
            $review->user_id = Auth::user()->id;
            $review->client_id = Input::get('client_id');
            $review->vin = Input::get('vin');
            $review->reg = Input::get('plate');
            $review->car = Input::get('car');
            $review->mileage = Input::get('mileage');
            $review->comment = Input::get('comment');
            $review->date = Input::get('date');
            $review->tiresFrontLeft = floatval(Input::get('og1L'));
            $review->tiresFrontRight = floatval(Input::get('og1P'));
            $review->tiresBackLeft = floatval(Input::get('og2L'));
            $review->tiresBackRight = floatval(Input::get('og2P'));
            $review->n1 = Input::get('n1');
            $review->n2 = Input::get('n2');
            $review->n3 = Input::get('n3');
            $review->n4 = Input::get('n4');
            $review->n5 = Input::get('n5');
            $review->n6 = Input::get('n6');
            $review->n7 = Input::get('n7');
            $review->n8 = Input::get('n8');
            $review->n9 = Input::get('n9');
            $review->n10 = Input::get('n10');
            $review->n11 = Input::get('n11');
            $review->n12 = Input::get('n12');
            $review->n13 = Input::get('n13');
            $review->n14 = Input::get('n14');
            $review->n15 = Input::get('n15');
            $review->n16 = Input::get('n16');
            $review->n17 = Input::get('n17');
            $review->n18 = Input::get('n18');
            $review->n19 = Input::get('n19');
            $review->n20 = Input::get('n20');
            $review->n21 = Input::get('n21');
            $review->n22 = Input::get('n22');
            $review->n23 = Input::get('n23');
            $review->n24 = Input::get('n24');
            $review->n25 = Input::get('n25');
            $review->n26 = Input::get('n26');
            $review->n27 = Input::get('n27');
            $review->save();

            return JsonPackage::prepare($type, null, null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type='json')
    {
        $result = Auth::user()->reviews()->with('client')->find($id);

        if($result == null){
            return JsonPackage::prepare($type, $result, null, 404);
        }
        return JsonPackage::prepare($type, $result, null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function update($id, $type="json")
    {
        $result = Auth::user()->reviews()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, $result, null, 404);
        }

        $rules = array(
            'client_id' => 'required|exists:clients,id',
            'vin'       => 'required|alpha_num|size:17',
            'plate'     => 'required',
            'mileage'   => 'required|numeric|min:0',
            'comment'   => 'required',
            'car'       => 'required',
            'date'      => 'required|date',
            'og1L'      => 'required|numeric',
            'og1P'      => 'required|numeric',
            'og2L'      => 'required|numeric',
            'og2P'      => 'required|numeric',
            'n1'        => 'string',
            'n2'        => 'string',
            'n3'        => 'string',
            'n4'        => 'string',
            'n5'        => 'string',
            'n6'        => 'string',
            'n7'        => 'string',
            'n8'        => 'string',
            'n9'        => 'string',
            'n10'       => 'string',
            'n11'       => 'string',
            'n12'       => 'string',
            'n13'       => 'string',
            'n14'       => 'string',
            'n15'       => 'string',
            'n16'       => 'string',
            'n17'       => 'string',
            'n18'       => 'string',
            'n19'       => 'string',
            'n20'       => 'string',
            'n21'       => 'string',
            'n22'       => 'string',
            'n23'       => 'string',
            'n24'       => 'string',
            'n25'       => 'string',
            'n26'       => 'string',
            'n27'       => 'string'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare($type, null, $validator->messages());
        } else {

            $userdata = array(
                'client_id' => Input::get('client_id'),
                'vin' => Input::get('vin'),
                'plate' => Input::get('plate'),
                'mileage' => Input::get('mileage'),
                'comment' => Input::get('comment'),
                'car' => Input::get('car'),
                'date' => Input::get('date'),
                'tiresFrontLeft' => floatval(Input::get('og1L')),
                'tiresFrontRight' => floatval(Input::get('og1P')),
                'tiresBackLeft' => floatval(Input::get('og2L')),
                'tiresBackRight' => floatval(Input::get('og2P')),
                'n1' => Input::get('n1'),
                'n2' => Input::get('n2'),
                'n3' => Input::get('n3'),
                'n4' => Input::get('n4'),
                'n5' => Input::get('n5'),
                'n6' => Input::get('n6'),
                'n7' => Input::get('n7'),
                'n8' => Input::get('n8'),
                'n9' => Input::get('n9'),
                'n10' => Input::get('n10'),
                'n11' => Input::get('n11'),
                'n12' => Input::get('n12'),
                'n13' => Input::get('n13'),
                'n14' => Input::get('n14'),
                'n15' => Input::get('n15'),
                'n16' => Input::get('n16'),
                'n17' => Input::get('n17'),
                'n18' => Input::get('n18'),
                'n19' => Input::get('n19'),
                'n20' => Input::get('n20'),
                'n21' => Input::get('n21'),
                'n22' => Input::get('n22'),
                'n23' => Input::get('n23'),
                'n24' => Input::get('n24'),
                'n25' => Input::get('n25'),
                'n26' => Input::get('n26'),
                'n27' => Input::get('n27')
            );

            $result->update($userdata);
            return JsonPackage::prepare($type, null, null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $type="json")
    {
        $result = Auth::user()->reviews()->find($id);

        if($result == null){
            return JsonPackage::prepare($type, null, null, 404);
        }

        $result->delete();
        return JsonPackage::prepare($type, null, null);
    }
}

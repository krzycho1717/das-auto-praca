<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('unique_device_id', function($attribute, $value)
        {
            return preg_match('/^[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}$/u', $value);
        });

        Validator::extend('token', function($attribute, $value)
        {
            return preg_match('/^[A-Z\d]{32}$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

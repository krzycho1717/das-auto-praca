<?php

namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {

    // TODO create client uniqe id for report
    protected $table = 'clients';
    protected $fillable = ['name', 'surname', 'phone', 'email', 'country', 'city', 'address', 'date'];
    protected $dates = ['created_at'];

    protected static $allowed_to_order = ['name', 'surname', 'phone', 'email', 'country', 'city', 'address', 'created_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function fullname(){
        return $this->surname . " " . $this->name;
    }

    public function fingerprint(){
        return $this->name.' '.$this->surname.', '.$this->address.', '.$this->city.', '.$this->country;
    }

    public function services(){
        return $this->hasMany('App\Models\Service', 'client_id');
    }

    public function reviews(){
        return $this->hasMany('App\Models\Review', 'client_id');
    }

    public function nextExpectedReview()
    {
        $next_review = $this->reviews()->orderBy('date', 'desc')->first();
        if ($next_review == null) return null;
        return self::getDateAttribute($next_review->date);
    }

    public static function canOrderBy($attribute){
        return in_array($attribute, self::$allowed_to_order);
    }

    public function getDateAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $date);
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    // default date format for updated_at
    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}

?>
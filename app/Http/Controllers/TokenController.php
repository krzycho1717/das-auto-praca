<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 03.07.2016
 * Time: 12:47
 */

namespace App\Http\Controllers;

use App\Models\Token;
use Auth;
use View;
use TokenGenerator;
use JsonPackage;
use Validator;
use Input;

use Illuminate\Http\Request;


class TokenController extends Controller
{
    public function index()
    {
        $tokens = Auth::user()->tokens()->orderBy('created_at', 'asc')->get();
        $data = array(
            'html' => View::make('page.panel.settings.devices', array('tokens' => $tokens))->render()
        );
        return JsonPackage::prepare('json', $data, null);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|min:4|unique:tokens'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare('json', null, $validator->messages());
        } else {
            $token = new Token();
            $token->user_id = Auth::user()->id;
            $token->name = Input::get('name');
            $token->token = TokenGenerator::make();
            $token->udid = null;

            $token->save();

            $tokens = Auth::user()->tokens()->orderBy('created_at', 'asc')->get();

            $data = array(
                'html' => View::make('page.panel.settings.devices', array('tokens' => $tokens))->render()
            );

            return JsonPackage::prepare('json', $data, null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'type' => 'required|in:RENAME,REGEN,RESET',
            'id' => 'required|exists:tokens,id'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare('json', null, $validator->messages());
        } else {
            $type = Input::get('type');

            switch ($type){
                case 'RENAME':
                    $rules['name'] = 'required|min:4|unique:tokens';

                    $validator = Validator::make(Input::all(), $rules);

                    if ($validator->fails()) {
                        return JsonPackage::prepare('json', null, $validator->messages());
                    } else {
                        $token = Auth::user()->tokens()->find(Input::get('id'));
                        if ($token != null) {
                            $token->name = Input::get('name');
                            $token->save();

                            $tokens = Auth::user()->tokens()->orderBy('created_at', 'asc')->get();

                            $data = array(
                                'html' => View::make('page.panel.settings.devices', array('tokens' => $tokens))->render()
                            );

                            return JsonPackage::prepare('json', $data, null);
                        } else {
                            return JsonPackage::prepare('json', null, 'No token with given ID');
                        }
                    }
                    break;
                case 'REGEN':
                    // re-generate token + reset udid field
                    $token = Auth::user()->tokens()->find(Input::get('id'));
                    if($token != null){
                        $token->udid = null;
                        $token->token = TokenGenerator::make();
                        $token->save();

                        $tokens = Auth::user()->tokens()->orderBy('created_at', 'asc')->get();

                        $data = array(
                            'html' => View::make('page.panel.settings.devices', array('tokens' => $tokens))->render()
                        );

                        return JsonPackage::prepare('json', $data, null);
                    }else{
                        return JsonPackage::prepare('json', null, 'No token with given ID');
                    }
                    break;
                case 'RESET':
                    // reset udid field
                    $token = Auth::user()->tokens()->find(Input::get('id'));
                    if($token != null){
                        $token->udid = null;
                        $token->save();

                        $tokens = Auth::user()->tokens()->orderBy('created_at', 'asc')->get();

                        $data = array(
                            'html' => View::make('page.panel.settings.devices', array('tokens' => $tokens))->render()
                        );

                        return JsonPackage::prepare('json', $data, null);
                    }else{
                        return JsonPackage::prepare('json', null, 'No token with given ID');
                    }
                    break;
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $rules = array(
            'id' => 'required|exists:tokens,id'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return JsonPackage::prepare('json', null, $validator->messages());
        } else {
            $token = Auth::user()->tokens()->find(Input::get('id'));
            if($token != null){
                $token->delete();

                $tokens = Auth::user()->tokens()->get();

                $data = array(
                    'html' => View::make('page.panel.settings.devices', array('tokens' => $tokens))->render()
                );

                return JsonPackage::prepare('json', $data, null);
            }else{
                return JsonPackage::prepare('json', null, 'No token with given ID');
            }
        }
    }
}
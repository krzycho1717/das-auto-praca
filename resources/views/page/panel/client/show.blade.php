@extends('layouts.default')
@section('content')

    <div class="modal fade modal-danger" id="delete-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ Lang::get('ui.confirm_delete.title') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ Lang::get('ui.confirm_delete.message') }}</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default delete-cancel inline" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.cancel') }}</a>
                    {{--<a href="#" class="btn btn-primary delete-confirm" id="delete-client-index-confirm" data-bb-handler="confirm">{{ Lang::get('ui.ok') }}</a>--}}
                    {{ Form::open(['method' => 'DELETE', 'action' => ['ClientController@destroy', $client->id], 'class' => 'inline' ]) }}
                    {{ Form::button(Lang::get('ui.ok'), ['class' => 'btn btn-primary', 'type' => 'submit', 'data-bb-handler' => 'confirm']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-primary" id="report-select-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ Lang::get('ui.select_range') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="input-daterange input-group" id="datepicker">
                        <span class="input-group-addon">{{ Lang::get('ui.from') }}</span>
                        <input data-date-format="YYYY-MM-DD" type="text" class="input-sm form-control" name="from" placeholder="{{ Lang::get('ui.from') }}"/>
                        <span class="input-group-addon">{{ Lang::get('ui.to') }}</span>
                        <input data-date-format="YYYY-MM-DD" type="text" class="input-sm form-control" name="to" placeholder="{{ Lang::get('ui.to') }}"/>
                        <span class="input-group-addon"></span>
                        <select name="extra" class="input-sm form-control">
                            <option value="0">{{ Lang::get('ui.generate_report.with_all') }}</option>
                            <option value="1">{{ Lang::get('ui.generate_report.with_services') }}</option>
                            <option value="2">{{ Lang::get('ui.generate_report.with_reviews') }}</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="generate-report" target="_blank" class="btn btn-primary" data-bb-handler="cancel" data-dismiss="modal">{{ Lang::get('ui.generate_report.label') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 column">
        <div class="panel panel-default plain">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">
                    {{ Lang::get('client.Client') }}
                    <a href="{{ URL::to('/') }}" class="header-link pull-right">{{ Lang::get('ui.return') }}</a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="row profile">
                    <!-- Start .row -->
                    <div class="col-md-5 mt20">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 text-center" style="height: 140px;">
                                <div class="picture128 mb10 text-center">
                                    <img src="/images/default.png" alt="Avatar" class="picture128" />
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6" style="height: 140px">
                                <dt>{{ Lang::get('table.created_at') }}</dt>
                                <dd class="text-muted">{{ $client->created_at }}</dd>
                                <dt>{{ Lang::get('service.Services') }}</dt>
                                <dd class="text-muted text-capitalize">{{ $s_count }}</dd>
                                <dt>{{ Lang::get('review.Reviews') }}</dt>
                                <dd class="text-muted text-capitalize">{{ $r_count }}</dd>
                            </div>
                        </div>
                        <div class="mb20 text-center" style="display: inline-block;width: 100%">
                            <div class="row">
                                <div class="col-md-6 col-xs-6 text-right">
                                    <a id="edit-action" href="{{ URL::action('ClientController@edit', $client->id) }}" class="btn btn-primary btn-large mt10 mb10" style="width: 160px"><i class="mdi mdi-pen"></i>{{ Lang::get('ui.edit') }}</a>
                                </div>
                                <div class="col-md-6 col-xs-6 text-left">
                                    <a id="delete-action" data-toggle="modal" data-target="#delete-modal" class="btn btn-danger btn-large mt10 mb10" style="width: 160px"><i class="mdi mdi-delete"></i>{{ Lang::get('ui.delete') }}</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6 text-right">
                                    <div class="btn-group">
                                        <a href="{{ URL::action('ServiceController@index', $client->id) }}" class="btn btn-default btn-large mt10 mb10" style="width: 133px"><i class="mdi mdi-clipboard-outline"></i>{{ Lang::get('service.services') }}</a>
                                        <a href="{{ URL::action('ServiceController@create', $client->id) }}" class="btn btn-success btn-large mt10 mb10 btn-icon"><i class="mdi mdi-plus pr0"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6 text-left">
                                    <div class="btn-group">
                                        <a href="{{ URL::action('ReviewController@index', $client->id) }}" class="btn btn-default btn-large mt10 mb10" style="width: 133px"><i class="mdi mdi-calendar-check"></i>{{ Lang::get('review.reviews') }}</a>
                                        <a href="{{ URL::action('ReviewController@create', $client->id) }}" class="btn btn-success btn-large mt10 mb10 btn-icon"><i class="mdi mdi-plus pr0"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6 text-right">
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle mt10 mb10" style="width: 160px" type="button" data-toggle="dropdown"><i class="mdi mdi-printer"></i>{{ Lang::get('ui.generate_report.label') }}
                                            <span class="caret ml5"></span></button>
                                        <ul class="dropdown-menu multi-level">
                                            <li class="dropdown-submenu">
                                                <a tabindex="-1">{{ Lang::get('ui.generate_report.last_month') }}</a>
                                                <ul class="dropdown-menu">
                                                    <li><a target="_blank" href="{{ URL::action('ReportController@last_month', [ 'id' => $client->id ]) }}">{{ Lang::get('ui.generate_report.with_all') }}</a></li>
                                                    <li><a target="_blank" href="{{ URL::action('ReportController@last_month', [ 'id' => $client->id, 'extra' => 'with/service' ]) }}">{{ Lang::get('ui.generate_report.with_services') }}</a></li>
                                                    <li><a target="_blank" href="{{ URL::action('ReportController@last_month', [ 'id' => $client->id, 'extra' => 'with/review' ]) }}">{{ Lang::get('ui.generate_report.with_reviews') }}</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu">
                                                <a tabindex="-1">{{ Lang::get('ui.generate_report.last_week') }}</a>
                                                <ul class="dropdown-menu">
                                                    <li><a target="_blank" href="{{ URL::action('ReportController@last_week', [ 'id' => $client->id ]) }}">{{ Lang::get('ui.generate_report.with_all') }}</a></li>
                                                    <li><a target="_blank" href="{{ URL::action('ReportController@last_week', [ 'id' => $client->id, 'extra' => 'with/service' ]) }}">{{ Lang::get('ui.generate_report.with_services') }}</a></li>
                                                    <li><a target="_blank" href="{{ URL::action('ReportController@last_week', [ 'id' => $client->id, 'extra' => 'with/review' ]) }}">{{ Lang::get('ui.generate_report.with_reviews') }}</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a data-toggle="modal" data-target="#report-select-modal">{{ Lang::get('ui.generate_report.select_date') }}</a>
                                            </li>
                                            {{--<li class="divider"></li>--}}
                                            {{--<li><a target="_blank" href="#">{{ Lang::get('ui.generate_report.custom') }}</a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6 text-left">
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle mt10 mb10" style="width: 160px" type="button" data-toggle="dropdown"><i class="mdi mdi-file-export"></i>{{ Lang::get('ui.export.label') }}<span class="caret ml5"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a target="_blank" href="{{ URL::action('ClientController@export', [ 'id' => $client->id, 'type' => 'xls']) }}"><div class="icon-file-xls inline mr5"></div>{{ Lang::get('ui.export.format_xls') }}</a></li>
                                            <li><a target="_blank" href="{{ URL::action('ClientController@export', [ 'id' => $client->id, 'type' => 'xlsx']) }}"><div class="icon-file-xlsx inline mr5"></div>{{ Lang::get('ui.export.format_xlsx') }}</a></li>
                                            <li><a target="_blank" href="{{ URL::action('ClientController@export', [ 'id' => $client->id, 'type' => 'csv']) }}"><div class="icon-file-csv inline mr5"></div>{{ Lang::get('ui.export.format_csv') }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-6">
                        <div class="mt20 profile-name">
                            <h5>{{ Lang::get('ui.info_basic') }}</h5>
                            <dl class="pt5 pl15 bt">
                                <dt>{{ Lang::get('table.surname_and_name') }}</dt>
                                <dd><h3 class="mt0 text-capitalize">{{ $client->surname.' '.$client->name }}</h3></dd>
                                <dt>{{ Lang::get('table.address') }}</dt>
                                <dd class="text-muted text-capitalize">{!! empty($client->address)? Lang::get('error.no_data'): '<a href="'.URL::action('UserController@search', [ 'type' => 'client', 'keyword' => str_replace('/', '&#8260;', $client->address) ] ).'">'.$client->address.'</a>' !!}</dd>
                                <dt>{{ Lang::get('table.city') }}</dt>
                                <dd class="text-muted text-capitalize">{!! empty($client->city)? Lang::get('error.no_data'): '<a href="'.URL::action('UserController@search', [ 'type' => 'client', 'keyword' => $client->city ] ).'">'.$client->city.'</a>' !!}</dd>
                                <dt>{{ Lang::get('table.country') }}</dt>
                                <dd class="text-muted text-capitalize">{!! empty($client->country)? Lang::get('error.no_data'): '<a href="'.URL::action('UserController@search', [ 'type' => 'client', 'keyword' => $client->country ] ).'">'.$client->country.'</a>' !!}</dd>
                                @php($next_review = $client->nextExpectedReview())
                                @if($next_review != null)
                                    @php($next_review = $next_review->addYears(1))
                                    <dt>{{ Lang::get('review.next_expected') }}</dt>
                                    <dd class="text-muted text-capitalize">{{ Lang::get('ui.month_'.$next_review->month).' '.$next_review->year }}</dd>
                                @endif
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h5>{{ Lang::get('ui.info_additional') }}</h5>
                        <div class="contact-info bt">
                            <div class="row">
                                <!-- Start .row -->
                                <div class="col-md-4 col-xs-4">
                                    <dl class="mt20">
                                        <dt>{{ Lang::get('table.phone') }}</dt>
                                        <dd class="text-muted">{!! empty($client->phone)? Lang::get('error.no_data'): $client->phone.Html::skype_call($client->phone) !!}</dd>
                                    </dl>
                                </div>
                                <div class="col-md-8 col-xs-8">
                                    <dl class="mt20">
                                        <dt>{{ Lang::get('table.email') }}</dt>
                                        <dd class="text-muted">{!! empty($client->email)? Lang::get('error.no_data'): '<a class="btn btn-link pl0" href="mailto:'.$client->email.'">'.$client->email.'</a>' !!}</dd>
                                    </dl>
                                </div>
                            </div>
                            <!-- End .row -->
                        </div>
                    </div>
                </div>
                <!-- End .row -->

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <h5>{{ Lang::get('ui.map.dir') }}</h5>
                        <div class="bt pt10 pb10 text-center" style="position:relative;height: 476px;">
                            @if($gmp != null && property_exists($gmp, 'status') && $gmp->status == 'OK')
                                <i class="large mdi mdi-refresh spin" style="position:absolute;font-size: 29px !important;top: 220px;left: 50%;"></i>
                                <iframe src="https://www.google.com/maps?q={{$client->address}}+{{$client->city}}+{{$client->country}}&output=embed" frameborder="0" style="position:absolute;top:10px;left:0;z-index: 1;border:0;width:100%;height:450px" allowfullscreen></iframe>
                            @else
                                <div class="bs-callout bs-callout-warning text-left">
                                    <h4>{{ Lang::get('ui.map.warning.header') }}</h4>
                                    {{ Lang::get('ui.map.warning.message') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- End .row -->
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
    <script type='text/javascript'>
        $(function() {
            $('input[name="from"]').val('');
            $('input[name="to"]').val('');
            $('.input-daterange').datepicker({
                autoclose: true,
                dateFormat: 'yy-mm-dd'
            });
            $('input[name="from"], input[name="to"], select[name="extra"]').change(function () {
                if ($('input[name="from"]').val() != '' && $('input[name="to"]').val() != '') {
                    var d1 = new Date($('input[name="from"]').val());
                    var d2 = new Date($('input[name="to"]').val());
                    var d3 = $('select[name="extra"]').val();
                    var burl = location.origin + location.pathname;
                    if (d3 == 0)
                        $('#generate-report').attr('href', burl + '/print/report/range/' + d1.getFullYear() + '-' + d1.getMonth() + '-' + d1.getDate() + '/' + d2.getFullYear() + '-' + d2.getMonth() + '-' + d2.getDate());
                    else{
                        $('#generate-report').attr('href', burl + '/print/report/range/' + d1.getFullYear() + '-' + d1.getMonth() + '-' + d1.getDate() + '/' + d2.getFullYear() + '-' + d2.getMonth() + '-' + d2.getDate() + '/with/' + ((d3 == 1) ? 'service' : 'review') );
                    }
                }
            });
            $(document).on('click', '#generate-report', function () {
                $('#report-select-modal').modal('hide');
            });
            $('#report-select-modal').on('hidden.bs.modal', function () {
                $('input[name="from"]').val('');
                $('input[name="to"]').val('');
                $('#generate-report').removeAttr('href');
            })
        });
    </script>
@stop
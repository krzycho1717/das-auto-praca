<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'Client' => 'Klient',
    'client' => 'klient',
    'Clients' => 'Klienci',
    'clients' => 'klienci',
    'gen' => 'klientów',
    'add' => 'dodaj klienta',
    'latest' => 'Klienci - ostatnio dodani',
    'edit_title' => 'Klient - edycja',
    'create_title' => 'Nowy klient',
    'empty' => 'brak klientów',
    'select' => 'wybierz klienta',
    'report' => 'Raport klienta'

];

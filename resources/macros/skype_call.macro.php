<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 11.12.2016
 * Time: 18:41:00
 */

Html::macro('skype_call', function($telephone){
        $html = View::make('layouts.skype_call')
            ->with('telephone', $telephone)
            ->render();
        return new \Illuminate\Support\HtmlString($html);
});
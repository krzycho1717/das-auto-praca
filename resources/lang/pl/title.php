<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */
    'client.start' => 'Klienci',
    'client.new'   => 'Nowy klient',
    'client.show'  => 'Klient',
    'client.edit'  => 'Klient - edycja',

    'service.start' => 'Usługi',
    'service.new'   => 'Nowa usługa',
    'service.show'  => 'Usługa',
    'service.edit'  => 'Usługa - edycja',

    'review.start' => 'Przeglądy',
    'review.new'   => 'Nowy przegląd',
    'review.show'  => 'Przegląd',
    'review.edit'  => 'Przegląd - edycja',
];
@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('service.edit_title'), $errors, session('message')) }}
        {{ Form::open(['action' => ['ServiceController@update', $client, $service->id], 'method' => 'PUT']) }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                        {{ Form::label('content', Lang::get('table.content')) }}
                        {{ Form::textarea('content', $service->content, ['class' => 'form-control', 'placeholder' => Lang::get('table.content'), 'novalidate' => 'novalidate']) }}
                        <span class="text-danger">{{ $errors->first('content') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('charge') ? 'has-error' : '' }}">
                        {{ Form::label('charge', Lang::get('table.charge')) }}
                        {{ Form::number('charge', $service->charge, ['class' => 'form-control', 'placeholder' => Lang::get('table.charge'), 'required' => 'required', 'min' => '0.0', 'step' => '0.1']) }}
                        <span class="text-danger">{{ $errors->first('charge') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                        {{ Form::label('date', Lang::get('table.performed')) }}
                        {{ Form::date('date', $service->date, ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'required' => 'required']) }}
                        <span class="text-danger">{{ $errors->first('date') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(Lang::get('ui.save'), ['class' => 'btn btn-success pull-right']) }}
                </div>
            </div>
        {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="content"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            }
        });
    </script>

@stop
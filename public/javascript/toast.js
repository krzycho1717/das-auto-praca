/**
 * Created by krzyc on 02.07.2016.
 */

(function ( $ ) {

    $.toast = function( message , trigerEl) {

        $('div[id^=toast]').remove();

        var id = 'toast-' + uniqId();
        $('body').append('<div id="'+id+'">'+message+'</div>');

        var self = $('div#' + id);


        var o = $(trigerEl).offset();
        var el = $(trigerEl);
        console.groupCollapsed('test-data');

        var t = o.top - 50;
        var l = o.left - 100;
        self.css({
            'display': 'none',
            'position': 'absolute',
            'top': 0,
            'right': 0,
            'min-width': '-webkit-min-content',
            'min-width': '-moz-min-content',
            'min-width': 'min-content',
            'z-index': '1900',
            'border': '1px solid #D4D4D5',
            'line-height': '1.4285em',
            'max-width': '250px',
            'background': '#FFF',
            'padding': '.833em 1em',
            'font-weight': '400',
            'font-style': 'normal',
            'color': 'rgba(0,0,0,.87)',
            'border-radius': '.28571429rem',
            'box-shadow': '0 2px 4px 0 rgba(34,36,38,.12),0 2px 10px 0 rgba(34,36,38,.15)',
            'top': t,
            'left': l
        });
        console.groupEnd();
        $(self).slideDown('fast', function () {
            setTimeout(function () {
                $(self).slideUp('fast', function () {
                    $(self).remove();
                });
            }, 1000);
        });
        return this;
    };

    function uniqId() {
        return Math.round(new Date().getTime() + (Math.random() * 100));
    }

}( jQuery ));
@extends('layouts.default')
@section('content')

    <div class="col-md-8 column">
        {{ Html::panel_open('default', Lang::get('service.create_title'), $errors, session('message')) }}
            {{ Form::open(['action' => ['ServiceController@store', $client], 'method' => 'POST']) }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                            <i class="mdi mdi-refresh spin mdi-24px mce-loader"></i>
                            {{ Form::label('content', Lang::get('table.content')) }}
                            {{ Form::textarea('content', Input::old('content'), ['class' => 'form-control', 'placeholder' => Lang::get('table.content'), 'novalidate' => 'novalidate']) }}
                            <span class="text-danger">{{ $errors->first('content') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('charge') ? 'has-error' : '' }}">
                            {{ Form::label('charge', Lang::get('table.charge')) }}
                            <div class="input-group">
                                {{ Form::number('charge', Input::old('charge'), ['class' => 'form-control', 'placeholder' => '0.0', 'required' => 'required', 'min' => '0.0', 'step' => '0.1']) }}
                                <span class="input-group-addon">zł</span>
                            </div>
                            <span class="text-danger">{{ $errors->first('charge') }}</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                            {{ Form::label('date', Lang::get('table.performed')) }}
                            {{ Form::date('date', Input::old('date') ? Input::old('date') : Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'placeholder' => Lang::get('table.performed'), 'required' => 'required']) }}
                            <span class="text-danger">{{ $errors->first('date') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(Lang::get('ui.save'), ['class' => 'btn btn-success pull-right']) }}
                    </div>
                </div>
            {{ Form::close() }}
        {{ Html::panel_close() }}
    </div>

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea[name="content"]',
            setup: function (ed) {
                ed.on('LoadContent', function(e) {
                    $('.mce-loader').hide();
                    $('.mce-panel').show();
                });
            }
        });
    </script>

@stop
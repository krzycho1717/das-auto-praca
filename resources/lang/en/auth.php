<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'already_have_account'          => 'Already have account ?',
    'create_account'                => 'Create',
    'dont_have_account'             => 'Don\'t have account  ?',
    'failed_msg'                    => 'E-mail or password are incorrect. Make sure data You entered are correct.',
    'forgot_password'               => 'forgot password ?',
    'placeholder_email'             => 'Your email',
    'placeholder_name'              => 'Your name',
    'placeholder_open_file'         => 'Open',
    'placeholder_password'          => 'Your password',
    'placeholder_phone'             => 'Your phone number',
    'placeholder_photo'             => 'Your photo',
    'placeholder_repeat_password'   => 'Repeat password',
    'placeholder_surname'           => 'Your surname',
    'remember_me'                   => 'remember me',
    'sign_in'                       => 'Sign in',
    'sign_out'                      => 'Sign out',
];

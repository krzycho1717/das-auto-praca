<div class="bs-callout bs-callout-success">
    <h4>{{ Lang::get('ui.hint') }}</h4>
    <p>{!! Lang::get('ui.device_hint') !!}</p>
</div>
<h4 class="panel-title bb ml15 mr15 pt10 pb10">{{ Lang::get('token.devices.title') }}</h4>
<div class="col-md-12 mt20 mb20 no-gutter">
    @if(!CheckVar::c_array($tokens))
        <div class="clearfix">
            <div class="bs-callout bs-callout-info">
                <div class="row">
                    <div class="col-md-9 column">
                        <h4 style="line-height: 36px;margin: 0;">{{ Lang::get('error.no_data') }}</h4>
                    </div>
                </div>
            </div>
        </div>
    @else
        @foreach($tokens as $token)
            <div class="row token-container" data-token-id="{{ $token->id }}" data-token-name="{{ $token->name }}">
                <div class="col-md-12">
                    <div class="row">
                        <h3 class="pull-left">
                            <i class="mdi mdi-cellphone pull-left mr5"></i>
                            {{ $token->name }}
                        </h3>
                        <button class="delete-button btn btn-danger btn-sm pull-right">{{ Lang::get('ui.delete') }}</button>
                    </div>
                </div>
                <div class="col-md-12 m10">
                    <div class="col-md-5 mb10">
                        <div class="row item-device">
                            @if(!empty($token->udid))
                            <div class="row item-up active">
                                <div class="col-md-10 field">{{ $token->udid }}</div>
                                <div class="col-md-2 no-gutter">
                                    <div class="row text-center">
                                        <span class="item-icon" title="{{ Lang::get('ui.delete') }}"><i class="mdi mdi-delete"></i></span>
                                        <span class="item-icon clipboard-button" data-toggle="tooltip" data-placement="top" data-clipboard-text="{{ $token->udid }}" title="{{ Lang::get('ui.copy_to_clipboard') }}"><i class="mdi mdi-clipboard-check"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row item-down active">
                                {{ Lang::get('token.device.id.tag') }}
                            </div>
                            @else
                            <div class="row item-up">
                                <div class="col-md-12 field">{{ Lang::get('token.device.id.empty.tag') }}</div>
                            </div>
                            <div class="row item-down">
                                {{ Lang::get('token.device.id.tag') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-5 mb10">
                        <div class="row item-token">
                            <div class="row item-up">
                                <div class="col-md-9 field">{{ $token->token }}</div>
                                <div class="col-md-3 no-gutter">
                                    <div class="row text-center">
                                        <span class="item-icon" title="{{ Lang::get('ui.reset') }}"><i class="mdi mdi-reload"></i></span>
                                        <span class="item-icon" title="{{ Lang::get('ui.qrcode') }}" data-qr-token="{{ $token->token }}" data-content="{{ Lang::get('ui.qrcode') }}"><i class="mdi mdi-qrcode"></i></span>
                                        <span class="item-icon clipboard-button" data-clipboard-message="{{ Lang::get('ui.toast_clipboard_message') }}" data-clipboard-text="{{ $token->token }}" title="{{ Lang::get('ui.copy_to_clipboard') }}"><i class="mdi mdi-clipboard-check"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row item-down">
                                {{ Lang::get('token.device.token.tag') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
<link rel="stylesheet" href="/css/transition.css"/>
<script src="/javascript/transition.js"></script>
<link rel="stylesheet" href="/css/popup.css"/>
<script src="/javascript/popup.js"></script>
<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Review;
use Auth;
use Illuminate\Support\Facades\Lang;
use Input;
use View;
use Redirect;
use Validator;
use Session;
use Illuminate\Http\Request;
class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Client $client
     * @return \Illuminate\Http\Response
     */

    public function index(Client $client)
    {
        $result = $client->reviews()
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return View::make('page.panel.review.index')
            ->with('reviews', $result)
            ->with('client', $client)
            ->with('title', Lang::get('title.review.start'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Client $client
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        return View::make('page.panel.review.create')
            ->with('client', $client)
            ->with('title', Lang::get('title.review.new'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function raw_create()
    {
        $clients = Auth::user()->clients()->get();

        return View::make('page.panel.review.raw_create')
            ->with('clients', $clients)
            ->with('title', Lang::get('title.review.new'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Client $client
     * @return \Illuminate\Http\Response
     */
    public function store(Client $client)
    {
        $rules = array(
            'vin'       => 'required|alpha_num|size:17',
            'plate'     => 'required',
            'mileage'   => 'required|numeric|min:0',
            'comment'   => 'required',
            'car'       => 'required',
            'og1L'      => 'required|numeric',
            'og1P'      => 'required|numeric',
            'og2L'      => 'required|numeric',
            'og2P'      => 'required|numeric',
            'date'      => 'required|date',
            'n1'        => 'string',
            'n2'        => 'string',
            'n3'        => 'string',
            'n4'        => 'string',
            'n5'        => 'string',
            'n6'        => 'string',
            'n7'        => 'string',
            'n8'        => 'string',
            'n9'        => 'string',
            'n10'       => 'string',
            'n11'       => 'string',
            'n12'       => 'string',
            'n13'       => 'string',
            'n14'       => 'string',
            'n15'       => 'string',
            'n16'       => 'string',
            'n17'       => 'string',
            'n18'       => 'string',
            'n19'       => 'string',
            'n20'       => 'string',
            'n21'       => 'string',
            'n22'       => 'string',
            'n23'       => 'string',
            'n24'       => 'string',
            'n25'       => 'string',
            'n26'       => 'string',
            'n27'       => 'string'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput(Input::all())
                ->withErrors($validator);
        } else {
            $review = new Review();
            $review->user_id = Auth::user()->id;
            $review->client_id = $client->id;
            $review->vin = Input::get('vin');
            $review->car = Input::get('car');
            $review->reg = Input::get('plate');
            $review->mileage = Input::get('mileage');
            $review->comment = Input::get('comment');
            $review->tiresFrontLeft = floatval(Input::get('og1L'));
            $review->tiresFrontRight = floatval(Input::get('og1P'));
            $review->tiresBackLeft = floatval(Input::get('og2L'));
            $review->tiresBackRight = floatval(Input::get('og2P'));
            $review->date = Input::get('date');
            $review->n1 = Input::get('n1');
            $review->n2 = Input::get('n2');
            $review->n3 = Input::get('n3');
            $review->n4 = Input::get('n4');
            $review->n5 = Input::get('n5');
            $review->n6 = Input::get('n6');
            $review->n7 = Input::get('n7');
            $review->n8 = Input::get('n8');
            $review->n9 = Input::get('n9');
            $review->n10 = Input::get('n10');
            $review->n11 = Input::get('n11');
            $review->n12 = Input::get('n12');
            $review->n13 = Input::get('n13');
            $review->n14 = Input::get('n14');
            $review->n15 = Input::get('n15');
            $review->n16 = Input::get('n16');
            $review->n17 = Input::get('n17');
            $review->n18 = Input::get('n18');
            $review->n19 = Input::get('n19');
            $review->n20 = Input::get('n20');
            $review->n21 = Input::get('n21');
            $review->n22 = Input::get('n22');
            $review->n23 = Input::get('n23');
            $review->n24 = Input::get('n24');
            $review->n25 = Input::get('n25');
            $review->n26 = Input::get('n26');
            $review->n27 = Input::get('n27');
            $review->save();

            return Redirect::action('UserController@wall');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function raw_store(Request $request)
    {
        $rules = array(
            'client_id' => 'required|numeric|min:0',
            'vin'       => 'required|alpha_num|size:17',
            'car'       => 'required',
            'plate'     => 'required',
            'mileage'   => 'required|numeric|min:0',
            'comment'   => 'required',
            'og1L'      => 'required|numeric',
            'og1P'      => 'required|numeric',
            'og2L'      => 'required|numeric',
            'og2P'      => 'required|numeric',
            'date'      => 'required|date'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput(Input::all())
                ->withErrors($validator);
        } else {
            $client = Client::find(Input::get('client_id'));
            if($client != null) {

                $review = new Review();
                $review->user_id = Auth::user()->id;
                $review->client_id = Input::get('client_id');
                $review->vin = Input::get('vin');
                $review->car = Input::get('car');
                $review->reg = Input::get('plate');
                $review->mileage = Input::get('mileage');
                $review->comment = Input::get('comment');
                $review->tiresFrontLeft = floatval(Input::get('og1L'));
                $review->tiresFrontRight = floatval(Input::get('og1P'));
                $review->tiresBackLeft = floatval(Input::get('og2L'));
                $review->tiresBackRight = floatval(Input::get('og2P'));
                $review->date = Input::get('date');
                $review->n1 = Input::get('n1');
                $review->n2 = Input::get('n2');
                $review->n3 = Input::get('n3');
                $review->n4 = Input::get('n4');
                $review->n5 = Input::get('n5');
                $review->n6 = Input::get('n6');
                $review->n7 = Input::get('n7');
                $review->n8 = Input::get('n8');
                $review->n9 = Input::get('n9');
                $review->n10 = Input::get('n10');
                $review->n11 = Input::get('n11');
                $review->n12 = Input::get('n12');
                $review->n13 = Input::get('n13');
                $review->n14 = Input::get('n14');
                $review->n15 = Input::get('n15');
                $review->n16 = Input::get('n16');
                $review->n17 = Input::get('n17');
                $review->n18 = Input::get('n18');
                $review->n19 = Input::get('n19');
                $review->n20 = Input::get('n20');
                $review->n21 = Input::get('n21');
                $review->n22 = Input::get('n22');
                $review->n23 = Input::get('n23');
                $review->n24 = Input::get('n24');
                $review->n25 = Input::get('n25');
                $review->n26 = Input::get('n26');
                $review->n27 = Input::get('n27');

                $review->save();

                return Redirect::action('UserController@wall');

            }else{
                return Redirect::back()
                    ->withInput(Input::all());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, $id)
    {
        $result = $client->reviews()->find($id);
        if($result == null){
            abort(404);
        }

        return View::make('page.panel.review.show')
            ->with('review', $result)
            ->with('title', Lang::get('title.review.show'));
    }

    public function show_ordered($attribute, $order){
        if(!Review::canOrderBy($attribute)) return Redirect::to('/panel/review');
        $result = Auth::user()->reviews()->join('clients', 'reviews.client_id', '=', 'clients.id')->orderBy($attribute, $order)->paginate(25);
        return View::make('page.panel.review.index')
            ->with('reviews', $result)
            ->with('title', Lang::get('title.review.start'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, $id)
    {
        $result = $client->reviews()->find($id);
        $clients = Auth::user()->clients()->get();

        if($result == null || $clients == null){
            abort(404);
        }

        return View::make('page.panel.review.edit')
            ->with('review', $result->first())
            ->with('client', $client)
            ->with('clients', $clients)
            ->with('title', Lang::get('title.review.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client, $id)
    {
        $result = $client->reviews()->find($id);
        if($result == null){
            abort(404);
        }

        $rules = array(
            'vin'       => 'required|alpha_num|size:17',
            'reg'       => 'required',
            'mileage'   => 'required|numeric|min:0',
            'comment'   => 'required',
            'og1L'      => 'required|numeric',
            'og1P'      => 'required|numeric',
            'og2L'      => 'required|numeric',
            'og2P'      => 'required|numeric',
            'n1'        => 'string',
            'n2'        => 'string',
            'n3'        => 'string',
            'n4'        => 'string',
            'n5'        => 'string',
            'n6'        => 'string',
            'n7'        => 'string',
            'n8'        => 'string',
            'n9'        => 'string',
            'n10'       => 'string',
            'n11'       => 'string',
            'n12'       => 'string',
            'n13'       => 'string',
            'n14'       => 'string',
            'n15'       => 'string',
            'n16'       => 'string',
            'n17'       => 'string',
            'n18'       => 'string',
            'n19'       => 'string',
            'n20'       => 'string',
            'n21'       => 'string',
            'n22'       => 'string',
            'n23'       => 'string',
            'n24'       => 'string',
            'n25'       => 'string',
            'n26'       => 'string',
            'n27'       => 'string'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {

            $userdata = array(
                'vin' => Input::get('vin'),
                'reg' => Input::get('reg'),
                'mileage' => Input::get('mileage'),
                'comment' => Input::get('comment'),
                'tiresFrontLeft' => floatval(Input::get('og1L')),
                'tiresFrontRight' => floatval(Input::get('og1P')),
                'tiresBackLeft' => floatval(Input::get('og2L')),
                'tiresBackRight' => floatval(Input::get('og2P')),
                'n1' => Input::get('n1'),
                'n2' => Input::get('n2'),
                'n3' => Input::get('n3'),
                'n4' => Input::get('n4'),
                'n5' => Input::get('n5'),
                'n6' => Input::get('n6'),
                'n7' => Input::get('n7'),
                'n8' => Input::get('n8'),
                'n9' => Input::get('n9'),
                'n10' => Input::get('n10'),
                'n11' => Input::get('n11'),
                'n12' => Input::get('n12'),
                'n13' => Input::get('n13'),
                'n14' => Input::get('n14'),
                'n15' => Input::get('n15'),
                'n16' => Input::get('n16'),
                'n17' => Input::get('n17'),
                'n18' => Input::get('n18'),
                'n19' => Input::get('n19'),
                'n20' => Input::get('n20'),
                'n21' => Input::get('n21'),
                'n22' => Input::get('n22'),
                'n23' => Input::get('n23'),
                'n24' => Input::get('n24'),
                'n25' => Input::get('n25'),
                'n26' => Input::get('n26'),
                'n27' => Input::get('n27')
            );

            $result->update($userdata);

            return Redirect::action('ReviewController@show', [$client, $id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client, $id)
    {
        $result = $client->reviews()->find($id);

        if($result == null){
            abort(404);
        }
        $result->delete();
        Session::set('reviews_number', Auth::user()->statistics()->reviews_count);
        return Redirect::action('UserController@wall');
    }
}

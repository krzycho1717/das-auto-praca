@extends('layouts.user')
@section('content')

    <div class="container error-container">
        <div class="error-panel panel panel-default plain animated bounceIn">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <img id="logo" src="/images/logo.png" alt="Dynamic logo">
                </h4>
            </div>
            <div class="panel-body" style="text-align: center;">
                <h1 class="error-number">Error 5<span class="animated flip">0</span>0</h1>
                <h1 class="text-center s24">{{ Lang::get('error.500.message') }}</h1>
                <div class="divider-dashed mb25"></div>
                <div class="col-md-6 col-md-offset-3 mb10">
                    <div class="btn-group btn-group-vertical">
                        <a href="javascript: history.go(-1)" class="btn btn-default btn-block"><i class="fa fa-long-arrow-left"></i>{{ Lang::get('error.choice') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop